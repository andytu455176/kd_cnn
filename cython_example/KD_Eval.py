import numpy as np 
import kd_eval
import scipy.io

def KD_Eval(f1, f2, img1_shape, img2_shape, Hom, descdims, radius, eval_type):
    """
    f1 and f2 are keypoint format according to repeatability.m
    x, y, a, b, c, d1, d2, ......
    with shape (num_pts, feat_dim)
    """
    common_part = 1
    feat1 = np.zeros((f1.shape[0], 9+descdims), dtype=np.float32)
    feat2 = np.zeros((f2.shape[0], 9+descdims), dtype=np.float32)
    feat1[:, :5] = f1[:, :5]
    feat2[:, :5] = f2[:, :5]
    feat1[:, 9:(9+descdims)] = f1[:, 5:(5+descdims)]
    feat2[:, 9:(9+descdims)] = f2[:, 5:(5+descdims)]

    H = Hom.copy()
    H_inv = np.linalg.inv(H)

    feat1, feat1t = project_region(feat1, H)
    feat2, feat2t = project_region(feat2, H_inv)

    index = np.where((feat1[:, 0]+feat1[:, 7] < img1_shape[1])&(feat1[:, 0]-feat1[:, 7] >= 0)&(feat1[:, 1]+feat1[:, 8] < img1_shape[0])&(feat1[:, 1]-feat1[:, 8] >= 0))[0]
    feat1 = feat1[index, :]
    feat1t = feat1t[index, :]
    index =  np.where((feat1t[:, 0]+feat1t[:, 7] < img2_shape[1])&(feat1t[:, 0]-feat1t[:, 7] >= 0)&(feat1t[:, 1]+feat1t[:, 8] < img2_shape[0])&(feat1t[:, 1]-feat1t[:, 8] >= 0))[0]
    feat1 = feat1[index, :]
    feat1t = feat1t[index, :]

    index = np.where((feat2[:, 0]+feat2[:, 7] < img2_shape[1])&(feat2[:, 0]-feat2[:, 7] >= 0)&(feat2[:, 1]+feat2[:, 8] < img2_shape[0])&(feat2[:, 1]-feat2[:, 8] >= 0))[0]
    feat2 = feat2[index, :]
    feat2t = feat2t[index, :]
    index =  np.where((feat2t[:, 0]+feat2t[:, 7] < img1_shape[1])&(feat2t[:, 0]-feat2t[:, 7] >= 0)&(feat2t[:, 1]+feat2t[:, 8] < img1_shape[0])&(feat2t[:, 1]-feat2t[:, 8] >= 0))[0]
    feat2 = feat2[index, :]
    feat2t = feat2t[index, :]

    sf = float(np.min([feat1.shape[0], feat2.shape[0]]))

    # pass feat1 and feat2t to c_eoverlap 
    wout, twout, dout, tdout = kd_eval.c_eoverlap(feat1, feat2t, common_part)
    # calculate metric (repeatability, nn map, matching score)
    erro = range(10, 61, 10)
    corresp = np.zeros((6,), dtype=np.float32)
    for i in range(6):
        wi = (wout < erro[i])
        corresp[i] = np.sum(wi)

    repeat = 100 * corresp / sf
    print "noverlap error: "
    print erro
    print "repeatability: "
    print repeat
    print "nnb of correspondences: "
    print corresp

    match_overlap = 40
    if common_part == 0:
        match_overlap = 50
    if common_part == 0:
        twi  = (twout < match_overlap)
    else:
        twi = (wout < match_overlap)

    dx = (dout < 10000) * twi
    matches = float(np.sum(dx))
    match_score = 100.0 * matches / sf

    print "matching score: "
    print match_score
    print "num of correct matches: "
    print matches

    return wout, twout, dout, tdout

def project_region(feat, H):
    feat_o = feat.copy()
    feat_p = feat.copy()
    #scale = np.zeros((feat.shape[0],), dtype=np.float32)

    for i in xrange(feat_o.shape[0]):
        M = np.array([[feat_o[i, 2], feat_o[i, 3]], [feat_o[i, 3], feat_o[i, 4]]], dtype=np.float32)
        e, _ = np.linalg.eig(M)
        feat_o[i, 5:7] = 1.0 / np.sqrt(e)
        #scale[i] = np.sqrt(feat_o[i, 5]*feat_o[i, 6])
        feat_o[i, 7] = np.sqrt(feat_o[i, 4] / (feat_o[i, 4]*feat_o[i, 2] - feat_o[i, 3]**2))
        feat_o[i, 8] = np.sqrt(feat_o[i, 2] / (feat_o[i, 4]*feat_o[i, 2] - feat_o[i, 3]**2))
        
        Aff = get_Aff(feat_o[i, 0], feat_o[i, 1], H)

        l1 = np.array([feat_o[i, 0], feat_o[i, 1], 1], dtype=np.float32)
        l1_2 = np.dot(H, l1)
        l1_2 = l1_2 / l1_2[2]
        feat_p[i, 0] = l1_2[0]
        feat_p[i, 1] = l1_2[1]

        BMB = np.linalg.inv(np.dot(np.dot(Aff, np.linalg.inv(M)), Aff.T))
        e_p, _ = np.linalg.eig(BMB)
        feat_p[i, 5:7] = 1.0 / np.sqrt(e_p)
        feat_p[i, 2:5] = np.array([BMB[0, 0], BMB[0, 1], BMB[1, 1]], dtype=np.float32)
        feat_p[i, 7] = np.sqrt(feat_p[i, 4] / (feat_p[i, 4]*feat_p[i, 2] - feat_p[i, 3]**2))
        feat_p[i, 8] = np.sqrt(feat_p[i, 2] / (feat_p[i, 4]*feat_p[i, 2] - feat_p[i, 3]**2))

    return feat_o, feat_p


def get_Aff(x, y, H):
    h11=H[0, 0]
    h12=H[0, 1]
    h13=H[0, 2]
    h21=H[1, 0]
    h22=H[1, 1]
    h23=H[1, 2]
    h31=H[2, 0]
    h32=H[2, 1]
    h33=H[2, 2]

    fxdx=h11/(h31*x + h32*y +h33) - (h11*x + h12*y +h13)*h31/(h31*x + h32*y +h33)**2
    fxdy=h12/(h31*x + h32*y +h33) - (h11*x + h12*y +h13)*h32/(h31*x + h32*y +h33)**2
    
    fydx=h21/(h31*x + h32*y +h33) - (h21*x + h22*y +h23)*h31/(h31*x + h32*y +h33)**2
    fydy=h22/(h31*x + h32*y +h33) - (h21*x + h22*y +h23)*h32/(h31*x + h32*y +h33)**2

    Aff = np.array([[fxdx, fxdy], [fydx, fydy]], dtype=np.float32)

    return Aff


def generate_SIFT_feat():
    """
    generate SIFT feat: x, y, a, b, c from opencv implementation
    according to TILDE 
    """
    return 



