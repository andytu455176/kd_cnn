#import kd_eval
#import numpy as np
#
#a = np.random.rand(3, 15) # descdim = 6
#b = np.random.rand(5, 15) # descdim = 6
#print a.shape, b.shape
##[wout, twout, dout, tdout] = opee.c_eoverlap_RADIUS(a, b, 1, 5)
#[wout, twout, dout, tdout] = kd_eval.c_eoverlap(a, b, 1)
#print wout.shape, twout.shape, dout.shape, tdout.shape
#print wout
#print twout
#print dout
#print tdout
#
#import scipy.io
#temp = {}
#temp['a'] = a
#temp['b'] = b
#scipy.io.savemat('test_eoverlap.mat', temp)

from KD_Eval import KD_Eval
import numpy as np 
import re, cv2

def load_feat(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    descdim = int(lines[0])
    nb_kp = int(lines[1])
    print "nb_kp:", nb_kp, ",descdim:", descdim
    feat = np.zeros((nb_kp, descdim+5), dtype=np.float32)
    for i in range(2, 2+nb_kp):
        feat[i-2, :] = np.array([float(x) for x in re.split(" ", lines[i].strip())], dtype=np.float32)
    return feat, descdim

def load_img(filename):
    image = cv2.imread(filename)
    return image.shape[:2]

def load_hom(filename):
    matrix = np.zeros((3, 3), dtype=np.float32)
    with open(filename, 'r') as f:
        for i in xrange(3):
            matrix[i, :] = np.array(filter(None, re.split(' |\t', f.readline().strip())), dtype=np.float32)
    return matrix

main_dir = '/home/iis/KD_CNN/color/Mikolajczyk_dataset/'
file1 = main_dir + 'bark/img1.mser.sift'
file2 = main_dir + 'bark/img2.mser.sift'
imf1 = main_dir + 'bark/img1.ppm'
imf2 = main_dir + 'bark/img2.ppm'
Hom = main_dir + 'bark/H1to2p'
common_part = 1

feat1, descdim1 = load_feat(file1)
feat2, descdim2 = load_feat(file2)
#feat1, descdim1 = np.random.rand(3, 12), 7
#feat2, descdim2 = np.random.rand(3, 12), 7
image1_shape = load_img(imf1)
image2_shape = load_img(imf2)
H = load_hom(Hom)

if descdim1 != descdim2:
    print "descriptor dimension not the same!"
    exit(0)

wout, twout, dout, tdout = KD_Eval(feat1, feat2, image1_shape, image2_shape, H, descdim1, 5, 0)



