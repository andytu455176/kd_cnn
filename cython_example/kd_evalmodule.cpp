#include "Python.h"
#include "numpy/arrayobject.h"
#include "cmath"
#include <iostream>

double get_value(PyArrayObject* arr, int i, int j) {
	double* tmp = (double*)PyArray_GETPTR2(arr, i, j);
	return *tmp;
}

static PyObject* 
c_eoverlap(PyObject* dummy, PyObject* args)
{
	PyObject *arg1 = NULL, *arg2 = NULL; // for feat1, feat2
	PyArrayObject *feat1 = NULL, *feat2 = NULL;
	PyArrayObject *over_out = NULL, *mover_out = NULL, *desc_out = NULL, *mdesc_out = NULL;
	int common_part = -1;

	// get input arguments (feat1, feat2)
	if (!PyArg_ParseTuple(args, "O!O!i", &PyArray_Type, &arg1, &PyArray_Type, &arg2, &common_part))
		return NULL;

	feat1 = (PyArrayObject*)PyArray_FROM_OTF(arg1, NPY_DOUBLE, NPY_IN_ARRAY);
	if (feat1 == NULL || PyArray_NDIM(feat1) != 2) {
		Py_XDECREF(feat1);
		Py_XDECREF(feat2);
		return NULL;
	}

	feat2 = (PyArrayObject*)PyArray_FROM_OTF(arg2, NPY_DOUBLE, NPY_IN_ARRAY);
	if (feat2 == NULL || PyArray_NDIM(feat2) != 2) {
		Py_XDECREF(feat1);
		Py_XDECREF(feat2);
		return NULL;
	}

	int dims1[2]; dims1[0] = feat1->dimensions[0]; dims1[1] = feat1->dimensions[1];
	int dims2[2]; dims2[0] = feat2->dimensions[0]; dims2[1] = feat2->dimensions[1];
	printf("%d %d\n", dims1[0], dims2[0]);

	npy_intp odim[2];
	odim[0] = dims1[0];
	odim[1] = dims2[0];

	// create output array (wout, twout, dout, tdout)
	over_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);
	mover_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);
	desc_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);
	mdesc_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);

	// create temporary varible for computation 
	float *feat1a = new float[9];
	float *feat2a = new float[9];
	float *tdesc_out = new float[dims1[0]*dims2[0]];
	float *tover_out = new float[dims1[0]*dims2[0]];

	// python is row-wise, matlab is col-wise
	for (int i=0; i<dims1[0]; i++) {
		for (int j=0;j<dims2[0];j++) {
			double *o = (double*)PyArray_GETPTR2(over_out, i, j);
			*o = 100.0;
			double *d = (double*)PyArray_GETPTR2(desc_out, i, j);
			*d = 1000000.0;
		}
	} 

	printf("common part: %d\n", common_part);

	// main calculation
	float max_dist, fac, dist, dx, dy, bna, bua, descd, ov;
	for (int i = 0; i < dims1[0]; i++) {
		//max_dist=sqrt(feat1[f1+5]*feat1[f1+6]);
		max_dist = sqrt(get_value(feat1, i, 5)*get_value(feat1, i, 6));
		if(common_part)fac=30.0/max_dist;
		else fac=3;
		max_dist=max_dist*4;
		fac=1.0/(fac*fac);

		feat1a[2]=fac * get_value(feat1, i, 2);
		feat1a[3]=fac * get_value(feat1, i, 3);
		feat1a[4]=fac * get_value(feat1, i, 4);
		feat1a[7] = sqrt(feat1a[4]/(feat1a[2]*feat1a[4] - feat1a[3]*feat1a[3]));
		feat1a[8] = sqrt(feat1a[2]/(feat1a[2]*feat1a[4] - feat1a[3]*feat1a[3]));
		for (int j=0; j < dims2[0]; j++) {
			//compute shift error between ellipses
			dx=get_value(feat2, j, 0)-get_value(feat1, i, 0);
			dy=get_value(feat2, j, 1)-get_value(feat1, i, 1);
			dist=sqrt(dx*dx+dy*dy);
			if(dist<max_dist){

				feat2a[2]=fac * get_value(feat2, j, 2);
				feat2a[3]=fac * get_value(feat2, j, 3);
				feat2a[4]=fac * get_value(feat2, j, 4);
				feat2a[7] = sqrt(feat2a[4]/(feat2a[2]*feat2a[4] - feat2a[3]*feat2a[3]));
				feat2a[8] = sqrt(feat2a[2]/(feat2a[2]*feat2a[4] - feat2a[3]*feat2a[3]));

				//find the largest eigenvalue
				float maxx=ceil((feat1a[7]>(dx+feat2a[7]))?feat1a[7]:(dx+feat2a[7]));
				float minx=floor((-feat1a[7]<(dx-feat2a[7]))?(-feat1a[7]):(dx-feat2a[7]));
				float maxy=ceil((feat1a[8]>(dy+feat2a[8]))?feat1a[8]:(dy+feat2a[8]));
				float miny=floor((-feat1a[8]<(dy-feat2a[8]))?(-feat1a[8]):(dy-feat2a[8]));

				float mina=(maxx-minx)<(maxy-miny)?(maxx-minx):(maxy-miny);
				float dr=mina/50.0;
				bua=0;bna=0;int t1=0;
				//compute the area
				for(float rx=minx;rx<=maxx;rx+=dr){
					float rx2=rx-dx;t1++;
					for(float ry=miny;ry<=maxy;ry+=dr){
						float ry2=ry-dy;
						//compute the distance from the ellipse center
						float a=feat1a[2]*rx*rx+2*feat1a[3]*rx*ry+feat1a[4]*ry*ry;
						float b=feat2a[2]*rx2*rx2+2*feat2a[3]*rx2*ry2+feat2a[4]*ry2*ry2;
						//compute the area
						if(a<1 && b<1)bna++;
						if(a<1 || b<1)bua++;
					}
				}
				ov=100.0*(1-bna/bua);
				tover_out[i*dims2[0]+j]=ov;
				double *mo = (double*)PyArray_GETPTR2(mover_out, i, j);
				*mo = ov;
				//printf("overlap %f  \n",over_out[j*dims2[1]+i]);return;
			}else {
				tover_out[i*dims2[0]+j]=100.0;
				double *mo = (double*)PyArray_GETPTR2(mover_out, i, j);
				*mo = 100.0;
			}
			descd=0;
			for(int v=9;v<dims1[1];v++){
				descd+=((get_value(feat1, i, v)-get_value(feat2, j, v))*(get_value(feat1, i, v)-get_value(feat2, j, v)));
			}
			descd=sqrt(descd);
			tdesc_out[i*dims2[0]+j]=descd;  
			double *md = (double*)PyArray_GETPTR2(mdesc_out, i, j);
			*md = descd;
		}
	}

	float minr=100;
	int mini=0;
	int minj=0;
	do{
		minr=100;
		for (int i = 0; i < dims1[0]; i++) {
			for (int j = 0; j < dims2[0]; j++) {
				if (minr > tover_out[i*dims2[0]+j]) {
					minr = tover_out[i*dims2[0]+j];
					mini = i;
					minj = j;
				}
			}
		}
		if(minr<100){
			for (int i = 0; i < dims1[0]; i++) {
				tover_out[i*dims2[0]+minj] = 100;
			}
			for (int j = 0; j < dims2[0]; j++) {
				tover_out[mini*dims2[0]+j] = 100;
			}
			double *o = (double*)PyArray_GETPTR2(over_out, mini, minj);
			*o = minr;
		}
		//printf("%f\n", minr);
	}while(minr<70);


	int dnbr=0;
	do{
		minr=1000000;
		for (int i = 0; i < dims1[0]; i++) {
			for (int j = 0; j < dims2[0]; j++) {
				if (minr > tdesc_out[i*dims2[0]+j]) {
					minr = tdesc_out[i*dims2[0]+j];
					mini = i;
					minj = j;
				}
			}
		}
		if(minr<1000000){
			for (int i = 0; i < dims1[0]; i++) {
				tdesc_out[i*dims2[0]+minj] = 1000000;
			}
			for (int j = 0; j < dims2[0]; j++) {
				tdesc_out[mini*dims2[0]+j] = 1000000;
			}
			double *d = (double*)PyArray_GETPTR2(desc_out, mini, minj);
			*d = dnbr++;
		}
	}while(minr<1000000);

	// delete temporary varible 
	delete []tdesc_out;
	delete []tover_out;
	delete []feat1a;
	delete []feat2a;

	Py_DECREF(feat1);
	Py_DECREF(feat2);
	return Py_BuildValue("NNNN", over_out, mover_out, desc_out, mdesc_out);
}

static PyObject* 
c_eoverlap_RADIUS(PyObject* dummy, PyObject* args)
{
	PyObject *arg1 = NULL, *arg2 = NULL; // for feat1, feat2
	PyArrayObject *feat1 = NULL, *feat2 = NULL;
	PyArrayObject *over_out = NULL, *mover_out = NULL, *desc_out = NULL, *mdesc_out = NULL;
	int common_part = -1;
	float radius = -1;

	// get input arguments (feat1, feat2)
	if (!PyArg_ParseTuple(args, "O!O!if", &PyArray_Type, &arg1, &PyArray_Type, &arg2, &common_part, &radius))
		return NULL;

	radius *= radius;

	feat1 = (PyArrayObject*)PyArray_FROM_OTF(arg1, NPY_DOUBLE, NPY_IN_ARRAY);
	if (feat1 == NULL || PyArray_NDIM(feat1) != 2) {
		Py_XDECREF(feat1);
		Py_XDECREF(feat2);
		return NULL;
	}

	feat2 = (PyArrayObject*)PyArray_FROM_OTF(arg2, NPY_DOUBLE, NPY_IN_ARRAY);
	if (feat2 == NULL || PyArray_NDIM(feat2) != 2) {
		Py_XDECREF(feat1);
		Py_XDECREF(feat2);
		return NULL;
	}

	int dims1[2]; dims1[0] = feat1->dimensions[0]; dims1[1] = feat1->dimensions[1];
	int dims2[2]; dims2[0] = feat2->dimensions[0]; dims2[1] = feat2->dimensions[1];
	printf("%d %d\n", dims1[0], dims2[0]);

	npy_intp odim[2];
	odim[0] = dims1[0];
	odim[1] = dims2[0];

	// create output array (wout, twout, dout, tdout)
	over_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);
	mover_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);
	desc_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);
	mdesc_out = (PyArrayObject*)PyArray_SimpleNew(2, odim, NPY_DOUBLE);

	// create temporary varible for computation 
	float *feat1a = new float[9];
	float *feat2a = new float[9];
	float *tdesc_out = new float[dims1[0]*dims2[0]];
	float *tover_out = new float[dims1[0]*dims2[0]];

	// python is row-wise, matlab is col-wise
	for (int i=0; i<dims1[0]; i++) {
		for (int j=0;j<dims2[0];j++) {
			double *o = (double*)PyArray_GETPTR2(over_out, i, j);
			*o = 100.0;
			double *d = (double*)PyArray_GETPTR2(desc_out, i, j);
			*d = 1000000.0;
		}
	} 

	printf("common part: %d, radius: %f\n", common_part, radius);

	// main calculation
	for (int i = 0; i < dims1[0]; i++) {
		for (int j = 0; j < dims2[0]; j++) {
			// compute shift error betweein ellipses 
			float dx = get_value(feat2, j, 0) - get_value(feat1, i, 0);
			float dy = get_value(feat2, j, 1) - get_value(feat1, i, 1);
			float dist = dx*dx+dy*dy;
			if (dist < radius) {
				tover_out[i*dims2[0]+j] = 0.0;
				double *mo = (double*)PyArray_GETPTR2(mover_out, i, j);
				*mo = 0.0;
			}
			else {
				tover_out[i*dims2[0]+j] = 100.0;
				double *mo = (double*)PyArray_GETPTR2(mover_out, i, j);
				*mo = 100.0;
			}
			float descd = 0;
			for (int v = 9; v < dims1[1]; v++) {
				descd += ((get_value(feat1, i, v)-get_value(feat2, j, v))*(get_value(feat1, i, v)-get_value(feat2, j, v)));
			}
			descd = sqrt(descd);
			tdesc_out[i*dims2[0]+j] = descd;
			double *md = (double*)PyArray_GETPTR2(mdesc_out, i, j);
			*md = descd;
		}
	}

	float minr=100;
	int mini=0;
	int minj=0;
	do{
		minr=100;
		for (int i = 0; i < dims1[0]; i++) {
			for (int j = 0; j < dims2[0]; j++) {
				if (minr > tover_out[i*dims2[0]+j]) {
					minr = tover_out[i*dims2[0]+j];
					mini = i;
					minj = j;
				}
			}
		}
		if(minr<100){
			for (int i = 0; i < dims1[0]; i++) {
				tover_out[i*dims2[0]+minj] = 100;
			}
			for (int j = 0; j < dims2[0]; j++) {
				tover_out[mini*dims2[0]+j] = 100;
			}
			double *o = (double*)PyArray_GETPTR2(over_out, mini, minj);
			*o = minr;
		}
		printf("%f\n", minr);
	}while(minr<70);


	int dnbr=0;
	do{
		minr=1000000;
		for (int i = 0; i < dims1[0]; i++) {
			for (int j = 0; j < dims2[0]; j++) {
				if (minr > tdesc_out[i*dims2[0]+j]) {
					minr = tdesc_out[i*dims2[0]+j];
					mini = i;
					minj = j;
				}
			}
		}
		if(minr<1000000){
			for (int i = 0; i < dims1[0]; i++) {
				tdesc_out[i*dims2[0]+minj] = 1000000;
			}
			for (int j = 0; j < dims2[0]; j++) {
				tdesc_out[mini*dims2[0]+j] = 1000000;
			}
			double *d = (double*)PyArray_GETPTR2(desc_out, mini, minj);
			*d = dnbr++;
		}
	}while(minr<1000000);

	// delete temporary varible 
	delete []tdesc_out;
	delete []tover_out;
	delete []feat1a;
	delete []feat2a;

	Py_DECREF(feat1);
	Py_DECREF(feat2);
	return Py_BuildValue("NNNN", over_out, mover_out, desc_out, mdesc_out);
}


static struct PyMethodDef methods[] = {
	{"c_eoverlap", c_eoverlap, METH_VARARGS, "descript of c_eoverlap"},
	{"c_eoverlap_RADIUS", c_eoverlap_RADIUS, METH_VARARGS, "descript of c_eoverlap_RADIUS"},
	{NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initkd_eval (void)
{
	(void)Py_InitModule("kd_eval", methods);
	import_array();
}

