#ifndef CAFFE_ROI_PROPOSAL_LAYER_HPP_
#define CAFFE_ROI_PROPOSAL_LAYER_HPP_

#include <vector>
#include "caffe/common.hpp" // tmporary for print information 
#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"

namespace caffe {
template <typename Dtype>
class ROIProposalLayer : public Layer<Dtype> {
public: 
  explicit ROIProposalLayer(const LayerParameter& param)
		: Layer<Dtype>(param) {}
  virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);

  virtual inline const char* type() const { return "ROIProposal"; }

protected:
  virtual void Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
  virtual void Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
private:
  string orient_type_;
  Blob<Dtype> orient_bin_; // for hard_argmax 
  string scale_type_;
  Blob<Dtype> scale_bin_; // for hard_argmax
  Dtype coor_scale_;
  bool debug;
};
} // namespace caffe


#endif // CAFFE_ROI_PROPOSAL_LAYER_HPP_
