#ifndef CAFFE_ROT_SCL_TRANS_LAYER_HPP_
#define CAFFE_ROT_SCL_TRANS_LAYER_HPP_

#include <vector>
#include "caffe/common.hpp" // temporary for print information 
#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"

namespace caffe {
template <typename Dtype>
class ROTSCLTransLayer : public Layer<Dtype> {
public:
  explicit ROTSCLTransLayer(const LayerParameter& param)
		: Layer<Dtype>(param) {}
  virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);

  virtual inline const char* type() const { return "ROTSCLTrans"; }
protected:
  virtual void Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
  virtual void Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
private:
  int N_;
  bool debug;
};
} // namespace caffe 

#endif // CAFFE_ROT_SCL_TRANS_LAYER_HPP_
