#ifndef CAFFE_ROI_TRANS_LAYER_HPP_
#define CAFFE_ROI_TRANS_LAYER_HPP_

#include <vector>
#include "caffe/common.hpp" // temporary for print information 
#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"

namespace caffe {
template <typename Dtype>
class ROITransLayer : public Layer<Dtype> {
public:
  explicit ROITransLayer(const LayerParameter& param)
		: Layer<Dtype>(param) {}
  virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);

  virtual inline const char* type() const { return "ROITrans"; }
protected:
  virtual void Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
  virtual void Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
private:
  int output_H_;
  int output_W_;
  int N_, C_, H_, W_;
  int map_size_;
  Blob<Dtype> patch_mask_;
  Blob<Dtype> input_grid_;
  Blob<int> input_range_;

  // for compute theta gradient 
  Blob<Dtype> input_grad_cache_; // C_ * num_rois * 2 * output_H_ * output_W_
  Blob<Dtype> all_ones_; // C_
  bool debug;
};
} // namespace caffe 

#endif // CAFFE_ROI_TRANS_LAYER_HPP_
