#ifndef CAFFE_ROI_TRANS_LAYER_HPP_
#define CAFFE_ROI_TRANS_LAYER_HPP_

#include <vector>
#include "caffe/common.hpp" // temporary for print information 
#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"

namespace caffe {
template <typename Dtype>
class ROITransOldLayer : public Layer<Dtype> {
public:
  explicit ROITransOldLayer(const LayerParameter& param)
		: Layer<Dtype>(param) {}
  virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);

  virtual inline const char* type() const { return "ROITransOld"; }
protected:
  virtual void Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) { 
	  NOT_IMPLEMENTED;
  }
  virtual void Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
private:
  inline Dtype abs(Dtype x) {
	  if(x < 0) return -x; return x;
  }
  inline Dtype max(Dtype x, Dtype y) {
	  if(x < y) return y; return x;
  }

  Dtype transform_forward_cpu(const Dtype* pic, Dtype px, Dtype py);
  int output_H_;
  int output_W_;
  int N, C, W, H;
  Blob<Dtype> patch_mask;
  Blob<Dtype> theta;
  Blob<Dtype> input_grid;
  // for gpu backward
  Blob<Dtype> dTheta_tmp; 
  Blob<Dtype> all_ones_2; 
  // for debug
  bool debug;
};
} // namespace caffe 

#endif // CAFFE_ROI_TRANS_LAYER_HPP_
