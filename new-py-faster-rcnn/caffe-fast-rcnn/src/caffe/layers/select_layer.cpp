#include "caffe/layers/select_layer.hpp"
#include "cmath"

namespace caffe {

template <typename Dtype>
void SelectLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	debug = false;
	string prefix = "\t\t Select Layer:: LayerSetup:\t";

	num_select_idx_ = this->layer_param_.select_param().select_index_size();

	// reshape select_idx_
	vector<int> select_idx_shape(4);
	select_idx_shape[0] = num_select_idx_;
	select_idx_shape[1] = 1;
	select_idx_shape[2] = 1;
	select_idx_shape[3] = 1;
	select_idx_.Reshape(select_idx_shape);

	// fill select_idx_
	int* select_idx_data = select_idx_.mutable_cpu_data();
	for (int i = 0; i < num_select_idx_; i++) {
		select_idx_data[i] = this->layer_param_.select_param().select_index(i);
	}
	
	if (debug) std::cout << prefix << "Initialization finish!!" << std::endl;
}

template <typename Dtype>
void SelectLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t Select Layer:: Reshape:\t";
	// bottom[0]: roi_trans_proposal, bottom[1]: sim(is backprop), top[0]: selected roi_trans_proposal
	
	// reshape top
	vector<int> top_shape(4);
	top_shape[0] = bottom[0]->num();
	top_shape[1] = num_select_idx_;
	top_shape[2] = 1;
	top_shape[3] = 1;
	top[0]->Reshape(top_shape);

	if (debug) std::cout << prefix << "Finish!" << std::endl;
}

template <typename Dtype>
void SelectLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t Select Layer:: Forward_cpu:\t";

	Dtype* top_data = top[0]->mutable_cpu_data();
	caffe_set<Dtype>(top[0]->count(), Dtype(0), top_data);
	const int* select_idx_data = select_idx_.cpu_data();
	const Dtype* is_forward_data = bottom[1]->cpu_data();

	for (int i = 0; i < bottom[0]->num(); i++) {
		if (is_forward_data[i] == 1) {
			for (int j = 0; j < num_select_idx_; j++) {
				top_data[top[0]->offset(i, j, 0, 0)] = bottom[0]->data_at(i, select_idx_data[j], 0, 0);
			}
		}
	}

	if (debug) std::cout << prefix << "Finish" << std::endl;
}

template <typename Dtype>
void SelectLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t Select Layer:: Backward_cpu:\t";

	Dtype* bottom_diff = bottom[0]->mutable_cpu_diff();
	caffe_set<Dtype>(bottom[0]->count(), Dtype(0), bottom_diff);
	const int* select_idx_data = select_idx_.cpu_data();
	const Dtype* is_backprop_data = bottom[1]->cpu_data();

	for (int i = 0; i < bottom[0]->num(); i++) {
		if (is_backprop_data[i] == 1) {
			for (int j = 0; j < num_select_idx_; j++) {
				bottom_diff[bottom[0]->offset(i, select_idx_data[j], 0, 0)] += top[0]->diff_at(i, j, 0, 0);
			}
		}
	}

	if(debug) std::cout << prefix << "Finish" << std::endl;
}

#ifdef CPU_ONLY
STUB_GPU(SelectLayer);
#endif 

INSTANTIATE_CLASS(SelectLayer);
REGISTER_LAYER_CLASS(Select);
} // namespace caffe 

