#include "caffe/layers/roi_trans_layer.hpp"
#include "cmath"


namespace caffe {

template <typename Dtype>
void ROITransLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	debug = false;
	string prefix = "\t\t ROI Trans Layer:: LayerSetUp:\t";
	output_H_ = this->layer_param_.roit_param().output_h();
	output_W_ = this->layer_param_.roit_param().output_w();
	map_size_ = output_H_ * output_W_;
	if(debug) std::cout << prefix << "output_H_=" << output_H_ << " output_W_=" << output_W_ << std::endl;

	// initialize patch_mask_
	vector<int> patch_mask_shape(4);
	patch_mask_shape[0] = 1;
	patch_mask_shape[1] = 3;
	patch_mask_shape[2] = output_H_;
	patch_mask_shape[3] = output_W_;
	patch_mask_.Reshape(patch_mask_shape);
	Dtype* patch_mask_data = patch_mask_.mutable_cpu_data();
	for (int i = 0; i < output_H_; i++) {
		for (int j = 0; j < output_W_; j++) {
			//patch_mask_data[patch_mask_.offset(0, 0, i, j)] = (Dtype) j - (Dtype) (output_W_) / 2;
			//patch_mask_data[patch_mask_.offset(0, 1, i, j)] = (Dtype) i - (Dtype) (output_H_) / 2;
			patch_mask_data[patch_mask_.offset(0, 0, i, j)] = (Dtype) i - (Dtype) (output_H_) / 2;
			patch_mask_data[patch_mask_.offset(0, 1, i, j)] = (Dtype) j - (Dtype) (output_W_) / 2;
			patch_mask_data[patch_mask_.offset(0, 2, i, j)] = (Dtype) 1.0;
			// std::cout << "(" << patch_mask_.data_at(0, 0, i, j) << "," << patch_mask_.data_at(0, 1, i, j) << ")" << " ";
 			//// for x;
			//target_data[target_.offset(0, 0, h, w)] = (Dtype) w / (Dtype) (output_W_ - 1) * 2. - 1.;
            //// for y
            //target_data[target_.offset(0, 1, h, w)] = (Dtype) h / (Dtype) (output_H_ - 1) * 2. - 1.;
            //// for constant
            //target_data[target_.offset(0, 2, h, w)] = (Dtype) 1.0;
		}
		// std::cout << std::endl;
	}
	if(debug) std::cout << prefix << "Initialization finish!!" << std::endl;
}

template <typename Dtype>
void ROITransLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	
	string prefix = "\t\t ROI Trans Layer:: Reshape:\t";
	N_ = bottom[0]->shape()[0];
	C_ = bottom[0]->shape()[1];
	H_ = bottom[0]->shape()[2];
	W_ = bottom[0]->shape()[3];
	
	// create input_grid_
	vector<int> input_grid_shape(4);
	input_grid_shape[0] = bottom[1]->shape(0); // num_rois
	// input_grid_shape[0] = N_;
	input_grid_shape[1] = 2;
	input_grid_shape[2] = output_H_;
	input_grid_shape[3] = output_W_;
	input_grid_.Reshape(input_grid_shape);

	// create input_range_ (for bilinear sampling)
	vector<int> input_range_shape(4);
	input_range_shape[0] = bottom[1]->shape(0); // num_rois
	// input_range_shape[0] = N_; 
	input_range_shape[1] = output_H_;
	input_range_shape[2] = output_W_;
	input_range_shape[3] = 4;
	input_range_.Reshape(input_range_shape);
	
	// reshape top
	vector<int> top_shape(4);
	top_shape[0] = bottom[1]->shape(0); // num_rois
	// top_shape[0] = N_;
	top_shape[1] = C_;
	top_shape[2] = output_H_;
	top_shape[3] = output_W_;
	top[0]->Reshape(top_shape);

	// reshape input_grad_cache_
	vector<int> input_grad_cache_shape(5);
	input_grad_cache_shape[0] = C_;
	input_grad_cache_shape[1] = bottom[1]->shape(0); // num_rois
	input_grad_cache_shape[2] = 2;
	input_grad_cache_shape[3] = output_H_;
	input_grad_cache_shape[4] = output_W_;
	input_grad_cache_.Reshape(input_grad_cache_shape);

	// all_ones_
	vector<int> all_ones_shape(1);
	all_ones_shape[0] = C_;
	all_ones_.Reshape(all_ones_shape);
	caffe_set<Dtype>(C_, Dtype(1.0), all_ones_.mutable_cpu_data());

	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

template <typename Dtype>
void ROITransLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROI Trans Layer:: Forward_cpu:\t";

	const Dtype* patch_mask_data = patch_mask_.cpu_data();
	const Dtype* theta_data = bottom[1]->cpu_data();
	Dtype* input_grid_data = input_grid_.mutable_cpu_data();
	int* input_range_data = input_range_.mutable_cpu_data();
	Dtype* V = top[0]->mutable_cpu_data();

	caffe_set<Dtype>(top[0]->count(), Dtype(0.0), V);
	int theta_offset = 7;
	int num_rois = bottom[1]->num();
	for (int n = 0; n < num_rois; n++) { // num_rois 
		int b = bottom[1]->data_at(n, 0, 0, 0);
		// compute input source coordinates
		caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, 2, map_size_, 3, Dtype(1.0), 
				theta_data + n * theta_offset + 1, patch_mask_data, Dtype(0.0), input_grid_data + n * 2 * map_size_);
		// compute U for V given source coordinates 
		for (int h = 0; h < output_H_; h++) {
			for (int w = 0; w < output_W_; w++) {
				Dtype x = input_grid_data[input_grid_.offset(n, 0, h, w)];
				Dtype y = input_grid_data[input_grid_.offset(n, 1, h, w)];
				//std::cout << "(" << x << "," << y << ")" << " ";
				
				//int w_min = (floor(x) > 0) ? floor(x) : 0;
                //int w_max = (ceil(x) < W_ - 1) ? ceil(x) : (W_ - 1);
                //int h_min = (floor(y) > 0) ? floor(y) : 0;
                //int h_max = (ceil(y) < H_ - 1) ? ceil(y) : (H_ - 1);
				int w_min = (floor(y) > 0) ? floor(y) : 0;
                int w_max = (ceil(y) < W_ - 1) ? ceil(y) : (W_ - 1);
                int h_min = (floor(x) > 0) ? floor(x) : 0;
                int h_max = (ceil(x) < H_ - 1) ? ceil(x) : (H_ - 1);
                input_range_data[input_range_.offset(n,h,w,0)] = w_min;
                input_range_data[input_range_.offset(n,h,w,1)] = w_max;
                input_range_data[input_range_.offset(n,h,w,2)] = h_min;
                input_range_data[input_range_.offset(n,h,w,3)] = h_max;

				//std::cout << "weight and pixel value" << std::endl;
                for (int hh = h_min; hh <= h_max; hh++) {
					for (int ww = w_min; ww <= w_max; ww++) {
						for (int c = 0; c < C_; c++) {
							// Dtype weight = (1 - fabs(x - ww)) * (1 - fabs(y - hh));
							Dtype weight = (1 - fabs(x - hh)) * (1 - fabs(y - ww));
							V[top[0]->offset(n, c, h, w)] += bottom[0]->data_at(b, c, hh, ww) * weight;
							//std::cout << "(" << bottom[0]->data_at(n, c, hh, ww) << "," << weight << ") ";
						}
					}
					//std::cout << std::endl;
				}
				//std::cout << "---" << std::endl;

			}
			//std::cout << std::endl;
		}
		//std::cout << std::endl;
	}

	//std::cout << "--input_grid--" << input_grid_.num() << " " << input_grid_.channels() << "----" << std::endl;
	//for (int i = 0; i < input_grid_.num(); i++) {
	//	for (int c = 0; c < input_grid_.channels(); c++) {
	//		for (int h = 0; h < input_grid_.height(); h++) {
	//			for (int w = 0; w < input_grid_.width(); w++) {
	//				std::cout << input_grid_.data_at(i, c, h, w) << " ";
	//			}
	//			std::cout << std::endl;
	//		}
	//		std::cout << std::endl;
	//	}
	//	std::cout << std::endl;
	//}
	//std::cout << std::endl;


	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

template <typename Dtype>
void ROITransLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t ROI Trans Layer:: Backward_cpu:\t";
	const Dtype* dV = top[0]->cpu_diff();
	Dtype* dU = bottom[0]->mutable_cpu_diff();
	Dtype* dTheta = bottom[1]->mutable_cpu_diff();
	const Dtype* patch_mask_data = patch_mask_.cpu_data();
	const Dtype* input_grid_data = input_grid_.cpu_data();
	const int* input_range_data = input_range_.cpu_data();
	Dtype* input_grid_diff = input_grid_.mutable_cpu_diff();

	caffe_set<Dtype>(bottom[0]->count(), Dtype(0.0), dU);
	caffe_set<Dtype>(bottom[1]->count(), Dtype(0.0), dTheta);

	int theta_offset = 7;
	int num_rois = bottom[1]->num();
	for (int n = 0; n < num_rois; n++) { // num_rois 
		int b = bottom[1]->data_at(n, 0, 0, 0);
		for (int h = 0; h < output_H_; h++) {
			for (int w = 0; w < output_W_; w++) {
				Dtype x = input_grid_data[input_grid_.offset(n, 0, h, w)];
				Dtype y = input_grid_data[input_grid_.offset(n, 1, h, w)];
				
				int w_min = input_range_data[input_range_.offset(n,h,w,0)];
                int w_max = input_range_data[input_range_.offset(n,h,w,1)];
                int h_min = input_range_data[input_range_.offset(n,h,w,2)];
                int h_max = input_range_data[input_range_.offset(n,h,w,3)];
                Dtype tmp_source_x = 0;
                Dtype tmp_source_y = 0;
				
				for (int hh = h_min; hh <= h_max; hh++) {
					for (int ww = w_min; ww <= w_max; ww++) {
						//int sign_x = caffe_sign<Dtype>(ww - x);
						//int sign_y = caffe_sign<Dtype>(hh - y);
						int sign_x = caffe_sign<Dtype>(hh - x);
						int sign_y = caffe_sign<Dtype>(ww - y);

						for (int c = 0; c < C_; c++) {
							// compute dU
							Dtype dv = dV[top[0]->offset(n, c, h, w)];
							// dU[bottom[0]->offset(b, c, hh, ww)] += dv * (1 - fabs(x - ww)) * (1 - fabs(y - hh));
							dU[bottom[0]->offset(b, c, hh, ww)] += dv * (1 - fabs(x - hh)) * (1 - fabs(y - ww));
							// compute dx and dy
							dv = dv * bottom[0]->data_at(b, c, hh, ww);
							//tmp_source_x += dv * (1 - fabs(y - hh)) * sign_x;
							//tmp_source_y += dv * (1 - fabs(x - ww)) * sign_y;
							tmp_source_x += dv * (1 - fabs(y - ww)) * sign_x;
							tmp_source_y += dv * (1 - fabs(x - hh)) * sign_y;
						}
					}
				}
				input_grid_diff[input_grid_.offset(n, 0, h, w)] = tmp_source_x;
				input_grid_diff[input_grid_.offset(n, 1, h, w)] = tmp_source_y;
			}
		}
		caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasTrans, 2, 3, map_size_, Dtype(1.0), 
				input_grid_diff + 2 * map_size_ * n, patch_mask_data, Dtype(0.0), dTheta + n * theta_offset + 1);
	}
	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

#ifdef CPU_ONLY
STUB_GPU(ROITransLayer);
#endif 

INSTANTIATE_CLASS(ROITransLayer);
REGISTER_LAYER_CLASS(ROITrans);

} // namespace caffe 











