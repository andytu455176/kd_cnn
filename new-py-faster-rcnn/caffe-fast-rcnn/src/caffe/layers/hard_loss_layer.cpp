#include <algorithm>
#include <cmath>
#include <cfloat>

#include "caffe/layer.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/layers/hard_loss_layer.hpp"

using std::max;
using namespace std;
using namespace cv;

namespace caffe {
	int gen_random_num (int i) { return caffe_rng_rand()%i; }

template <typename Dtype>
void HardLossLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	debug = false;
	string prefix = "\t\t Hard Loss Layer:: LayerSetUp:\t";

	LossLayer<Dtype>::LayerSetUp(bottom, top);

	HardParameter hard_param = this->layer_param_.hard_param();
	neg_num_ = hard_param.neg_num();
	pair_size_ = hard_param.pair_size();
	hard_ratio_ = hard_param.hard_ratio();
	rand_ratio_ = hard_param.rand_ratio();
	margin_ = hard_param.margin();

	//if (debug) std::cout << "neg_num_: " << neg_num_ << " pair_size_: " << pair_size_ << " hard_ratio_: " << hard_ratio_ << " rand_ratio_: " << rand_ratio_ << " margin_: " << margin_ << std::endl;

  	diff_.ReshapeLike(*bottom[0]);
	dis_.Reshape(bottom[0]->num(), bottom[0]->num(), 1, 1);
	mask_.Reshape(bottom[0]->num(), bottom[0]->num(), 1, 1);

	if(debug) std::cout << prefix << "Initialization finish!!" << std::endl;
}

template <typename Dtype>
void HardLossLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t Hard Loss Layer:: Reshape:\t";
  	LossLayer<Dtype>::Reshape(bottom, top);

  	diff_.ReshapeLike(*bottom[0]);
	dis_.Reshape(bottom[0]->num(), bottom[0]->num(), 1, 1);
	mask_.Reshape(bottom[0]->num(), bottom[0]->num(), 1, 1);

	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

template <typename Dtype>
void HardLossLayer<Dtype>::set_mask(const vector<Blob<Dtype>*>& bottom) {
	HardParameter hard_param = this->layer_param_.hard_param();

	int hard_num = neg_num_ * hard_ratio_;
	int rand_num = neg_num_ * rand_ratio_;
	//if(debug) std::cout << hard_num << " " << rand_num << std::endl;

	const Dtype* bottom_data = bottom[0]->cpu_data();
	const Dtype* label = bottom[1]->cpu_data();
	int num = bottom[0]->num();
	int dim = bottom[0]->count() / bottom[0]->num();
	Dtype* dis_data = dis_.mutable_cpu_data();
	Dtype* mask_data = mask_.mutable_cpu_data();

	// initialize distance and mask to 0.0
	caffe_set<Dtype>(num*num, Dtype(0.0), dis_data);
	caffe_set<Dtype>(num*num, Dtype(0.0), mask_data);

	// calculate distance 
	for (int i = 0; i < num; i++) {
		for (int j = i + 1; j < num; j++) {
			const Dtype* feat1 = bottom_data + i * dim;
			const Dtype* feat2 = bottom_data + j * dim;
			Dtype dist = 0;
			for (int k = 0; k < dim; k++) {
				dist += (feat1[k] - feat2[k])*(feat1[k] - feat2[k]);
			}
			dis_data[i * num + j] = dist;
			dis_data[j * num + i] = dist;
		}
	}
	//if (debug) {
	//	for (int i = 0; i < num; i++) {
	//		for (int j = 0; j < num; j++) {
	//			std::cout << dis_data[i * num + j] << " ";
	//		}
	//		std::cout << std::endl;
	//	}
	//}

	// select samples 
	vector<pair<float, int> > negpairs;
	vector<int> sid1;
	vector<int> sid2;

	for (int i = 0; i < num; i+=pair_size_) {
		negpairs.clear();
		sid1.clear();
		sid2.clear();
		for (int j = 0; j < num; j++) {
			//if (debug) std::cout << label[i] << " " << label[j] << std::endl;
			if (label[j] == label[i])
				continue;
			Dtype loss = max(Dtype(0), Dtype(margin_)-sqrt(dis_data[i * num + j]));
			//if (debug) std::cout << loss << std::endl;
			if (loss == 0) continue;

			negpairs.push_back(make_pair(dis_data[i * num + j], j));
		}
		//if (debug) std::cout << "==========" << negpairs.size() << std::endl;
		if (negpairs.size() <= neg_num_) {
			for (int j = 0; j < negpairs.size(); j++) {
					int id = negpairs[j].second;
					mask_data[i * num + id] = 1;
			}
			continue;
		}
		sort(negpairs.begin(), negpairs.end());

		for (int j = 0; j < neg_num_; j++) {
			sid1.push_back(negpairs[j].second);
		}
		for (int j = neg_num_; j < negpairs.size(); j++) {
			sid2.push_back(negpairs[j].second);
		}
		std::random_shuffle(sid1.begin(), sid1.end(), gen_random_num);
		
		for(int j = 0; j < min(hard_num, (int)(sid1.size())); j++) {
			mask_data[i * num + sid1[j]] = 1;
		}
		for(int j = hard_num; j < sid1.size(); j++) {
			sid2.push_back(sid1[j]);
		}
		std::random_shuffle(sid2.begin(), sid2.end(), gen_random_num);

		for(int j = 0; j < min(rand_num, (int)(sid2.size())); j ++) {
			mask_data[i * num + sid2[j]] = 1;
		}
	}

	//if (debug) {
	//	for (int i = 0; i < num; i++) {
	//		for (int j = 0; j < num; j++) {
	//			// std::cout << " (" << dis_data[i * num + j] << "-" << mask_data[i * num + j] << ") ";
	//			std::cout << mask_data[i*num+j];
	//		}
	//		std::cout << std::endl;
	//	}
	//}
}

template <typename Dtype>
void HardLossLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t Hard Loss Layer:: Forward_cpu:\t";

	int num = bottom[0]->num();

	Dtype* dis_data = dis_.mutable_cpu_data();
	Dtype* mask_data = mask_.mutable_cpu_data();

	set_mask(bottom);

	Dtype loss = 0;
	cnt_ = 0;

	for (int i = 0; i < num; i+=pair_size_) {
		loss += dis_data[i * num + i+1];
		//if (debug)
		//	std::cout << "same label loss in forward: " << dis_data[i * num + i+1] << std::endl;
		cnt_ += 1;
		for (int j = 0; j < num; j++) {
			if (mask_data[i * num + j] == 0)
				continue;
			Dtype neg_loss1 = max(Dtype(0), Dtype(margin_) - sqrt(dis_data[i * num + j]));
			Dtype neg_loss2 = max(Dtype(0), Dtype(margin_) - sqrt(dis_data[(i+1) * num + j]));
			loss += (neg_loss1*neg_loss1 + neg_loss2*neg_loss2);
			//loss += (neg_loss1*neg_loss1);
			cnt_ += 2;
			//cnt_ += 1;
		}
	}

	loss = loss / (2.0*cnt_);
	top[0]->mutable_cpu_data()[0] = loss;

	// if(debug) std::cout << "cnt_: " << cnt_ << std::endl;
	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

template <typename Dtype>
void HardLossLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t Hard Loss Layer:: Backward_cpu:\t";

	const Dtype* bottom_data = bottom[0]->cpu_data();
	Dtype* bottom_diff = bottom[0]->mutable_cpu_diff();
	int count = bottom[0]->count();
	int num = bottom[0]->num();
	int dim = bottom[0]->count() / bottom[0]->num();

	Dtype* dis_data = dis_.mutable_cpu_data();
	Dtype* mask_data = mask_.mutable_cpu_data();
	const Dtype alpha = 2.0 / static_cast<Dtype>(cnt_);

	caffe_set<Dtype>(count, Dtype(0.0), bottom_diff);

	for (int i = 0; i < num; i+=pair_size_) {
		const Dtype* fori = bottom_data + i * dim;
	    const Dtype* fpos = bottom_data + (i + 1) * dim;

	    Dtype* fori_diff = bottom_diff + i * dim;
		Dtype* fpos_diff = bottom_diff + (i + 1) * dim;

		// handle positive term diff
		for (int k = 0; k < dim; k++) {
			fori_diff[k] += 1.0 * alpha * (fori[k] - fpos[k]);
			fpos_diff[k] += -1.0 * alpha * (fori[k] - fpos[k]);
		}

		for (int j = 0; j < num; j++) {
			if (mask_data[i * num + j] == 0) continue;

			const Dtype* fneg = bottom_data + j * dim;
			Dtype* fneg_diff = bottom_diff + j * dim;

			Dtype mdist1 = Dtype(margin_) - sqrt(dis_data[i * num + j]);
			Dtype mdist2 = Dtype(margin_) - sqrt(dis_data[(i+1) * num + j]);

			// handle negative term diff * 2
			if (mdist1 > 0) {
				const Dtype beta = -alpha * mdist1 / (sqrt(dis_data[i * num + j]) + 1e-4);
				for (int k = 0; k < dim; k++) {
					fori_diff[k] += 1.0 * beta * (fori[k] - fneg[k]);
					fneg_diff[k] += -1.0 * beta * (fori[k] - fneg[k]);
				}
			}
			if (mdist2 > 0) {
				const Dtype beta = -alpha * mdist2 / (sqrt(dis_data[(i+1) * num + j]) + 1e-4);
				for (int k = 0; k < dim; k++) {
					fpos_diff[k] += 1.0 * beta * (fpos[k] - fneg[k]);
					fneg_diff[k] += -1.0 * beta * (fpos[k] - fneg[k]);
				}
			}
		}
	}

	//if (debug) {
	//	for (int i = 0; i < num; i++) {
	//		for (int k = 0; k < dim; k++) {
	//			std::cout << bottom_diff[i*dim+k] << " ";
	//		}
	//		std::cout << std::endl;
	//	}
	//}

	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

#ifdef CPU_ONLY
STUB_GPU(HardLossLayer);
#endif

INSTANTIATE_CLASS(HardLossLayer);
REGISTER_LAYER_CLASS(HardLoss);
} // namespace caffe 

