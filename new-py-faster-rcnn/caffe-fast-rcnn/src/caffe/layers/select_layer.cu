#include "caffe/layers/select_layer.hpp"
#include "cmath"

namespace caffe {

template <typename Dtype>
__global__ void SelectLayerForwardGPU(const int nthreads, const Dtype* bottom_data, Dtype* top_data, const int* select_idx_data, const int num_select_idx, const int bottom_channels, const Dtype* is_forward_data) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		if (is_forward_data[index] == 1) {
			for (int i = 0; i < num_select_idx; i++) {
				top_data[index * num_select_idx + i] = bottom_data[index * bottom_channels + select_idx_data[i]];
			}
		}
	}
}

template <typename Dtype>
__global__ void SelectLayerBackwardGPU(const int nthreads, const Dtype* top_diff, Dtype* bottom_diff, const int* select_idx_data, const int num_select_idx, const int bottom_channels, const Dtype* is_backprop_data) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		if (is_backprop_data[index] == 1) {
			for (int i = 0; i < num_select_idx; i++) {
				bottom_diff[index * bottom_channels + select_idx_data[i]] += top_diff[index * num_select_idx + i];
			}
		}
	}
}

template <typename Dtype>
void SelectLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t Select Layer:: Forward_gpu:\t";

	const Dtype* bottom_data = bottom[0]->gpu_data();
	const int* select_idx_data = select_idx_.gpu_data();
	Dtype* top_data = top[0]->mutable_gpu_data();
	const Dtype* is_forward_data = bottom[1]->gpu_data();

	caffe_gpu_set<Dtype>(top[0]->count(), Dtype(0), top_data);

	const int nthreads = bottom[0]->num();
	SelectLayerForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, bottom_data, top_data, select_idx_data, num_select_idx_, bottom[0]->channels(), is_forward_data);
	CUDA_POST_KERNEL_CHECK;

	if (debug) std::cout << prefix << "Finish" << std::endl;
}

template <typename Dtype>
void SelectLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t Select Layer:: Backward_gpu:\t";

	const Dtype* top_diff = top[0]->gpu_diff();
	const int* select_idx_data = select_idx_.gpu_data();
	const Dtype* is_backprop_data = bottom[1]->gpu_data();
	Dtype* bottom_diff = bottom[0]->mutable_gpu_diff();
	caffe_gpu_set<Dtype>(bottom[0]->count(), Dtype(0), bottom_diff);

	const int nthreads = bottom[0]->num();
	SelectLayerBackwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, top_diff, bottom_diff, select_idx_data, num_select_idx_, bottom[0]->channels(), is_backprop_data);
	CUDA_POST_KERNEL_CHECK;

	if (debug) std::cout << prefix << "Finish" << std::endl;
}

INSTANTIATE_LAYER_GPU_FUNCS(SelectLayer);
} // namespace caffe 

