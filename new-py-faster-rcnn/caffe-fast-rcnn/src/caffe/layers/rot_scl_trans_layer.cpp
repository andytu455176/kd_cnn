#include "caffe/layers/rot_scl_trans_layer.hpp"
#include "cmath"

namespace caffe {

template <typename Dtype>
void ROTSCLTransLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	debug = false;
	string prefix = "\t\t ROT SCL Trans Layer:: LayerSetUp:\t";
	if (debug) std::cout << prefix << "Initialization Finsh!!" << std::endl;
}

template <typename Dtype>
void ROTSCLTransLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROT SCL Trans Layer:: Reshape:\t";
	N_ = bottom[0]->num();
	if (debug) std::cout << prefix << "N_: " << N_ << std::endl;

	// reshape top
	vector<int> top_shape(4);
	top_shape[0] = N_; top_shape[1] = 7; top_shape[2] = 1; top_shape[3] = 1;
	top[0]->Reshape(top_shape);
	if (debug) std::cout << prefix << "Finsh!!" << std::endl;
}

template <typename Dtype>
void ROTSCLTransLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROT SCL Trans Layer:: Forward_cpu:\t";
	Dtype* top_data = top[0]->mutable_cpu_data();

	for (int i = 0; i < N_; i++) {
		Dtype idx = bottom[0]->data_at(i, 0, 0, 0);
		Dtype x = bottom[0]->data_at(i, 1, 0, 0);
		Dtype y = bottom[0]->data_at(i, 2, 0, 0);
		Dtype rot = bottom[0]->data_at(i, 3, 0, 0);
		Dtype scl = bottom[0]->data_at(i, 4, 0, 0);
		top_data[top[0]->offset(i, 0, 0, 0)] = idx;
		top_data[top[0]->offset(i, 1, 0, 0)] = scl * cos(rot);
		top_data[top[0]->offset(i, 2, 0, 0)] = scl * sin(rot);
		top_data[top[0]->offset(i, 3, 0, 0)] = x;
		top_data[top[0]->offset(i, 4, 0, 0)] = -scl * sin(rot);
		top_data[top[0]->offset(i, 5, 0, 0)] = scl * cos(rot);
		top_data[top[0]->offset(i, 6, 0, 0)] = y;
	}
	if (debug) std::cout << prefix << "Finsh!!" << std::endl;
}

template <typename Dtype>
void ROTSCLTransLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t ROT SCL Trans Layer:: Backward_cpu:\t";
	const Dtype* top_diff = top[0]->cpu_diff();
	Dtype* bottom_diff = bottom[0]->mutable_cpu_diff();

	for (int i = 0; i < N_; i++) {
		Dtype s = sin(bottom[0]->data_at(i, 3, 0, 0));
		Dtype c = cos(bottom[0]->data_at(i, 3, 0, 0));
		Dtype scl = bottom[0]->data_at(i, 4, 0, 0);
		Dtype t1 = top_diff[top[0]->offset(i, 1, 0, 0)];
		Dtype t2 = top_diff[top[0]->offset(i, 2, 0, 0)];
		Dtype t4 = top_diff[top[0]->offset(i, 4, 0, 0)];
		Dtype t5 = top_diff[top[0]->offset(i, 5, 0, 0)];
		bottom_diff[bottom[0]->offset(i, 0, 0, 0)] = top_diff[top[0]->offset(i, 0, 0, 0)];
		bottom_diff[bottom[0]->offset(i, 1, 0, 0)] = top_diff[top[0]->offset(i, 3, 0, 0)];
		bottom_diff[bottom[0]->offset(i, 2, 0, 0)] = top_diff[top[0]->offset(i, 6, 0, 0)];
		bottom_diff[bottom[0]->offset(i, 3, 0, 0)] = scl * (-s*t1 + c*t2 - c*t4 - s*t5);
		bottom_diff[bottom[0]->offset(i, 4, 0, 0)] = c*t1 + s*t2 - s*t4 + c*t5;
	}
	if (debug) std::cout << prefix << "Finsh!!" << std::endl;
}


#ifdef CPU_ONLY
STUB_GPU(ROTSCLTransLayer);
#endif 

INSTANTIATE_CLASS(ROTSCLTransLayer);
REGISTER_LAYER_CLASS(ROTSCLTrans);

} // namespace caffe 








