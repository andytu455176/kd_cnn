#include "caffe/layers/roi_trans_old_layer.hpp"
#include "caffe/util/gpu_util.cuh"
#include "cmath"

namespace caffe {

template <typename Dtype>
__global__ void ComputeTransParam(const int nthreads, const Dtype* rois_trans, Dtype* theta_data, int offset) {
	// compute theta_data 
	CUDA_KERNEL_LOOP(index, nthreads) {
		Dtype* trans_param = theta_data + 6 * index;
		const Dtype* input_trans = rois_trans + offset * index;
		trans_param[0] = input_trans[4]*cos(input_trans[3]);
		trans_param[1] = input_trans[4]*sin(input_trans[3]);
		trans_param[2] = input_trans[1];
		trans_param[3] = -input_trans[4]*sin(input_trans[3]);
		trans_param[4] = input_trans[4]*cos(input_trans[3]);
		trans_param[5] = input_trans[2];
	}
}

template <typename Dtype>
__global__ void ROITransOldForwardGPU(const int nthreads, const Dtype* patch_mask_data, const Dtype* rois_trans, const Dtype* theta_data, int offset, Dtype* input_grid_data, const Dtype* U, Dtype* V, const int C, const int output_H_, const int output_W_, const int H, const int W) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		// compute input_grid_data (coordinate)
		const int t = index % output_W_;
		const int s = (index / output_W_) % output_H_;
		const int c = (index / (output_H_ * output_W_)) % C;
		const int r = index / (output_H_ * output_W_ * C);
		const int b = rois_trans[r * offset];
		Dtype* coordinate = input_grid_data + (output_H_ * output_W_ * 2) * r;
		const int row_idx = output_W_ * s + t;
		const Dtype* trans_param = theta_data + 6 * r;
		coordinate[row_idx*2] = trans_param[0]*patch_mask_data[row_idx*3] + trans_param[1]*patch_mask_data[row_idx*3+1] + trans_param[2];
		coordinate[row_idx*2+1] = trans_param[3]*patch_mask_data[row_idx*3] + trans_param[4]*patch_mask_data[row_idx*3+1] + trans_param[5];


		// compute final output (V)
		const Dtype px = coordinate[row_idx * 2];
		const Dtype py = coordinate[row_idx * 2 + 1];
		V[index] = (Dtype)0.;
		int m, n; Dtype w;
		const Dtype* pic = U + b * (C * H * W) + c * (H * W);

	  	m = floor(px); n = floor(py); w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (px - m)) * (1 - (py - n));
	  		V[index] += w * pic[m * W + n];
	  	}

	  	m = floor(px) + 1; n = floor(py); w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (m - px)) * (1 - (py - n));
	  		V[index] += w * pic[m * W + n];
	  	}

	  	m = floor(px); n = floor(py) + 1; w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (px - m)) * (1 - (n - py));
	  		V[index] += w * pic[m * W + n];
	  	}

	  	m = floor(px) + 1; n = floor(py) + 1; w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (m - px)) * (1 - (n - py));
	  		V[index] += w * pic[m * W + n];
	  	}
	}
}

template <typename Dtype>
__global__ void ROITransOldBackwardGPU_dU(const int nthreads, const Dtype* dV, Dtype* dU, const Dtype* input_grid_data, const Dtype* rois_trans, int offset, const int C, const int output_H_, const int output_W_, const int H, const int W) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		const int t = index % output_W_;
		const int s = (index / output_W_) % output_H_;
		const int c = (index / (output_H_ * output_W_)) % C;
		const int r = index / (output_H_ * output_W_ * C);
		const int b = rois_trans[r * offset];

		const Dtype* coordinate = input_grid_data + (output_H_ * output_W_ * 2) * r;
		const int row_idx = output_W_ * s + t;
		const Dtype px = coordinate[row_idx * 2];
		const Dtype py = coordinate[row_idx * 2 + 1];

	  	int m, n; Dtype w;
	  	Dtype* pic = dU + b * (C * H * W) + c * (H * W);

	  	m = floor(px); n = floor(py); w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (px - m)) * (1 - (py - n));
			caffe_gpu_atomic_add(w * dV[index], pic + (m * W + n));
	  	}

	  	m = floor(px) + 1; n = floor(py); w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (m - px)) * (1 - (py - n));
			caffe_gpu_atomic_add(w * dV[index], pic + (m * W + n));
	  	}

	  	m = floor(px); n = floor(py) + 1; w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (px - m)) * (1 - (n - py));
			caffe_gpu_atomic_add(w * dV[index], pic + (m * W + n));
	  	}

	  	m = floor(px) + 1; n = floor(py) + 1; w = 0;
	  	if(m >= 0 && m < H && n >= 0 && n < W) {
	  		w = (1 - (m - px)) * (1 - (n - py));
			caffe_gpu_atomic_add(w * dV[index], pic + (m * W + n));
	  	}
	}
}

template <typename Dtype>
__global__ void ROITransOldBackwardGPU_dTheta(const int nthreads, const Dtype* dV, const Dtype* U, Dtype* dTheta_tmp_diff, const Dtype* input_grid_data, const Dtype* rois_trans, int offset, const int C, const int output_H_, const int output_W_, const int H, const int W, const Dtype* patch_mask_data) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		const int t = index % output_W_;
		const int s = (index / output_W_) % output_H_;
		const int c = (index / (output_H_ * output_W_)) % C;
		const int r = index / (output_H_ * output_W_ * C);
		const int b = rois_trans[r * offset];

		const Dtype* coordinate = input_grid_data + (output_H_ * output_W_ * 2) * r;
		const int row_idx = output_W_ * s + t;
		const Dtype px = coordinate[row_idx * 2];
		const Dtype py = coordinate[row_idx * 2 + 1];

		Dtype delta_dpx = (Dtype)0.;
		Dtype delta_dpy = (Dtype)0.;
		const Dtype dv = dV[index];

		int m, n;
	  	const Dtype* pic = U + b * (C * H * W) + c * (H * W);

		m = floor(px); n = floor(py);
		if(m >= 0 && m < H && n >= 0 && n < W) {
			delta_dpx -= (1 - (py - n)) * pic[m * W + n] * dv;
			delta_dpy -= (1 - (px - m)) * pic[m * W + n] * dv;
		}

		m = floor(px); n = floor(py) + 1;
		if(m >= 0 && m < H && n >= 0 && n < W) {
			delta_dpx -= (1 - (n - py)) * pic[m * W + n] * dv;
			delta_dpy += (1 - (px - m)) * pic[m * W + n] * dv;
		}

		m = floor(px) + 1; n = floor(py);
		if(m >= 0 && m < H && n >= 0 && n < W) {
			delta_dpx += (1 - (py - n)) * pic[m * W + n] * dv;
			delta_dpy -= (1 - (m - px)) * pic[m * W + n] * dv;
		}

		m = floor(px) + 1; n = floor(py) + 1;
		if(m >= 0 && m < H && n >= 0 && n < W) {
			delta_dpx += (1 - (n - py)) * pic[m * W + n] * dv;
			delta_dpy += (1 - (m - px)) * pic[m * W + n] * dv;
		}
		
		int idx = c * (output_H_ * output_W_) + s * output_W_ + t;
		Dtype d11 = delta_dpx * patch_mask_data[row_idx * 3];
		Dtype d12 = delta_dpx * patch_mask_data[row_idx * 3+1];
		Dtype d21 = delta_dpy * patch_mask_data[row_idx * 3];
		Dtype d22 = delta_dpy * patch_mask_data[row_idx * 3+1];
		Dtype rot = rois_trans[r * offset + 3];
		Dtype scl = rois_trans[r * offset + 4];

		dTheta_tmp_diff[(5 * r + 0) * (output_H_ * output_W_ * C) + idx] += 0.0;
		dTheta_tmp_diff[(5 * r + 1) * (output_H_ * output_W_ * C) + idx] += delta_dpx;
		dTheta_tmp_diff[(5 * r + 2) * (output_H_ * output_W_ * C) + idx] += delta_dpy;
		dTheta_tmp_diff[(5 * r + 3) * (output_H_ * output_W_ * C) + idx] += scl * (-sin(rot)*d11 + cos(rot)*d12 - cos(rot)*d21 - sin(rot)*d22);
		dTheta_tmp_diff[(5 * r + 4) * (output_H_ * output_W_ * C) + idx] += (cos(rot)*d11 + sin(rot)*d12 - sin(rot)*d21 + cos(rot)*d22);
	}
}

template <typename Dtype>
void ROITransOldLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROI Trans Layer:: Forward_gpu:\t";
	const Dtype* U = bottom[0]->gpu_data();
	const Dtype* rois_trans = bottom[1]->gpu_data();
	const Dtype* patch_mask_data = patch_mask.gpu_data();

	Dtype* V = top[0]->mutable_gpu_data();
	Dtype* input_grid_data = input_grid.mutable_gpu_data();
	Dtype* theta_data = theta.mutable_gpu_data();

	caffe_gpu_set(theta.count(), (Dtype)0, theta_data);
	caffe_gpu_set(input_grid.count(), (Dtype)0, input_grid_data);
	caffe_gpu_set(top[0]->count(), (Dtype)0, V);

	const int nthreads = bottom[1]->num();
	ComputeTransParam<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, rois_trans, theta_data, bottom[1]->offset(1));
	if(debug) std::cout << prefix << "ComputeTransParam Finish" << std::endl;

	const int count = top[0]->count(); // n * C * output_H_ * output_W_
	ROITransOldForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(count), CAFFE_CUDA_NUM_THREADS >> >(count, patch_mask_data, rois_trans, theta_data, bottom[1]->offset(1), input_grid_data, U, V, C, output_H_, output_W_, H, W);
	if(debug) std::cout << prefix << "ROITransOldForwardGPU Finish" << std::endl;
	CUDA_POST_KERNEL_CHECK;

	/////
	std::cout << "ROITransOld" << std::endl;
	const Dtype* theta_data_cpu = theta.cpu_data();
	for (int i = 0; i < theta.count(); i++) {
		std::cout << theta_data_cpu[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "--input_grid--" << std::endl;
	const Dtype* input_grid_data_cpu = input_grid.cpu_data();
	for (int i = 0; i < input_grid.count(); i++) {
		std::cout << input_grid_data_cpu[i] << " ";
		if (i % 2 == 1)
			std::cout << std::endl;
		//if (i % output_W_ == (output_W_-1)) 
		//	std::cout << std::endl;
		//if (i % (output_H_ * output_W_) == (output_H_ * output_W_ - 1))
		//	std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "--patch_mask--" << std::endl;
	/////
}

template <typename Dtype>
void ROITransOldLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t ROI Trans Layer:: Backward_gpu:\t";
	// currently only implement dU 
	const Dtype* dV = top[0]->gpu_diff();
	const Dtype* input_grid_data = input_grid.gpu_data();
	const Dtype* rois_trans = bottom[1]->gpu_data();

	//// calculate dTheta 
	//const Dtype* U = bottom[0]->gpu_data();
	//const Dtype* patch_mask_data = patch_mask.gpu_data();
	//Dtype* dTheta_tmp_diff = dTheta_tmp.mutable_gpu_diff();
	//Dtype* dTheta = bottom[1]->mutable_gpu_diff();

	//caffe_gpu_set(dTheta_tmp.count(), (Dtype)0., dTheta_tmp_diff);
	//caffe_gpu_set(bottom[1]->count(), (Dtype)0., dTheta);
	//const int count = top[0]->count(); // n * C * output_H_ * output_W_
	//ROITransOldBackwardGPU_dTheta<Dtype> << <CAFFE_GET_BLOCKS(count), CAFFE_CUDA_NUM_THREADS >> >(count, dV, U, dTheta_tmp_diff, input_grid_data, rois_trans, bottom[1]->offset(1), C, output_H_, output_W_, H, W, patch_mask_data);

	///// for debug 
	//std::cout << "rois_num:" << bottom[1]->num() << " dTheta_tmp_count:" << dTheta_tmp.count() << std::endl;
	//const Dtype* dTheta_temptemp = dTheta_tmp.cpu_diff();
	//for (int i = 0; i < dTheta_tmp.count(); i++) {
	//	std::cout << dTheta_temptemp[i] << " ";
	//	if (i%(C*output_H_*output_W_) == (C*output_H_*output_W_-1))
	//		std::cout << std::endl;
	//}
	/////

	//Dtype* all_ones_2_data = all_ones_2.mutable_gpu_data();
	//caffe_gpu_set(all_ones_2.count(), (Dtype)1., all_ones_2_data);

	//caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, bottom[1]->count(), 1, output_H_ * output_W_ * C,
	//		(Dtype)1., dTheta_tmp_diff, all_ones_2_data, (Dtype)0., dTheta);

	// calculate dU
	Dtype* dU = bottom[0]->mutable_gpu_diff();
	caffe_gpu_set(bottom[0]->count(), (Dtype)0., dU);
	const int nthreads = top[0]->count(); // n * C * output_H_ * output_W_
	ROITransOldBackwardGPU_dU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, dV, dU, input_grid_data, rois_trans, bottom[1]->offset(1), C, output_H_, output_W_, H, W);

	if(debug) std::cout << prefix << "ROITransOldBackwardGPU_dU Finish!!" << std::endl;
	CUDA_POST_KERNEL_CHECK;
}


INSTANTIATE_LAYER_GPU_FUNCS(ROITransOldLayer);
} // namespace caffe 





