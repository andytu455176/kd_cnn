#include "caffe/layers/roi_trans_old_layer.hpp"
#include "cmath"

namespace caffe {
template <typename Dtype>
void ROITransOldLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
		const vector<Blob<Dtype>*>& top) {	

	debug = false;

	string prefix = "\t\t ROI Trans Layer:: LayerSetUp:\t";
	output_H_ = this->layer_param_.roit_old_param().output_h();
	output_W_ = this->layer_param_.roit_old_param().output_w();
	if(debug) std::cout << prefix << "output_H_=" << output_H_ << " output_W_=" << output_W_ << std::endl;

	// initialize for patch_mask
	vector<int> patch_mask_shape(2);
	patch_mask_shape[0] = output_H_ * output_W_; patch_mask_shape[1] = 3;
	patch_mask.Reshape(patch_mask_shape);

	Dtype* data = patch_mask.mutable_cpu_data();
	for (int i = 0; i < output_H_*output_W_; i++) {
		data[3*i] = (i / output_W_) - output_H_ / 2;
		data[3*i+1] = (i % output_W_) - output_W_ / 2;
		data[3*i+2] = 1;
		//std::cout << "(" << data[3*i] << "," << data[3*i+1] << ")" << std::endl;
	}
	
	// initialize for input_grid
	vector<int> input_grid_shape(3);
	input_grid_shape[0] = bottom[1]->shape(0); input_grid_shape[1] = output_H_*output_W_; input_grid_shape[2] = 2;
	input_grid.Reshape(input_grid_shape);
	
	// initialize for theta 
	vector<int> theta_shape(2);
	theta_shape[0] = bottom[1]->shape(0); theta_shape[1] = 6;
	theta.Reshape(theta_shape);

	if(debug) std::cout << prefix << "Initialization finish!!" << std::endl;
}

template <typename Dtype>
void ROITransOldLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	/**
	 * shape of bottom[0] = (N, C, H, W)
	 * shape of bottom[1] = (n, 5), for n = num_of_rois, (0, (1, 2), 3, 4) = (batch_id, center, angle, scale)
	 * */
	string prefix = "\t\t ROI Trans Layer:: Reshape:\t";
	N = bottom[0]->shape(0);
	C = bottom[0]->shape(1);
	H = bottom[0]->shape(2);
	W = bottom[0]->shape(3);
	
	// reshape output (top[0])
	vector<int> shape(4);
	shape[0] = bottom[1]->num(); shape[1] = C; shape[2] = output_H_; shape[3] = output_W_;
	top[0]->Reshape(shape);
	
	// initialize for input_grid
	vector<int> input_grid_shape(3);
	input_grid_shape[0] = bottom[1]->shape(0); input_grid_shape[1] = output_H_*output_W_; input_grid_shape[2] = 2;
	input_grid.Reshape(input_grid_shape);
	
	// initialize for theta 
	vector<int> theta_shape(2);
	theta_shape[0] = bottom[1]->shape(0); theta_shape[1] = 6;
	theta.Reshape(theta_shape);

	// reshape dTheta_tmp
	vector<int> dTheta_tmp_shape(4);
	dTheta_tmp_shape[0] = bottom[1]->num();
	dTheta_tmp_shape[1] = 1;
	dTheta_tmp_shape[2] = 5;
	dTheta_tmp_shape[3] = C * output_H_ * output_W_;
	dTheta_tmp.Reshape(dTheta_tmp_shape);

	// reshape all_ones_2
	vector<int> all_ones_2_shape(1);
	all_ones_2_shape[0] = C * output_H_ * output_W_;
	all_ones_2.Reshape(all_ones_2_shape);
	
	if(debug) std::cout << prefix << "top: (N,C,H,W) = " << "(" << N << "," << C << "," << H << "," << W << ")" << std::endl;
	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

template <typename Dtype>
void ROITransOldLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROI Trans Layer:: Forward_cpu:\t";
	const Dtype* U = bottom[0]->cpu_data();
	const Dtype* rois_trans = bottom[1]->cpu_data();
	const Dtype* patch_mask_data = patch_mask.cpu_data();

	Dtype* V = top[0]->mutable_cpu_data();
	Dtype* input_grid_data = input_grid.mutable_cpu_data();
	Dtype* theta_data = theta.mutable_cpu_data();

	int num_rois = bottom[1]->num();
	if(debug) std::cout << prefix << "num_rois: " << num_rois << std::endl;

	caffe_set(theta.count(), (Dtype)0, theta_data);
	caffe_set(input_grid.count(), (Dtype)0, input_grid_data);
	caffe_set(top[0]->count(), (Dtype)0, V);

	for (int n = 0; n < num_rois; n++) {
		int batch_id = rois_trans[0];
		Dtype* coordinate = input_grid_data + (output_H_*output_W_ * 2) * n;
		Dtype* trans_param = theta_data + 6 * n;

		// fill the transformation matrix for patch mask
		trans_param[0] = rois_trans[4]*cos(rois_trans[3]); trans_param[1] = rois_trans[4]*sin(rois_trans[3]); 
		trans_param[2] = rois_trans[1];
		trans_param[3] = -rois_trans[4]*sin(rois_trans[3]); trans_param[4] = rois_trans[4]*cos(rois_trans[3]); 
		trans_param[5] = rois_trans[2];

		caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasTrans, output_H_*output_W_, 2, 3, (Dtype)1., patch_mask_data, trans_param, (Dtype)0., coordinate);

		int row_idx; Dtype px, py;
		for (int c = 0; c < C; c++) {
			for (int s = 0; s < output_H_; s++) {
				for (int t = 0; t < output_W_; t++) {
					row_idx = output_W_ * s + t;
					px = coordinate[row_idx * 2];
					py = coordinate[row_idx * 2 + 1];
					V[top[0]->offset(n, c, s, t)] = transform_forward_cpu(U + bottom[0]->offset(batch_id, c, 0, 0), px, py);
				}
			}
		}
		rois_trans += bottom[1]->offset(1);
	}
}

template<typename Dtype>
Dtype ROITransOldLayer<Dtype>::transform_forward_cpu(const Dtype* pic, Dtype px, Dtype py) {
	string prefix = "\t\t ROI Trans Layer:: transform_forward_cpu:\t";

	Dtype res = (Dtype)0.;

	int m, n; Dtype w;
	m = floor(px); n = floor(py); w = 0;
	if(m >= 0 && m < H && n >= 0 && n < W) {
		w = max(0, 1 - abs(px - m)) * max(0, 1 - abs(py - n));
		res += w * pic[m * W + n];
	}

	m = floor(px) + 1; n = floor(py); w = 0;
	if(m >= 0 && m < H && n >= 0 && n < W) {
		w = max(0, 1 - abs(px - m)) * max(0, 1 - abs(py - n));
		res += w * pic[m * W + n];
	}

	m = floor(px); n = floor(py) + 1; w = 0;
	if(m >= 0 && m < H && n >= 0 && n < W) {
		w = max(0, 1 - abs(px - m)) * max(0, 1 - abs(py - n));
		res += w * pic[m * W + n];
	}

	m = floor(px) + 1; n = floor(py) + 1; w = 0;
	if(m >= 0 && m < H && n >= 0 && n < W) {
		w = max(0, 1 - abs(px - m)) * max(0, 1 - abs(py - n));
		res += w * pic[m * W + n];
	}

	return res;
}

#ifdef CPU_ONLY
STUB_GPU(ROITransOldLayer);
#endif 

INSTANTIATE_CLASS(ROITransOldLayer);
REGISTER_LAYER_CLASS(ROITransOld);
} // namespace caffe 











