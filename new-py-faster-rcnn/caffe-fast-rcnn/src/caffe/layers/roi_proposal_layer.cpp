#include "caffe/layers/roi_proposal_layer.hpp"
#include "cmath"

namespace caffe {

template <typename Dtype>
void ROIProposalLayer<Dtype>::LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	debug = false;
	string prefix = "\t\t ROI Proposal Layer:: LayerSetUp:\t";
	if (this->layer_param_.roip_param().orient_type() == "hard_argmax") {
		orient_type_ = "hard_argmax"; // given index and return the orient in degree

		vector<int> orient_bin_shape(1);
		orient_bin_shape[0] = 36;
		orient_bin_.Reshape(orient_bin_shape);
		Dtype* orient_bin_data = orient_bin_.mutable_cpu_data();
		for (int i = 0; i < orient_bin_.count(); i++) {
			orient_bin_data[i] = Dtype((-175 + i * 10)) * Dtype(M_PI / 180.0);
		}
	}
	else if (this->layer_param_.roip_param().orient_type() == "soft_argmax") {
		orient_type_ = "soft_argmax"; // given orient in degree directly
	}
	else {
		CHECK(false) << prefix << "Orient type " << this->layer_param_.roip_param().orient_type() << " not defined!" << std::endl;
	}

	if (this->layer_param_.roip_param().scale_type() == "hard_argmax") {
		scale_type_ = "hard_argmax";

		vector<int> scale_bin_shape(1);
		scale_bin_shape[0] = 8;
		scale_bin_.Reshape(scale_bin_shape);
		Dtype* scale_bin_data = scale_bin_.mutable_cpu_data();
		for (int i = 0; i < scale_bin_.count(); i++) {
			scale_bin_data[i] = Dtype(1.6*pow(2.0, Dtype(i)/4.0)*0.3);
		}
	}
	else if (this->layer_param_.roip_param().scale_type() == "soft_argmax") {
		scale_type_ = "soft_argmax";
	}
	else {
		CHECK(false) << prefix << "Scale type " << this->layer_param_.roip_param().scale_type() << " not defined!" << std::endl;
	}

	coor_scale_ = this->layer_param_.roip_param().coor_scale();
	CHECK(bottom[1]->shape() == bottom[2]->shape()) << "ROIProposalLayer need bottom[1] and bottom[2] have same shape";
	if (debug) std::cout << prefix << "Initialization finish!!" << std::endl;
}

template <typename Dtype>
void ROIProposalLayer<Dtype>::Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	// bottom[0]: keypoint proposal (n, 3)
	// bottom[1]: orient map
	// bottom[2]: scale map
	// top[0]: rot_scl_trans_input (n, 5)
	string prefix = "\t\t ROI Proposal Layer:: Reshape:\t";
	// reshape top
	vector<int> top_shape(4);
	top_shape[0] = bottom[0]->num();
	top_shape[1] = 5; // (batch_id, center_x, center_y, angle, scale)
	top_shape[2] = 1;
	top_shape[3] = 1;
	top[0]->Reshape(top_shape);
	if (debug) std::cout << prefix << "Finish!!" << std::endl; 
}

template <typename Dtype>
void ROIProposalLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROI Proposal Layer:: Forward_cpu:\t";
	Dtype* top_data = top[0]->mutable_cpu_data();

	for (int i = 0; i < bottom[0]->num(); i++) {
		int idx = bottom[0]->data_at(i, 0, 0, 0);
		Dtype x = bottom[0]->data_at(i, 1, 0, 0) * coor_scale_;
		Dtype y = bottom[0]->data_at(i, 2, 0, 0) * coor_scale_;
		top_data[top[0]->offset(i, 0, 0, 0)] = idx;
		top_data[top[0]->offset(i, 1, 0, 0)] = x;
		top_data[top[0]->offset(i, 2, 0, 0)] = y;

		if (orient_type_ == "hard_argmax") {
			const Dtype* orient_bin_data = orient_bin_.cpu_data();
			top_data[top[0]->offset(i, 3, 0, 0)] = orient_bin_data[int(bottom[1]->data_at(idx, 0, int(x), int(y)))];
		}
		else if (orient_type_ == "soft_argmax")
			top_data[top[0]->offset(i, 3, 0, 0)] = bottom[1]->data_at(idx, 0, int(x), int(y));

		if (scale_type_ == "hard_argmax") {
			const Dtype* scale_bin_data = scale_bin_.cpu_data();
			top_data[top[0]->offset(i, 4, 0, 0)] = scale_bin_data[int(bottom[2]->data_at(idx, 0, int(x), int(y)))];
		}
		else if (scale_type_ == "soft_argmax")
			top_data[top[0]->offset(i, 4, 0, 0)] = bottom[2]->data_at(idx, 0, int(x), int(y));
	}

	if (debug) std::cout << prefix << "Finish!!" << std::endl;
}

template <typename Dtype>
void ROIProposalLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	// only the orient_map gradient will be propagated currently
	string prefix = "\t\t ROI Proposal Layer:: Backward_cpu:\t";
	Dtype* orient_map_diff = bottom[1]->mutable_cpu_diff();
	Dtype* scale_map_diff = bottom[2]->mutable_cpu_diff();

	caffe_set<Dtype>(bottom[1]->count(), Dtype(0), orient_map_diff);
	caffe_set<Dtype>(bottom[2]->count(), Dtype(0), scale_map_diff);

	// only soft_argmax needs to be backpropagated
	for (int i = 0; i < bottom[0]->num(); i++) {
		int idx = bottom[0]->data_at(i, 0, 0, 0);
		Dtype x = bottom[0]->data_at(i, 1, 0, 0) * coor_scale_;
		Dtype y = bottom[0]->data_at(i, 2, 0, 0) * coor_scale_;

		if (orient_type_ == "soft_argmax") 
			orient_map_diff[bottom[1]->offset(idx, 0, int(x), int(y))] = top[0]->diff_at(i, 3, 0, 0);
		if (scale_type_ == "soft_argmax") 
			scale_map_diff[bottom[2]->offset(idx, 0, int(x), int(y))] = top[0]->diff_at(i, 4, 0, 0);
	}
	if (debug) std::cout << prefix << "Finish!!" << std::endl;
}

#ifdef CPU_ONLY
STUB_GPU(ROIProposalLayer);
#endif 

INSTANTIATE_CLASS(ROIProposalLayer);
REGISTER_LAYER_CLASS(ROIProposal);

} // namespace caffe 


