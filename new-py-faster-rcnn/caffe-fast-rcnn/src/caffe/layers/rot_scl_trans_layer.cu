#include "caffe/layers/rot_scl_trans_layer.hpp"
#include "cmath"

namespace caffe {

template <typename Dtype>
__global__ void ROTSCLTransForwardGPU(const int nthreads, const Dtype* bottom_data, Dtype* top_data) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		int b_offset = 5;
		int t_offset = 7;
		Dtype idx = bottom_data[index * b_offset + 0];
		Dtype x = bottom_data[index * b_offset + 1];
		Dtype y = bottom_data[index * b_offset + 2];
		Dtype rot = bottom_data[index * b_offset + 3];
		Dtype scl = bottom_data[index * b_offset + 4];
		top_data[index * t_offset + 0] = idx;
		top_data[index * t_offset + 1] = scl * cos(rot);
		top_data[index * t_offset + 2] = scl * sin(rot);
		top_data[index * t_offset + 3] = x;
		top_data[index * t_offset + 4] = -scl * sin(rot);
		top_data[index * t_offset + 5] = scl * cos(rot);
		top_data[index * t_offset + 6] = y;
	}
}

template <typename Dtype>
__global__ void ROTSCLTransBackwardGPU(const int nthreads, const Dtype* bottom_data, const Dtype* top_diff, Dtype* bottom_diff) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		int b_offset = 5;
		int t_offset = 7;
		Dtype s = sin(bottom_data[index * b_offset + 3]);
		Dtype c = cos(bottom_data[index * b_offset + 3]);
		Dtype scl = bottom_data[index * b_offset + 4];
		Dtype t1 = top_diff[index * t_offset + 1];
		Dtype t2 = top_diff[index * t_offset + 2];
		Dtype t4 = top_diff[index * t_offset + 4];
		Dtype t5 = top_diff[index * t_offset + 5];
		bottom_diff[index * b_offset + 0] = top_diff[index * t_offset + 0];
		bottom_diff[index * b_offset + 1] = top_diff[index * t_offset + 3];
		bottom_diff[index * b_offset + 2] = top_diff[index * t_offset + 6];
		bottom_diff[index * b_offset + 3] = scl * (-s*t1 + c*t2 - c*t4 - s*t5);
		bottom_diff[index * b_offset + 4] = c*t1 + s*t2 - s*t4 + c*t5;
	}
}

template <typename Dtype>
void ROTSCLTransLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROT SCL Trans Layer:: Forward_gpu:\t";
	const Dtype* bottom_data = bottom[0]->gpu_data();
	Dtype* top_data = top[0]->mutable_gpu_data();
	
	const int nthreads = N_;
	ROTSCLTransForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, bottom_data, top_data);
	CUDA_POST_KERNEL_CHECK;
	if (debug) std::cout << prefix << "Finsh!!" << std::endl;
}

template <typename Dtype>
void ROTSCLTransLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t ROT SCL Trans Layer:: Backward_gpu:\t";
	const Dtype* bottom_data = bottom[0]->gpu_data();
	const Dtype* top_diff = top[0]->gpu_diff();
	Dtype* bottom_diff = bottom[0]->mutable_gpu_diff();

	const int nthreads = N_;
	ROTSCLTransBackwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, bottom_data, top_diff, bottom_diff);
	CUDA_POST_KERNEL_CHECK;
	if (debug) std::cout << prefix << "Finsh!!" << std::endl;
}

INSTANTIATE_LAYER_GPU_FUNCS(ROTSCLTransLayer);
} // namespace caffe 





