#include "caffe/layers/roi_trans_layer.hpp"
#include "caffe/util/gpu_util.cuh"
#include "cmath"

namespace caffe {

template <typename Dtype>
__global__ void ComputeSourceGPU(const int nthreads, const int H_, const int W_, const int output_H_, const int output_W_, const Dtype* theta_data, const Dtype* patch_mask_data, Dtype* input_grid_data, int* input_range_data) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		// num_rois * output_H_ * output_W_
		int map_size = output_H_ * output_W_;
		int r = index / map_size;
		int div = map_size / output_H_;
		int h = (index % map_size) / div;
		int w = (index % map_size) % div;

		Dtype tx = patch_mask_data[h * output_W_ + w];
		Dtype ty = patch_mask_data[h * output_W_ + w + map_size];

		int theta_offset = 7 * r;
		Dtype x = theta_data[theta_offset + 1] * tx + theta_data[theta_offset + 2] * ty + theta_data[theta_offset + 3];
		Dtype y = theta_data[theta_offset + 4] * tx + theta_data[theta_offset + 5] * ty + theta_data[theta_offset + 6];

		int input_grid_offset = r * map_size * 2 + h * output_W_ + w;
		input_grid_data[input_grid_offset] = x;
		input_grid_data[input_grid_offset + map_size] = y;

		//int w_min = (floor(x) > 0) ? floor(x) : 0;
        //int w_max = (ceil(x) < W_ - 1) ? ceil(x) : (W_ - 1);
        //int h_min = (floor(y) > 0) ? floor(y) : 0;
        //int h_max = (ceil(y) < H_ - 1) ? ceil(y) : (H_ - 1);
		int w_min = (floor(y) > 0) ? floor(y) : 0;
        int w_max = (ceil(y) < W_ - 1) ? ceil(y) : (W_ - 1);
        int h_min = (floor(x) > 0) ? floor(x) : 0;
        int h_max = (ceil(x) < H_ - 1) ? ceil(x) : (H_ - 1);
		int input_range_offset = (r * map_size + h * output_W_ + w) * 4;
		input_range_data[input_range_offset] = w_min;
		input_range_data[input_range_offset + 1] = w_max;
		input_range_data[input_range_offset + 2] = h_min;
		input_range_data[input_range_offset + 3] = h_max;
	}
}

template <typename Dtype>
__global__ void ROITransForwardGPU(const int nthreads, const int H_, const int W_, const int output_H_, const int output_W_, const int C_, const Dtype* U, const Dtype* theta_data, const Dtype* input_grid_data, const int* input_range_data, Dtype* V) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		// num_rois * C_ * output_H_ * output_W_
		int c_map_size = C_ * output_H_ * output_W_;
        int r = index / c_map_size;
        int rem = index % c_map_size;
        int map_size = output_H_ * output_W_;
        int c = rem / map_size;
        rem = rem % map_size;
        int h = rem / output_W_;
        int w = rem % output_W_;

		int input_grid_offset = r * map_size * 2 + h * output_W_ + w;
        Dtype x = input_grid_data[input_grid_offset];
        Dtype y = input_grid_data[input_grid_offset + map_size];

        int input_range_offset = (r * map_size + h * output_W_ + w) * 4;
        int w_min = input_range_data[input_range_offset];
        int w_max = input_range_data[input_range_offset + 1];
        int h_min = input_range_data[input_range_offset + 2];
        int h_max = input_range_data[input_range_offset + 3];

        int V_offset = r * c_map_size + c * map_size;
		int U_offset = theta_data[7 * r] * C_ * H_ * W_ + c * H_ * W_;
        Dtype res = 0.;
        for (int hh = h_min; hh <= h_max; ++hh) {
            for (int ww = w_min; ww <= w_max; ++ww) {
                // res += U[U_offset + hh * W_ + ww] * (1 - fabs(x - ww)) * (1 - fabs(y - hh));
                res += U[U_offset + hh * W_ + ww] * (1 - fabs(x - hh)) * (1 - fabs(y - ww));
            }
        }
        V[V_offset + h * output_W_ + w] = res;
	}
}

template <typename Dtype> 
__global__ void ROITransBackwardGPU(const int nthreads, const int num_rois, const int H_, const int W_, const int output_H_, const int output_W_, const int C_, const Dtype* U, const Dtype* dV, const Dtype* theta_data, const Dtype* input_grid_data, const int* input_range_data, Dtype* dU, Dtype* input_grad_cache_data) {
	CUDA_KERNEL_LOOP(index, nthreads) {
		// num_rois * C_ * output_H_ * output_W_
		int c_map_size = C_ * output_H_ * output_W_;
        int r = index / c_map_size;
        int rem = index % c_map_size;
        int map_size = output_H_ * output_W_;
        int c = rem / map_size;
        rem = rem % map_size;
        int h = rem / output_W_;
        int w = rem % output_W_;

		int input_grid_offset = r * map_size * 2 + h * output_W_ + w;
        Dtype x = input_grid_data[input_grid_offset];
        Dtype y = input_grid_data[input_grid_offset + map_size];

	    int input_range_offset = (r * map_size + h * output_W_ + w) * 4;
        int w_min = input_range_data[input_range_offset]; // y
        int w_max = input_range_data[input_range_offset + 1];
        int h_min = input_range_data[input_range_offset + 2]; // x
        int h_max = input_range_data[input_range_offset + 3];

		int input_grad_cache_offset_x = c * num_rois * 2 * output_H_ * output_W_ + r * 2 * output_H_ * output_W_ + h * output_W_ + w;
		int input_grad_cache_offset_y = input_grad_cache_offset_x + map_size;
		Dtype input_grad_x = 0;
		Dtype input_grad_y = 0;
		Dtype dv = dV[r * c_map_size + c * map_size + h * output_W_ + w];
		for (int hh = h_min; hh <= h_max; hh++) {
			for (int ww = w_min; ww <= w_max; ww++) {
				//int sign_x = (Dtype(0) <= Dtype(ww - x)) - (Dtype(ww - x) < Dtype(0));
				//int sign_y = (Dtype(0) <= Dtype(hh - y)) - (Dtype(hh - y) < Dtype(0));
				int sign_x = (Dtype(0) <= Dtype(hh - x)) - (Dtype(hh - x) < Dtype(0));
				int sign_y = (Dtype(0) <= Dtype(ww - y)) - (Dtype(ww - y) < Dtype(0));
				int U_offset = theta_data[7 * r] * C_ * H_ * W_ + c * H_ * W_ + hh * W_ + ww;
				Dtype Udv = dv * U[U_offset];
				//Dtype tmp_hh = 1 - fabs(y - hh);
				//Dtype tmp_ww = 1 - fabs(x - ww);
				Dtype tmp_hh = 1 - fabs(x - hh);
				Dtype tmp_ww = 1 - fabs(y - ww);
				// compute dx and dy
				//input_grad_x += Udv * sign_x * tmp_hh;
				//input_grad_y += Udv * sign_y * tmp_ww;
				input_grad_x += Udv * sign_x * tmp_ww;
				input_grad_y += Udv * sign_y * tmp_hh;
				// compute dU
				Dtype inc = dv * tmp_hh * tmp_ww;
				caffe_gpu_atomic_add(inc, dU + U_offset);
			}
		}
		input_grad_cache_data[input_grad_cache_offset_x] = input_grad_x;
		input_grad_cache_data[input_grad_cache_offset_y] = input_grad_y;
	}
}

template <typename Dtype>
void ROITransLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROI Trans Layer:: Forward_gpu:\t";
	const Dtype* patch_mask_data = patch_mask_.gpu_data();
	const Dtype* U = bottom[0]->gpu_data();
	const Dtype* theta_data = bottom[1]->gpu_data();
	Dtype* input_grid_data = input_grid_.mutable_gpu_data();
	int* input_range_data = input_range_.mutable_gpu_data();
	Dtype* V = top[0]->mutable_gpu_data();

	caffe_gpu_set<Dtype>(top[0]->count(), (Dtype)0, V);

	const int nthreads = top[0]->num() * output_H_ * output_W_;
	ComputeSourceGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, H_, W_, output_H_, output_W_, theta_data,patch_mask_data, input_grid_data, input_range_data);

	const int count = top[0]->count();
	ROITransForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(count), CAFFE_CUDA_NUM_THREADS >> >(count, H_, W_, output_H_, output_W_, C_, U, theta_data, input_grid_data, input_range_data, V);
	CUDA_POST_KERNEL_CHECK;
	if(debug) std::cout << prefix << "Finish!!" << std::endl;
	/////
	//std::cout << "ROITrans" << std::endl;
	//std::cout << "--theta--" << std::endl;
	//const Dtype* theta_data_cpu = bottom[1]->cpu_data();
	//for (int i = 0; i < bottom[1]->count(); i++) {
	//	std::cout << theta_data_cpu[i] << " ";
	//}
	//std::cout << std::endl;
	//std::cout << "--input_grid--" << std::endl;
	//const Dtype* inpu_grid_data_cpu = input_grid_.cpu_data();
	//for (int i = 0; i < input_grid_.num(); i++) {
	//	for (int j = 0; j < input_grid_.channels(); j++) {
	//		for (int h = 0; h < input_grid_.height(); h++) {
	//			for (int w = 0; w < input_grid_.width(); w++) {
	//				std::cout << input_grid_.data_at(i, j, h, w) << " ";
	//			}
	//			std::cout << std::endl;
	//		}
	//		std::cout << std::endl;
	//	}
	//	std::cout << std::endl;
	//}
	//std::cout << std::endl;
	/////
}

template <typename Dtype>
void ROITransLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t ROI Trans Layer:: Backward_gpu:\t";
	const Dtype* U = bottom[0]->gpu_data();
	const Dtype* theta_data = bottom[1]->gpu_data();
	const Dtype* dV = top[0]->gpu_diff();
	const Dtype* input_grid_data = input_grid_.gpu_data();
	const int* input_range_data = input_range_.gpu_data();
	const Dtype* patch_mask_data = patch_mask_.gpu_data();
	Dtype* input_grad_cache_data = input_grad_cache_.mutable_gpu_data();
	Dtype* input_grid_diff = input_grid_.mutable_gpu_diff();
	Dtype* dU = bottom[0]->mutable_gpu_diff();
	Dtype* dTheta = bottom[1]->mutable_gpu_diff();

	caffe_gpu_set<Dtype>(bottom[0]->count(), (Dtype)0, dU);
	caffe_gpu_set<Dtype>(bottom[1]->count(), (Dtype)0, dTheta);

	const int nthreads = top[0]->count();
	ROITransBackwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, bottom[1]->num(), H_, W_, output_H_, output_W_, C_, U, dV, theta_data, input_grid_data, input_range_data, dU, input_grad_cache_data);

	caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, 1, bottom[1]->num() * 2 * map_size_, C_,
                Dtype(1), all_ones_.gpu_data(), input_grad_cache_data, Dtype(0), input_grid_diff);
	
	int theta_offset = 7;
	for (int index = 0; index < bottom[1]->num(); index++) {
		caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasTrans, 2, 3, map_size_,
				Dtype(1), input_grid_diff + index * 2 * map_size_, patch_mask_data, Dtype(0), dTheta + index * theta_offset + 1);
	}
	CUDA_POST_KERNEL_CHECK;
	if(debug) std::cout << prefix << "Finish!!" << std::endl;
}

INSTANTIATE_LAYER_GPU_FUNCS(ROITransLayer);
} // namespace caffe 









