#include "caffe/layers/roi_proposal_layer.hpp"
#include "caffe/util/gpu_util.cuh"
#include "cmath"

namespace caffe {

template <typename Dtype>
__global__ void ROIProposalForwardGPU(const int nthreads, const Dtype* keypoint_proposal, const Dtype* orient_map, const Dtype* scale_map, Dtype* top_data, const int H_, const int W_, const int orient_type, const Dtype* orient_bin_data, const int scale_type, const Dtype* scale_bin_data, Dtype coor_scale) {
	// num_rois (bottom[0]->num())
	CUDA_KERNEL_LOOP(index, nthreads) {
		int keypoint_proposal_offset = 3;
		int top_offset = 5;
		int map_size = H_ * W_;

		int idx = keypoint_proposal[index * keypoint_proposal_offset + 0];
		Dtype x = keypoint_proposal[index * keypoint_proposal_offset + 1] * coor_scale;
		Dtype y = keypoint_proposal[index * keypoint_proposal_offset + 2] * coor_scale;

		top_data[index * top_offset + 0] = keypoint_proposal[index * keypoint_proposal_offset + 0];
		top_data[index * top_offset + 1] = x;
		top_data[index * top_offset + 2] = y;

		if (orient_type == 0) // hard_argmax
			top_data[index * top_offset + 3] = orient_bin_data[int(orient_map[idx * map_size + int(x) * W_ + int(y)])];
		else if (orient_type == 1) // soft_argmax
			top_data[index * top_offset + 3] = orient_map[idx * map_size + int(x) * W_ + int(y)];

		if (scale_type == 0) // hard_argmax
			top_data[index * top_offset + 4] = scale_bin_data[int(scale_map[idx * map_size + int(x) * W_ + int(y)])];
		else if (scale_type == 1) // soft_argmax
			top_data[index * top_offset + 4] = scale_map[idx * map_size + int(x) * W_ + int(y)];
	}
}

template <typename Dtype>
__global__ void ROIProposalBackwardGPU(const int nthreads, const Dtype* keypoint_proposal, const int H_, const int W_, const Dtype* top_diff, Dtype* orient_map_diff, Dtype* scale_map_diff, Dtype coor_scale, const int orient_type, const int scale_type) {
	// num_rois (bottom[0]->num())
	CUDA_KERNEL_LOOP(index, nthreads) {
		int keypoint_proposal_offset = 3;
		int top_offset = 5;
		int map_size = H_ * W_;

		int idx = keypoint_proposal[index * keypoint_proposal_offset + 0];
		Dtype x = keypoint_proposal[index * keypoint_proposal_offset + 1] * coor_scale;
		Dtype y = keypoint_proposal[index * keypoint_proposal_offset + 2] * coor_scale;

		if (orient_type == 1) // soft_argmax
			orient_map_diff[idx * map_size + int(x) * W_ + int(y)] = top_diff[index * top_offset + 3];
		if (scale_type == 1) // soft_argmax
			scale_map_diff[idx * map_size + int(x) * W_ + int(y)] = top_diff[index * top_offset + 4];
	}
}

template <typename Dtype>
void ROIProposalLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
	string prefix = "\t\t ROI Proposal Layer:: Forward_gpu:\t";
	const Dtype* keypoint_proposal = bottom[0]->gpu_data();
	const Dtype* orient_map = bottom[1]->gpu_data();
	const Dtype* scale_map = bottom[2]->gpu_data();
	Dtype* top_data = top[0]->mutable_gpu_data();

	const int nthreads = bottom[0]->num();
	if (orient_type_ == "hard_argmax" && scale_type_ == "hard_argmax") {
		const Dtype* orient_bin_data = orient_bin_.gpu_data();
		const Dtype* scale_bin_data = scale_bin_.gpu_data();
		ROIProposalForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, keypoint_proposal, orient_map, scale_map, top_data, bottom[1]->height(), bottom[1]->width(), 0, orient_bin_data, 0, scale_bin_data, coor_scale_);
	}
	else if (orient_type_ == "soft_argmax" && scale_type_ == "hard_argmax") {
		const Dtype* scale_bin_data = scale_bin_.gpu_data();
		ROIProposalForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, keypoint_proposal, orient_map, scale_map, top_data, bottom[1]->height(), bottom[1]->width(), 1, NULL, 0, scale_bin_data, coor_scale_);
	}
	else if (orient_type_ == "hard_argmax" && scale_type_ == "soft_argmax") {
		const Dtype* orient_bin_data = orient_bin_.gpu_data();
		ROIProposalForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, keypoint_proposal, orient_map, scale_map, top_data, bottom[1]->height(), bottom[1]->width(), 0, orient_bin_data, 1, NULL, coor_scale_);
	}
	else if (orient_type_ == "soft_argmax" && scale_type_ == "soft_argmax") {
		ROIProposalForwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, keypoint_proposal, orient_map, scale_map, top_data, bottom[1]->height(), bottom[1]->width(), 1, NULL, 1, NULL, coor_scale_);
	}
	CUDA_POST_KERNEL_CHECK;
	if (debug) std::cout << prefix << "Finish!!" << std::endl;
}

template <typename Dtype>
void ROIProposalLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
	string prefix = "\t\t ROI Proposal Layer:: Backward_gpu:\t";
	const Dtype* keypoint_proposal = bottom[0]->gpu_data();
	const Dtype* top_diff = top[0]->gpu_diff();
	Dtype* orient_map_diff = bottom[1]->mutable_gpu_diff();
	Dtype* scale_map_diff = bottom[2]->mutable_gpu_diff();

	caffe_gpu_set<Dtype>(bottom[1]->count(), Dtype(0), orient_map_diff);
	caffe_gpu_set<Dtype>(bottom[2]->count(), Dtype(0), scale_map_diff);

	if (orient_type_ == "soft_argmax" || scale_type_ == "soft_argmax") {
		const int nthreads = bottom[0]->num();
		const int orient_mode = (orient_type_ == "soft_argmax")? 1: 0;
		const int scale_mode = (scale_type_ == "soft_argmax")? 1: 0;
		ROIProposalBackwardGPU<Dtype> << <CAFFE_GET_BLOCKS(nthreads), CAFFE_CUDA_NUM_THREADS >> >(nthreads, keypoint_proposal, bottom[1]->height(), bottom[1]->width(), top_diff, orient_map_diff, scale_map_diff, coor_scale_, orient_mode, scale_mode);
		CUDA_POST_KERNEL_CHECK;
	}
	if (debug) std::cout << prefix << "Finish!!" << std::endl;
}

INSTANTIATE_LAYER_GPU_FUNCS(ROIProposalLayer);
} // namespace caffe 




