#include <cmath>
#include <cstring>
#include <vector>
#include <iostream>
#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/roi_trans_layer.hpp"

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"


namespace caffe {

	typedef ::testing::Types<CPUDevice<float>, CPUDevice<double> > TestDtypesCPU;
	typedef ::testing::Types<GPUDevice<float>, GPUDevice<double> > TestDtypesGPU;

    // Reference affine transformer for checking results
    // compute coordinates and sample feature map explicitly using loops
    template <typename Dtype>
    void affine_transform(const Blob<Dtype>* in, const Blob<Dtype>* theta, Blob<Dtype>* out, int out_h, int out_w) {
        //int num = in->shape(0);
		int num = out->shape(0);
        int channels = in->shape(1);
        //int height = in->shape(2);
        //int width = in->shape(3);
		int height = out_h;
		int width = out_w;
        Dtype* out_data = out->mutable_cpu_data();
        caffe_set<Dtype>(out->count(), 0, out_data);
        const Dtype* theta_data = theta->cpu_data();
        for (int n = 0; n < num; ++n) {
            for (int h = 0; h < height; ++h) {
                // Dtype ty = h / (Dtype) (height - 1) * (Dtype) 2. - (Dtype) 1.;
				// Dtype ty = h - (Dtype) (out_h) / 2;
                for (int w = 0; w < width; ++w) {
                    // Dtype tx = w / (Dtype) (width - 1)*(Dtype) 2. - (Dtype) 1.;
					// Dtype tx = w - (Dtype) (out_w) / 2;
					Dtype tx = h - (Dtype) (out_h) / 2;
					Dtype ty = w - (Dtype) (out_w) / 2;
                    Dtype sx = tx * theta_data[n * 7 + 1] + ty * theta_data[n * 7 + 2] + theta_data[n * 7 + 3];
                    Dtype sy = tx * theta_data[n * 7 + 4] + ty * theta_data[n * 7 + 5] + theta_data[n * 7 + 6];
					std::cout << "(" << sx << "," << sy << ")" << " ";
                    //sx = (sx + 1.) / (Dtype) 2. * (width - 1);
                    //sy = (sy + 1.) / (Dtype) 2. * (height - 1);
                    for (int c = 0; c < channels; ++c) {
						//std::cout << "test weight and pixel value" << std::endl;
                        for (int hh = 0; hh < in->shape(2); ++hh) {
                            for (int ww = 0; ww < in->shape(3); ++ww) {
                                Dtype max_y = 0;
                                //if (hh > sy) {
                                //    max_y = hh - sy;
                                //} else {
                                //    max_y = sy - hh;
                                //}
                                //if (1 - max_y < 0) {
                                //    max_y = 0;
                                //} else {
                                //    max_y = 1 - max_y;
                                //}
								if (ww > sy) {
                                    max_y = ww - sy;
                                } else {
                                    max_y = sy - ww;
                                }
                                if (1 - max_y < 0) {
                                    max_y = 0;
                                } else {
                                    max_y = 1 - max_y;
                                }
                                //Dtype max_x = 0;
                                //if (ww > sx) {
                                //    max_x = ww - sx;
                                //} else {
                                //    max_x = sx - ww;
                                //}
                                //if (1 - max_x < 0) {
                                //    max_x = 0;
                                //} else {
                                //    max_x = 1 - max_x;
                                //}
								Dtype max_x = 0;
                                if (hh > sx) {
                                    max_x = hh - sx;
                                } else {
                                    max_x = sx - hh;
                                }
                                if (1 - max_x < 0) {
                                    max_x = 0;
                                } else {
                                    max_x = 1 - max_x;
                                }
                                out_data[out->offset(n, c, h, w)] += in->data_at(theta_data[n*7], c, hh, ww) * max_x*max_y;
								//std::cout << "(" << max_x*max_y << "," << in->data_at(n, c, hh, ww) << ") ";
                            }
                        }
						//std::cout << "----------------" << std::endl;
                    }
                }
				std::cout << std::endl;
            }
        }
    }

    template void affine_transform(const Blob<float>* in, const Blob<float>* theta, Blob<float>* out, int out_h, int out_w);

    template void affine_transform(const Blob<double>* in, const Blob<double>* theta, Blob<double>* out, int out_h, int out_w);

    template <typename TypeParam>
    class ROITransLayerTest : public MultiDeviceTest<TypeParam> {
        typedef typename TypeParam::Dtype Dtype;
    protected:

        ROITransLayerTest()
        //: blob_data_(new Blob<Dtype>(vector<int>{2, 3, 5, 9})),
        //blob_theta_(new Blob<Dtype>(vector<int>{2, 6})),
        : blob_data_(new Blob<Dtype>(2, 3, 5, 9)),
        blob_theta_(new Blob<Dtype>(3, 7, 1, 1)),
        blob_top_(new Blob<Dtype>()) {
        }

        virtual void SetUp() {
            FillerParameter filler_param;
            filler_param.set_min(-1);
            filler_param.set_max(1);
            UniformFiller<Dtype> filler(filler_param);
            filler.Fill(this->blob_data_);
            blob_bottom_vec_.push_back(blob_data_);
            blob_bottom_vec_.push_back(blob_theta_);
            blob_top_vec_.push_back(blob_top_);
        }

        virtual ~ROITransLayerTest() {
            delete blob_data_;
            delete blob_theta_;
            delete blob_top_;
        }

        virtual Blob<Dtype>* MakeReferenceTop(Blob<Dtype>* top) {
            this->ref_blob_top_.reset(new Blob<Dtype>());
            this->ref_blob_top_->ReshapeLike(*top);
            return this->ref_blob_top_.get();
        }

        Blob<Dtype> * const blob_data_;
        Blob<Dtype> * const blob_theta_;
        Blob<Dtype> * const blob_top_;
        shared_ptr<Blob<Dtype> > ref_blob_top_;
        vector<Blob<Dtype>*> blob_bottom_vec_;
        vector<Blob<Dtype>*> blob_top_vec_;
    };
    
    TYPED_TEST_CASE(ROITransLayerTest, TestDtypesAndDevices);
	// TYPED_TEST_CASE(ROITransLayerTest, TestDtypesCPU);
	// TYPED_TEST_CASE(ROITransLayerTest, TestDtypesGPU);
    // check top blob shape
    TYPED_TEST(ROITransLayerTest, TestSetUp) {
        typedef typename TypeParam::Dtype Dtype;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        shared_ptr<Layer<Dtype> > layer(
                new ROITransLayer<Dtype>(layer_param));

        layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        EXPECT_EQ(this->blob_top_->num(), this->blob_theta_->num());
        EXPECT_EQ(this->blob_top_->channels(), this->blob_data_->channels());
        //EXPECT_EQ(this->blob_top_->height(), this->blob_data_->height());
        //EXPECT_EQ(this->blob_top_->width(), this->blob_data_->width());
        EXPECT_EQ(this->blob_top_->height(), 4);
        EXPECT_EQ(this->blob_top_->width(), 4);
    }

    // test forward: to test flip: 1/(h-1) & 1/(w-1) must be 2^{-n}
    TYPED_TEST(ROITransLayerTest, TestIdenticalForward) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        ConstantFiller<Dtype> constant_filler(filler_param);
        constant_filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = 0;
        this->blob_theta_->mutable_cpu_data()[1] = 1.;
        this->blob_theta_->mutable_cpu_data()[2] = 0.;
        this->blob_theta_->mutable_cpu_data()[3] = 2.;
        this->blob_theta_->mutable_cpu_data()[4] = 0.;
        this->blob_theta_->mutable_cpu_data()[5] = 1.;
        this->blob_theta_->mutable_cpu_data()[6] = 2.;
        this->blob_theta_->mutable_cpu_data()[0 + 7] = 1;
        this->blob_theta_->mutable_cpu_data()[1 + 7] = 1.;
        this->blob_theta_->mutable_cpu_data()[2 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 7] = 2.;
        this->blob_theta_->mutable_cpu_data()[4 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 7] = 1.;
        this->blob_theta_->mutable_cpu_data()[6 + 7] = 2.;
        this->blob_theta_->mutable_cpu_data()[0 + 14] = 1;
        this->blob_theta_->mutable_cpu_data()[1 + 14] = 1.;
        this->blob_theta_->mutable_cpu_data()[2 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 14] = 2.;
        this->blob_theta_->mutable_cpu_data()[4 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 14] = 1.;
        this->blob_theta_->mutable_cpu_data()[6 + 14] = 2.;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        shared_ptr<Layer<Dtype> > layer(
                new ROITransLayer<Dtype>(layer_param));
        layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        const Dtype* top_data;
        const Dtype* ref_top_data;
        top_data = this->blob_top_->cpu_data();

		Blob<Dtype>* ref_top = new Blob<Dtype>(3, 3, 4, 4); 
		for (int i = 0; i < ref_top->num(); i++) {
			for (int c = 0; c < ref_top->channels(); c++) {
				for (int h = 0; h < ref_top->height(); h++) {
					for (int w = 0; w < ref_top->width(); w++) {
						ref_top->mutable_cpu_data()[ref_top->offset(i, c, h, w)] = this->blob_data_->cpu_data()[this->blob_data_->offset(this->blob_theta_->cpu_data()[7*i], c, h, w)];
					}
				}
			}
		}
        ref_top_data = ref_top->cpu_data();

		//////////
		//std::cout << "--top--" << this->blob_top_->num() << " " << this->blob_top_->channels() << "----" << std::endl;
		//for (int i = 0; i < this->blob_top_->num(); i++) {
		//	for (int c = 0; c < this->blob_top_->channels(); c++) {
		//		for (int h = 0; h < this->blob_top_->height(); h++) {
		//			for (int w = 0; w < this->blob_top_->width(); w++) {
		//				std::cout << this->blob_top_->data_at(i, c, h, w) << " ";
		//			}
		//			std::cout << std::endl;
		//		}
		//		std::cout << std::endl;
		//	}
		//	std::cout << std::endl;
		//}
		//std::cout << std::endl;

		//std::cout << "--data--" << this->blob_data_->num() << " " << this->blob_data_->channels() << "----" << std::endl;
		//for (int i = 0; i < this->blob_data_->num(); i++) {
		//	for (int c = 0; c < this->blob_data_->channels(); c++) {
		//		for (int h = 0; h < this->blob_data_->height(); h++) {
		//			for (int w = 0; w < this->blob_data_->width(); w++) {
		//				std::cout << this->blob_data_->data_at(i, c, h, w) << " ";
		//			}
		//			std::cout << std::endl;
		//		}
		//		std::cout << std::endl;
		//	}
		//	std::cout << std::endl;
		//}
		//std::cout << std::endl;
		//////////

        for (int i = 0; i < this->blob_top_->count(); ++i) {
            EXPECT_NEAR(top_data[i], ref_top_data[i], 1e-4);
        }
    }

    TYPED_TEST(ROITransLayerTest, TestFlipXForward) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        ConstantFiller<Dtype> constant_filler(filler_param);
        constant_filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = 1;
        this->blob_theta_->mutable_cpu_data()[1] = -1.;
        this->blob_theta_->mutable_cpu_data()[2] = 0.;
        this->blob_theta_->mutable_cpu_data()[3] = 1.;
        this->blob_theta_->mutable_cpu_data()[4] = 0.;
        this->blob_theta_->mutable_cpu_data()[5] = 1.;
        this->blob_theta_->mutable_cpu_data()[6] = 2.;
        this->blob_theta_->mutable_cpu_data()[0 + 7] = 1;
        this->blob_theta_->mutable_cpu_data()[1 + 7] = -1.;
        this->blob_theta_->mutable_cpu_data()[2 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 7] = 1.;
        this->blob_theta_->mutable_cpu_data()[4 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 7] = 1.;
        this->blob_theta_->mutable_cpu_data()[6 + 7] = 2.;
        this->blob_theta_->mutable_cpu_data()[0 + 14] = 0;
        this->blob_theta_->mutable_cpu_data()[1 + 14] = -1.;
        this->blob_theta_->mutable_cpu_data()[2 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 14] = 1.;
        this->blob_theta_->mutable_cpu_data()[4 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 14] = 1.;
        this->blob_theta_->mutable_cpu_data()[6 + 14] = 2.;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        shared_ptr<Layer<Dtype> > layer(
                new ROITransLayer<Dtype>(layer_param));
        layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        const Dtype* top_data;
        const Dtype* ref_top_data;

        top_data = this->blob_top_->cpu_data();

		//////////
		std::cout << "--top--" << this->blob_top_->num() << " " << this->blob_top_->channels() << "----" << std::endl;
		for (int i = 0; i < this->blob_top_->num(); i++) {
			for (int c = 0; c < this->blob_top_->channels(); c++) {
				for (int h = 0; h < this->blob_top_->height(); h++) {
					for (int w = 0; w < this->blob_top_->width(); w++) {
						std::cout << this->blob_top_->data_at(i, c, h, w) << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;

		std::cout << "--data--" << this->blob_data_->num() << " " << this->blob_data_->channels() << "----" << std::endl;
		for (int i = 0; i < this->blob_data_->num(); i++) {
			for (int c = 0; c < this->blob_data_->channels(); c++) {
				for (int h = 0; h < this->blob_data_->height(); h++) {
					for (int w = 0; w < this->blob_data_->width(); w++) {
						std::cout << this->blob_data_->data_at(i, c, h, w) << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		//////////


		Blob<Dtype>* ref_top = new Blob<Dtype>(3, 3, 4, 4); 
		for (int i = 0; i < ref_top->num(); i++) {
			for (int c = 0; c < ref_top->channels(); c++) {
				for (int h = 0; h < ref_top->height(); h++) {
					for (int w = 0; w < ref_top->width(); w++) {
						ref_top->mutable_cpu_data()[ref_top->offset(i, c, h, w)] = this->blob_data_->cpu_data()[this->blob_data_->offset(this->blob_theta_->cpu_data()[7*i], c, h, w)];
					}
				}
			}
		}
        ref_top_data = ref_top->cpu_data();
        int num = this->blob_top_->num();
        int channels = this->blob_top_->channels();
        int height = this->blob_top_->height();
        int width = this->blob_top_->width();
        for (int n = 0; n < num; ++n) {
            for (int c = 0; c < channels; ++c) {
                for (int h = 0; h < height; ++h) {
                    for (int w = 0; w < width; ++w) {
                        EXPECT_NEAR(top_data[this->blob_top_->offset(n, c, h, w)], ref_top_data[ref_top->offset(n, c, height - 1 - h, w)], 1e-4);
                    }
                }
            }
        }
    }

    TYPED_TEST(ROITransLayerTest, TestFlipYForward) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        ConstantFiller<Dtype> constant_filler(filler_param);
        constant_filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = (Dtype) 0;
        this->blob_theta_->mutable_cpu_data()[1] = (Dtype) 1.;
        this->blob_theta_->mutable_cpu_data()[2] = (Dtype) 0.;
        this->blob_theta_->mutable_cpu_data()[3] = (Dtype) 2.;
        this->blob_theta_->mutable_cpu_data()[4] = (Dtype) 0.;
        this->blob_theta_->mutable_cpu_data()[5] = (Dtype) - 1.;
        this->blob_theta_->mutable_cpu_data()[6] = (Dtype) 1.;
        this->blob_theta_->mutable_cpu_data()[0 + 7] = (Dtype) 0;
        this->blob_theta_->mutable_cpu_data()[1 + 7] = (Dtype) 1.;
        this->blob_theta_->mutable_cpu_data()[2 + 7] = (Dtype) 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 7] = (Dtype) 2.;
        this->blob_theta_->mutable_cpu_data()[4 + 7] = (Dtype) 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 7] = (Dtype) - 1.;
        this->blob_theta_->mutable_cpu_data()[6 + 7] = (Dtype) 1.;
        this->blob_theta_->mutable_cpu_data()[0 + 14] = (Dtype) 1;
        this->blob_theta_->mutable_cpu_data()[1 + 14] = (Dtype) 1.;
        this->blob_theta_->mutable_cpu_data()[2 + 14] = (Dtype) 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 14] = (Dtype) 2.;
        this->blob_theta_->mutable_cpu_data()[4 + 14] = (Dtype) 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 14] = (Dtype) - 1.;
        this->blob_theta_->mutable_cpu_data()[6 + 14] = (Dtype) 1.;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        shared_ptr<Layer<Dtype> > layer(
                new ROITransLayer<Dtype>(layer_param));
        layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        const Dtype* top_data;
        const Dtype* ref_top_data;
        top_data = this->blob_top_->cpu_data();
		
		Blob<Dtype>* ref_top = new Blob<Dtype>(3, 3, 4, 4); 
		for (int i = 0; i < ref_top->num(); i++) {
			for (int c = 0; c < ref_top->channels(); c++) {
				for (int h = 0; h < ref_top->height(); h++) {
					for (int w = 0; w < ref_top->width(); w++) {
						ref_top->mutable_cpu_data()[ref_top->offset(i, c, h, w)] = this->blob_data_->cpu_data()[this->blob_data_->offset(this->blob_theta_->cpu_data()[7*i], c, h, w)];
					}
				}
			}
		}
        ref_top_data = ref_top->cpu_data();

        int num = this->blob_top_->num();
        int channels = this->blob_top_->channels();
        int height = this->blob_top_->height();
        int width = this->blob_top_->width();
        for (int n = 0; n < num; ++n) {
            for (int c = 0; c < channels; ++c) {
                for (int h = 0; h < height; ++h) {
                    for (int w = 0; w < width; ++w) {
                        EXPECT_NEAR(top_data[this->blob_top_->offset(n, c, h, w)], ref_top_data[ref_top->offset(n, c, h, width - 1 - w)], 1e-4);
                    }
                }
            }
        }
    }

    TYPED_TEST(ROITransLayerTest, TestFlipXYForward) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        ConstantFiller<Dtype> constant_filler(filler_param);
        constant_filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = 1;
        this->blob_theta_->mutable_cpu_data()[1] = -1.;
        this->blob_theta_->mutable_cpu_data()[2] = 0.;
        this->blob_theta_->mutable_cpu_data()[3] = 1.;
        this->blob_theta_->mutable_cpu_data()[4] = 0.;
        this->blob_theta_->mutable_cpu_data()[5] = -1.;
        this->blob_theta_->mutable_cpu_data()[6] = 1.;
        this->blob_theta_->mutable_cpu_data()[0 + 7] = 1;
        this->blob_theta_->mutable_cpu_data()[1 + 7] = -1.;
        this->blob_theta_->mutable_cpu_data()[2 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 7] = 1.;
        this->blob_theta_->mutable_cpu_data()[4 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 7] = -1.;
        this->blob_theta_->mutable_cpu_data()[6 + 7] = 1.;
        this->blob_theta_->mutable_cpu_data()[0 + 14] = 1;
        this->blob_theta_->mutable_cpu_data()[1 + 14] = -1.;
        this->blob_theta_->mutable_cpu_data()[2 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 14] = 1.;
        this->blob_theta_->mutable_cpu_data()[4 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 14] = -1.;
        this->blob_theta_->mutable_cpu_data()[6 + 14] = 1.;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        shared_ptr<Layer<Dtype> > layer(
                new ROITransLayer<Dtype>(layer_param));
        layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        const Dtype* top_data;
        const Dtype* ref_top_data;
        top_data = this->blob_top_->cpu_data();

		Blob<Dtype>* ref_top = new Blob<Dtype>(3, 3, 4, 4); 
		for (int i = 0; i < ref_top->num(); i++) {
			for (int c = 0; c < ref_top->channels(); c++) {
				for (int h = 0; h < ref_top->height(); h++) {
					for (int w = 0; w < ref_top->width(); w++) {
						ref_top->mutable_cpu_data()[ref_top->offset(i, c, h, w)] = this->blob_data_->cpu_data()[this->blob_data_->offset(this->blob_theta_->cpu_data()[7*i], c, h, w)];
					}
				}
			}
		}
        ref_top_data = ref_top->cpu_data();

        int num = this->blob_top_->num();
        int channels = this->blob_top_->channels();
        int height = this->blob_top_->height();
        int width = this->blob_top_->width();
        for (int n = 0; n < num; ++n) {
            for (int c = 0; c < channels; ++c) {
                for (int h = 0; h < height; ++h) {
                    for (int w = 0; w < width; ++w) {
                        EXPECT_NEAR(top_data[this->blob_top_->offset(n, c, h, w)], ref_top_data[ref_top->offset(n, c, height - 1 - h, width - 1 - w)], 1e-4);
                    }
                }
            }
        }
    }

    TYPED_TEST(ROITransLayerTest, TestScalingForward) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        ConstantFiller<Dtype> constant_filler(filler_param);
        constant_filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = 0;
        this->blob_theta_->mutable_cpu_data()[1] = 2;
        this->blob_theta_->mutable_cpu_data()[2] = 0.;
        this->blob_theta_->mutable_cpu_data()[3] = 4.;
        this->blob_theta_->mutable_cpu_data()[4] = 0.;
        this->blob_theta_->mutable_cpu_data()[5] = 2;
        this->blob_theta_->mutable_cpu_data()[6] = 4.;
        this->blob_theta_->mutable_cpu_data()[0 + 7] = 0;
        this->blob_theta_->mutable_cpu_data()[1 + 7] = 2;
        this->blob_theta_->mutable_cpu_data()[2 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 7] = 4.;
        this->blob_theta_->mutable_cpu_data()[4 + 7] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 7] = 2;
        this->blob_theta_->mutable_cpu_data()[6 + 7] = 4.;
        this->blob_theta_->mutable_cpu_data()[0 + 14] = 0;
        this->blob_theta_->mutable_cpu_data()[1 + 14] = 2;
        this->blob_theta_->mutable_cpu_data()[2 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[3 + 14] = 4.;
        this->blob_theta_->mutable_cpu_data()[4 + 14] = 0.;
        this->blob_theta_->mutable_cpu_data()[5 + 14] = 2;
        this->blob_theta_->mutable_cpu_data()[6 + 14] = 4.;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        shared_ptr<Layer<Dtype> > layer(
                new ROITransLayer<Dtype>(layer_param));
        layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        const Dtype* top_data;
        const Dtype* ref_top_data;
        top_data = this->blob_top_->cpu_data();

		Blob<Dtype>* ref_top = new Blob<Dtype>(3, 3, 8, 8); 
		for (int i = 0; i < ref_top->num(); i++) {
			for (int c = 0; c < ref_top->channels(); c++) {
				for (int h = 0; h < ref_top->height(); h++) {
					for (int w = 0; w < ref_top->width(); w++) {
						if (h < this->blob_data_->height() && w < this->blob_data_->width())
							ref_top->mutable_cpu_data()[ref_top->offset(i, c, h, w)] = this->blob_data_->cpu_data()[this->blob_data_->offset(this->blob_theta_->cpu_data()[7*i], c, h, w)];
						else
							ref_top->mutable_cpu_data()[ref_top->offset(i, c, h, w)] = 0.0;
					}
				}
			}
		}
        ref_top_data = ref_top->cpu_data();

        int num = this->blob_top_->num();
        int channels = this->blob_top_->channels();
        int height = this->blob_top_->height();
        int width = this->blob_top_->width();
        for (int n = 0; n < num; ++n) {
            for (int c = 0; c < channels; ++c) {
                for (int h = 0; h < height; ++h) {
                    for (int w = 0; w < width; ++w) {
                        EXPECT_NEAR(top_data[this->blob_top_->offset(n, c, h, w)], ref_top_data[ref_top->offset(n, c, h * 2, w * 2)], 1e-4);
                    }
                }
            }
        }
    }

    TYPED_TEST(ROITransLayerTest, TestAffineForward) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        filler_param.set_min(-1);
        filler_param.set_max(1);
        UniformFiller<Dtype> filler(filler_param);
        filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = caffe_rng_rand() % 2;
        this->blob_theta_->mutable_cpu_data()[7] = caffe_rng_rand() % 2;
        this->blob_theta_->mutable_cpu_data()[14] = caffe_rng_rand() % 2;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        shared_ptr<Layer<Dtype> > layer(
                new ROITransLayer<Dtype>(layer_param));
        layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        affine_transform(this->blob_data_, this->blob_theta_, this->MakeReferenceTop(this->blob_top_), 4, 4);
        const Dtype* top_data;
        const Dtype* ref_top_data;
        top_data = this->blob_top_->cpu_data();
        ref_top_data = this->ref_blob_top_->cpu_data();

		////////
		std::cout << "--top--" << this->blob_top_->num() << " " << this->blob_top_->channels() << "----" << std::endl;
		for (int i = 0; i < this->blob_top_->num(); i++) {
			for (int c = 0; c < this->blob_top_->channels(); c++) {
				for (int h = 0; h < this->blob_top_->height(); h++) {
					for (int w = 0; w < this->blob_top_->width(); w++) {
						std::cout << this->blob_top_->data_at(i, c, h, w) << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;

		std::cout << "--data--" << this->blob_data_->num() << " " << this->blob_data_->channels() << "----" << std::endl;
		for (int i = 0; i < this->blob_data_->num(); i++) {
			for (int c = 0; c < this->blob_data_->channels(); c++) {
				for (int h = 0; h < this->blob_data_->height(); h++) {
					for (int w = 0; w < this->blob_data_->width(); w++) {
						std::cout << this->blob_data_->data_at(i, c, h, w) << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;

		std::cout << "--ref_top--" << this->ref_blob_top_->num() << " " << this->ref_blob_top_->channels() << "----" << std::endl;
		for (int i = 0; i < this->ref_blob_top_->num(); i++) {
			for (int c = 0; c < this->ref_blob_top_->channels(); c++) {
				for (int h = 0; h < this->ref_blob_top_->height(); h++) {
					for (int w = 0; w < this->ref_blob_top_->width(); w++) {
						std::cout << this->ref_blob_top_->data_at(i, c, h, w) << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;

		std::cout << "--theta--" << this->blob_theta_->num() << " " << this->blob_theta_->channels() << "----" << std::endl;
		for (int i = 0; i < this->blob_theta_->num(); i++) {
			for (int c = 0; c < this->blob_theta_->channels(); c++) {
				for (int h = 0; h < this->blob_theta_->height(); h++) {
					for (int w = 0; w < this->blob_theta_->width(); w++) {
						std::cout << this->blob_theta_->data_at(i, c, h, w) << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		////////

        for (int i = 0; i < this->blob_top_->count(); ++i) {
            EXPECT_NEAR(top_data[i], ref_top_data[i], 1e-4);
        }
    }

    // test gradients of data part using standard caffe utility 
    TYPED_TEST(ROITransLayerTest, TestDataGradient) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        filler_param.set_min(-1);
        filler_param.set_max(1);
        UniformFiller<Dtype> filler(filler_param);
        filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = caffe_rng_rand() % 2;
        this->blob_theta_->mutable_cpu_data()[7] = caffe_rng_rand() % 2;
        this->blob_theta_->mutable_cpu_data()[14] = caffe_rng_rand() % 2;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        ROITransLayer<Dtype> layer(layer_param);
        GradientChecker<Dtype> checker(1e-2, 1e-3);
        checker.CheckGradientExhaustive(&layer, this->blob_bottom_vec_,
                this->blob_top_vec_, 0);
    }
	
    // finite difference with mask trick for max operation: track the winner (refer to http://cs231n.github.io/neural-networks-3/)
    template <typename Dtype>
    void theta_gradient(const Blob<Dtype>* in, const Blob<Dtype>* theta, double delta, Blob<Dtype>* gradient, int out_h, int out_w) {
        //int num = in->shape(0);
		int num = theta->shape(0);
        int channels = in->shape(1);
        //int height = in->shape(2);
        //int width = in->shape(3);
		int height = out_h;
		int width = out_w;
        Dtype* gradient_data = gradient->mutable_cpu_diff();
        caffe_set<Dtype>(theta->count(), 0, gradient_data);
        const Dtype* theta_data = theta->cpu_data();
        for (int i = 0; i < 6; ++i) {
            for (int n = 0; n < num; ++n) {
                for (int h = 0; h < height; ++h) {
                    // double ty = h / (double) (height - 1) * (double) 2. - (double) 1.;
					// double ty = h - (double) (out_h) / 2;
                    for (int w = 0; w < width; ++w) {
                        // double tx = w / (double) (width - 1)*(double) 2. - (double) 1.;
						// double tx = w - (double) (out_w) / 2;
						double tx = h - (double) (out_h) / 2;
						double ty = w - (double) (out_w) / 2;
                        double sx = tx * theta_data[n * 7 + 1] + ty * theta_data[n * 7 + 2] + theta_data[n * 7 + 3];
                        double sy = tx * theta_data[n * 7 + 4] + ty * theta_data[n * 7 + 5] + theta_data[n * 7 + 6];
                        double sxn = sx;
                        double syn = sy;
                        if (i == 0) {
                            sxn += delta * tx;
                        } else if (i == 1) {
                            sxn += delta * ty;
                        } else if (i == 2) {
                            sxn += delta;
                        } else if (i == 3) {
                            syn += delta * tx;
                        } else if (i == 4) {
                            syn += delta * ty;
                        } else if (i == 5) {
                            syn += delta;
                        }
                        //sx = (sx + 1.) / (double) 2. * (width - 1);
                        //sy = (sy + 1.) / (double) 2. * (height - 1);
                        //sxn = (sxn + 1.) / (double) 2. * (width - 1);
                        //syn = (syn + 1.) / (double) 2. * (height - 1);
                        for (int c = 0; c < channels; ++c) {
                            for (int hh = 0; hh < in->shape(2); ++hh) {
                                for (int ww = 0; ww < in->shape(3); ++ww) {
                                    double max_y = 0;
                                    double max_yn = 0;
                                    //if (hh > sy) {
                                    //    max_y = hh - sy;
                                    //    max_yn = hh - syn;
                                    //} else {
                                    //    max_y = sy - hh;
                                    //    max_yn = syn - hh;
                                    //}
                                    //if (1 - max_y < 0) {
                                    //    max_y = 0;
                                    //    max_yn = 0;
                                    //} else {
                                    //    max_y = 1 - max_y;
                                    //    max_yn = 1 - max_yn;
                                    //}
									if (ww > sy) {
                                        max_y = ww - sy;
                                        max_yn = ww - syn;
                                    } else {
                                        max_y = sy - ww;
                                        max_yn = syn - ww;
                                    }
                                    if (1 - max_y < 0) {
                                        max_y = 0;
                                        max_yn = 0;
                                    } else {
                                        max_y = 1 - max_y;
                                        max_yn = 1 - max_yn;
                                    }
                                    double max_x = 0;
                                    double max_xn = 0;
                                    //if (ww > sx) {
                                    //    max_x = ww - sx;
                                    //    max_xn = ww - sxn;
                                    //} else {
                                    //    max_x = sx - ww;
                                    //    max_xn = sxn - ww;
                                    //}
                                    //if (1 - max_x < 0) {
                                    //    max_x = 0;
                                    //    max_xn = 0;
                                    //} else {
                                    //    max_x = 1 - max_x;
                                    //    max_xn = 1 - max_xn;
                                    //}
									if (hh > sx) {
                                        max_x = hh - sx;
                                        max_xn = hh - sxn;
                                    } else {
                                        max_x = sx - hh;
                                        max_xn = sxn - hh;
                                    }
                                    if (1 - max_x < 0) {
                                        max_x = 0;
                                        max_xn = 0;
                                    } else {
                                        max_x = 1 - max_x;
                                        max_xn = 1 - max_xn;
                                    }
                                    gradient_data[i + 1 + n * 7] += (Dtype) (in->data_at(theta_data[n*7], c, hh, ww) * (max_xn * max_yn - max_x * max_y));
                                }
                            }
                        }
                    }
                }
            }
        }
        // to improve numeric precision
        for(int i = 0; i < theta->count(); ++i) {
            gradient_data[i] /= delta;
        }
    }

    template void theta_gradient(const Blob<float>* data, const Blob<float>* theta, double delta, Blob<float>* gradient, int out_h, int out_w);

    template void theta_gradient(const Blob<double>* data, const Blob<double>* theta, double delta, Blob<double>* gradient, int out_h, int out_w);


	//  current here !!!
    // test gradients of theta using finite difference method: max operator would fail caffe utility
    TYPED_TEST(ROITransLayerTest, TestThetaGradient) {
        typedef typename TypeParam::Dtype Dtype;
        FillerParameter filler_param;
        filler_param.set_min(-1);
        filler_param.set_max(1);
        UniformFiller<Dtype> filler(filler_param);
        filler.Fill(this->blob_theta_);
        this->blob_theta_->mutable_cpu_data()[0] = caffe_rng_rand() % 2;
        this->blob_theta_->mutable_cpu_data()[7] = caffe_rng_rand() % 2;
        this->blob_theta_->mutable_cpu_data()[14] = caffe_rng_rand() % 2;
        LayerParameter layer_param;
		ROITransParameter* roit_param = layer_param.mutable_roit_param();
		roit_param->set_output_h(4);
		roit_param->set_output_w(4);
        ROITransLayer<Dtype> layer(layer_param);
        // call backward to generate theta_gradient
        layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
        layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        for (int i = 0; i < this->blob_top_->count(); ++i) {
            this->blob_top_->mutable_cpu_diff()[i] = (Dtype) 1.;
        }
        vector<bool> propagate_down(this->blob_bottom_vec_.size(), true);
        layer.Backward(this->blob_top_vec_, propagate_down,
                this->blob_bottom_vec_);
        // compute theta gradient using finite difference
        shared_ptr<Blob<Dtype> > ref_blob_theta_diff;
        ref_blob_theta_diff.reset(new Blob<Dtype>());
        ref_blob_theta_diff->ReshapeLike(*(this->blob_theta_));
        theta_gradient(this->blob_data_, this->blob_theta_, (double) 1e-4, ref_blob_theta_diff.get(), 4, 4);
        const Dtype* theta_diff = this->blob_theta_->cpu_diff();
        const Dtype* ref_theta_diff = ref_blob_theta_diff->cpu_diff();
		//////
		std::cout << "--theta--" << this->blob_theta_->num() << " " << this->blob_theta_->channels() << "----" << std::endl;
		for (int i = 0; i < this->blob_theta_->num(); i++) {
			for (int c = 0; c < this->blob_theta_->channels(); c++) {
				for (int h = 0; h < this->blob_theta_->height(); h++) {
					for (int w = 0; w < this->blob_theta_->width(); w++) {
						std::cout << this->blob_theta_->data_at(i, c, h, w) << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;

		std::cout << std::endl << "theta diff" << std::endl;
		for (int i = 0; i < this->blob_theta_->num(); i++) {
			for (int j = 0; j < this->blob_theta_->channels(); j++) {
				std::cout << theta_diff[this->blob_theta_->offset(i, j, 0, 0)] << " ";
			}
			std::cout << std::endl;
		}
		//////
        for (int i = 0; i < this->blob_theta_->count(); ++i) {
            EXPECT_NEAR(theta_diff[i], ref_theta_diff[i], 1e-4) << "i=" << i;
        }
    }
}


