#include <cmath>
#include <cstring>
#include <vector>
#include <iostream>
#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/rot_scl_trans_layer.hpp"

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {

	typedef ::testing::Types<GPUDevice<float>, GPUDevice<double> > TestDtypesGPU;
	// typedef ::testing::Types<CPUDevice<float>, CPUDevice<double> > TestDtypesCPU;

    template <typename TypeParam>
    class ROTSCLTransLayerTest : public MultiDeviceTest<TypeParam> {
        typedef typename TypeParam::Dtype Dtype;
    protected:

        ROTSCLTransLayerTest()
        : blob_bottom_(new Blob<Dtype>(10, 5, 1, 1)),
        blob_top_(new Blob<Dtype>()) {
			FillerParameter filler_param;
			filler_param.set_std(10);
			GaussianFiller<Dtype> filler(filler_param);
			filler.Fill(this->blob_bottom_);
			blob_bottom_vec_.push_back(blob_bottom_);
			blob_top_vec_.push_back(blob_top_);
        }

        virtual ~ROTSCLTransLayerTest() {
            delete blob_bottom_;
            delete blob_top_;
        }

        Blob<Dtype>* blob_bottom_;
        Blob<Dtype>* blob_top_;
        vector<Blob<Dtype>*> blob_bottom_vec_;
        vector<Blob<Dtype>*> blob_top_vec_;
    };
    
    //TYPED_TEST_CASE(ROTSCLTransLayerTest, TestDtypesAndDevices);
	TYPED_TEST_CASE(ROTSCLTransLayerTest, TestDtypesGPU);
    // check top blob shape
    TYPED_TEST(ROTSCLTransLayerTest, TestGradient) {
		typedef typename TypeParam::Dtype Dtype;
		LayerParameter layer_param;
		ROTSCLTransLayer<Dtype> layer(layer_param);
		GradientChecker<Dtype> checker(1e-2, 1e-3, 1701);
		checker.CheckGradientExhaustive(&layer, this->blob_bottom_vec_, this->blob_top_vec_, 0);
	}
} // namespace caffe 


