#include <cmath>
#include <cstring>
#include <vector>
#include <iostream>
#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/select_layer.hpp"

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {
	typedef ::testing::Types<CPUDevice<float>, CPUDevice<double> > TestDtypesCPU;
	typedef ::testing::Types<GPUDevice<float>, GPUDevice<double> > TestDtypesGPU;

	template <typename TypeParam>
	class SelectLayerTest : public MultiDeviceTest<TypeParam> {
		typedef typename TypeParam::Dtype Dtype;
	protected:
		SelectLayerTest() 
			: blob_bottom_(new Blob<Dtype>(10, 5, 1, 1)),
			blob_top_(new Blob<Dtype>()) {
			}

		virtual void SetUp() {
			FillerParameter filler_param;
			filler_param.set_min(-1);
			filler_param.set_max(1);
			UniformFiller<Dtype> filler(filler_param);
			filler.Fill(this->blob_bottom_);
			blob_bottom_vec_.push_back(blob_bottom_);
			blob_top_vec_.push_back(blob_top_);
		}

		virtual ~SelectLayerTest() {
			delete blob_bottom_;
			delete blob_top_;
		}

		Blob<Dtype> * const blob_bottom_;
		Blob<Dtype> * const blob_top_;
		vector<Blob<Dtype>*> blob_bottom_vec_;
		vector<Blob<Dtype>*> blob_top_vec_;
	};

	TYPED_TEST_CASE(SelectLayerTest, TestDtypesGPU);
	TYPED_TEST(SelectLayerTest, TestGradient) {
		typedef typename TypeParam::Dtype Dtype;
		LayerParameter layer_param;
		SelectParameter* select_param = layer_param.mutable_select_param();
		select_param->add_select_index(1);
		select_param->add_select_index(2);
		select_param->add_select_index(0);
		SelectLayer<Dtype> layer(layer_param);
		GradientChecker<Dtype> checker(1e-3, 1e-4);
		checker.CheckGradientExhaustive(&layer, this->blob_bottom_vec_, this->blob_top_vec_, 0);
	}

} // namespace caffe

