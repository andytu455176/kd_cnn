#include <cmath>
#include <cstring>
#include <vector>
#include <iostream>
#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/roi_proposal_layer.hpp"

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"


namespace caffe {

	typedef ::testing::Types<CPUDevice<float>, CPUDevice<double> > TestDtypesCPU;
	typedef ::testing::Types<GPUDevice<float>, GPUDevice<double> > TestDtypesGPU;

	template <typename TypeParam>
	class ROIProposalLayerTest : public MultiDeviceTest<TypeParam> {
		typedef typename TypeParam::Dtype Dtype;
	protected:
		ROIProposalLayerTest()
		: blob_keypoint_proposal_(new Blob<Dtype>(5, 3, 1, 1)), 
		blob_orient_map_(new Blob<Dtype>(3, 1, 10, 10)), 
		blob_scale_map_(new Blob<Dtype>(3, 1, 10, 10)), 
		blob_top_(new Blob<Dtype>()) {
		}

		virtual void SetUp() {
			FillerParameter filler_param;
			filler_param.set_min(-1);
			filler_param.set_max(1);
			UniformFiller<Dtype> filler(filler_param);
			filler.Fill(this->blob_orient_map_);
			filler.Fill(this->blob_scale_map_);
			blob_bottom_vec_.push_back(blob_keypoint_proposal_);
			blob_bottom_vec_.push_back(blob_orient_map_);
			blob_bottom_vec_.push_back(blob_scale_map_);
			blob_top_vec_.push_back(blob_top_);
		}

		virtual ~ROIProposalLayerTest() {
			delete blob_keypoint_proposal_;
			delete blob_orient_map_;
			delete blob_top_;
		}

		Blob<Dtype> * const blob_keypoint_proposal_;
		Blob<Dtype> * const blob_orient_map_;
		Blob<Dtype> * const blob_scale_map_;
		Blob<Dtype> * const blob_top_;
		vector<Blob<Dtype>*> blob_bottom_vec_;
		vector<Blob<Dtype>*> blob_top_vec_;
	};
	TYPED_TEST_CASE(ROIProposalLayerTest, TestDtypesGPU);
    TYPED_TEST(ROIProposalLayerTest, TestSoftForward) {
        typedef typename TypeParam::Dtype Dtype;
		LayerParameter layer_param;
		ROIProposalParameter* roip_param = layer_param.mutable_roip_param();
		float coor_scale = 0.5;
		roip_param->set_orient_type("soft_argmax");
		roip_param->set_coor_scale(coor_scale);

		for (int i = 0; i < this->blob_keypoint_proposal_->count(); i++) {
			if (i % 3 == 0)
				this->blob_keypoint_proposal_->mutable_cpu_data()[i] = caffe_rng_rand() % 3;
			else 
				this->blob_keypoint_proposal_->mutable_cpu_data()[i] = caffe_rng_rand() % 10;
		}

		shared_ptr<Layer<Dtype> > layer(new ROIProposalLayer<Dtype>(layer_param));
		layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
		layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);


		/////
		std::cout << "top_" << std::endl;
		const Dtype* top_data = this->blob_top_->cpu_data();
		for (int i = 0; i < this->blob_top_->num(); i++) {
			for (int j = 0; j < this->blob_top_->channels(); j++) {
				std::cout << top_data[this->blob_top_->offset(i, j, 0, 0)] << " ";
			}
			std::cout << std::endl;
		}
		
		std::cout << "orient_map_" << std::endl;
		const Dtype* bottom1_data = this->blob_orient_map_->cpu_data();
		for (int i = 0; i < this->blob_orient_map_->num(); i++) {
			for (int j = 0; j < this->blob_orient_map_->channels(); j++) {
				for (int h = 0; h < this->blob_orient_map_->height(); h++) {
					for (int w = 0; w < this->blob_orient_map_->width(); w++) {
						std::cout << bottom1_data[this->blob_orient_map_->offset(i, j, h, w)] << " ";
					}
					std::cout << std::endl;
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}

		std::cout << "keypoint_proposal_" << std::endl;
		const Dtype* bottom0_data = this->blob_keypoint_proposal_->cpu_data();
		for (int i = 0; i < this->blob_keypoint_proposal_->num(); i++) {
			for (int j = 0; j < this->blob_keypoint_proposal_->channels(); j++) {
				std::cout << bottom0_data[this->blob_keypoint_proposal_->offset(i, j, 0, 0)] << " ";
			}
			std::cout << std::endl;
		}
		/////



		for (int i = 0; i < this->blob_top_->num(); i++) {
			int idx = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 0, 0, 0)];
			Dtype x = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 1, 0, 0)] * coor_scale;
			Dtype y = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 2, 0, 0)] * coor_scale;
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 0, 0, 0)], idx, 1e-4) << "roi: " << i << " (idx)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 1, 0, 0)], x, 1e-4) << "roi: " << i << " (x)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 2, 0, 0)], y, 1e-4) << "roi: " << i << " (y)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 3, 0, 0)], 
				this->blob_orient_map_->cpu_data()[this->blob_orient_map_->offset(idx, 0, int(x), int(y))], 1e-4) << "roi: " << i << " (rot)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 4, 0, 0)], 
				this->blob_scale_map_->cpu_data()[this->blob_orient_map_->offset(idx, 0, int(x), int(y))], 1e-4) << "roi: " << i << " (scl)";
		}
	}
	TYPED_TEST(ROIProposalLayerTest, TestHardForward) {
        typedef typename TypeParam::Dtype Dtype;
		LayerParameter layer_param;
		ROIProposalParameter* roip_param = layer_param.mutable_roip_param();
		float coor_scale = 0.5;
		roip_param->set_orient_type("hard_argmax");
		roip_param->set_coor_scale(coor_scale);

		for (int i = 0; i < this->blob_keypoint_proposal_->count(); i++) {
			if (i % 3 == 0)
				this->blob_keypoint_proposal_->mutable_cpu_data()[i] = caffe_rng_rand() % 3;
			else 
				this->blob_keypoint_proposal_->mutable_cpu_data()[i] = caffe_rng_rand() % 10;
		}

		shared_ptr<Layer<Dtype> > layer(new ROIProposalLayer<Dtype>(layer_param));
		layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);

		for (int i = 0; i < this->blob_orient_map_->count(); i++) {
			this->blob_orient_map_->mutable_cpu_data()[i] = caffe_rng_rand() % 36;
		}

		vector<Dtype> orient_bin(36);
		for (int i = 0; i < 36; i++) {
			orient_bin[i] = Dtype(-175 + i * 10) * Dtype(M_PI / 180.0);
		}

		layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
		for (int i = 0; i < this->blob_top_->num(); i++) {
			int idx = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 0, 0, 0)];
			Dtype x = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 1, 0, 0)] * coor_scale;
			Dtype y = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 2, 0, 0)] * coor_scale;
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 0, 0, 0)], idx, 1e-4) << "roi: " << i << " (idx)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 1, 0, 0)], x, 1e-4) << "roi: " << i << " (x)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 2, 0, 0)], y, 1e-4) << "roi: " << i << " (y)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 3, 0, 0)], 
				orient_bin[this->blob_orient_map_->cpu_data()[this->blob_orient_map_->offset(idx, 0, int(x), int(y))]], 1e-4) << "roi: " << i << " (rot)";
			EXPECT_NEAR(this->blob_top_->cpu_data()[this->blob_top_->offset(i, 4, 0, 0)], 
					this->blob_scale_map_->cpu_data()[this->blob_orient_map_->offset(idx, 0, int(x), int(y))], 1e-4) << "roi: " << i << " (scl)";
		}
	}

	TYPED_TEST(ROIProposalLayerTest, TestBackward) {
        typedef typename TypeParam::Dtype Dtype;
		LayerParameter layer_param;
		ROIProposalParameter* roip_param = layer_param.mutable_roip_param();
		float coor_scale = 0.5;
		roip_param->set_orient_type("soft_argmax");
		roip_param->set_coor_scale(coor_scale);

		for (int i = 0; i < this->blob_keypoint_proposal_->count(); i++) {
			if (i % 3 == 0)
				this->blob_keypoint_proposal_->mutable_cpu_data()[i] = caffe_rng_rand() % 3;
			else 
				this->blob_keypoint_proposal_->mutable_cpu_data()[i] = caffe_rng_rand() % 10;
		}

		shared_ptr<Layer<Dtype> > layer(new ROIProposalLayer<Dtype>(layer_param));
		layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
		layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
        vector<bool> propagate_down(this->blob_bottom_vec_.size(), true);
		int offset = 5;
		for (int i = 0; i < this->blob_top_->num(); i++) {
			this->blob_top_->mutable_cpu_diff()[offset * i] = 1;
			this->blob_top_->mutable_cpu_diff()[offset * i + 1] = 2;
			this->blob_top_->mutable_cpu_diff()[offset * i + 2] = 3;
			this->blob_top_->mutable_cpu_diff()[offset * i + 3] = 4;
			this->blob_top_->mutable_cpu_diff()[offset * i + 4] = 5;
		}
		layer->Backward(this->blob_top_vec_, propagate_down, this->blob_bottom_vec_);
		Blob<Dtype>* ref_blob_orient = new Blob<Dtype>(3, 1, 10, 10);
		Blob<Dtype>* ref_blob_scale = new Blob<Dtype>(3, 1, 10, 10);
		Dtype* ref_orient = ref_blob_orient->mutable_cpu_data();
		Dtype* ref_scale = ref_blob_scale->mutable_cpu_data();
		caffe_set<Dtype>(ref_blob_orient->count(), Dtype(0), ref_orient);
		for (int i = 0; i < this->blob_top_->num(); i++) {
			int idx = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 0, 0, 0)];
			Dtype x = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 1, 0, 0)] * coor_scale;
			Dtype y = this->blob_keypoint_proposal_->cpu_data()[this->blob_keypoint_proposal_->offset(i, 2, 0, 0)] * coor_scale;
			ref_orient[ref_blob_orient->offset(idx, 0, int(x), int(y))] = 4;
			ref_scale[ref_blob_scale->offset(idx, 0, int(x), int(y))] = 5;
		}
		for (int i = 0; i < this->blob_orient_map_->count(); i++) {
			EXPECT_NEAR(this->blob_orient_map_->cpu_diff()[i], ref_orient[i], 1e-4) << "i=" << i << " (rot)";
			EXPECT_NEAR(this->blob_scale_map_->cpu_diff()[i], ref_scale[i], 1e-4) << "i=" << i << " (scl)";
		}
	}
} // namespace caffe 




