#include <algorithm>
#include <cmath>
#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/hard_loss_layer.hpp"

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {

template <typename TypeParam>
class HardLossLayerTest : public MultiDeviceTest<TypeParam> {
	typedef typename TypeParam::Dtype Dtype;

	protected:
	HardLossLayerTest()
		: blob_bottom_data_(new Blob<Dtype>(12, 3, 1, 1)), 
		  blob_bottom_label_(new Blob<Dtype>(12, 1, 1, 1)),
		  blob_top_loss_(new Blob<Dtype>()) {

		FillerParameter filler_param;
		filler_param.set_min(-1.0);
		filler_param.set_max(1.0);

		UniformFiller<Dtype> filler(filler_param);
		filler.Fill(this->blob_bottom_data_);
		blob_bottom_vec_.push_back(blob_bottom_data_);
		
		//for (int i = 0; i < blob_bottom_y->count(); i++) {
		//	blob_bottom_label_->mutable_cpu_data()[i] = caffe_rg_rand() % 3;
		//}
		blob_bottom_label_->mutable_cpu_data()[0] = 0;
		blob_bottom_label_->mutable_cpu_data()[1] = 0;
		blob_bottom_label_->mutable_cpu_data()[2] = 1;
		blob_bottom_label_->mutable_cpu_data()[3] = 1;

		blob_bottom_vec_.push_back(blob_bottom_label_);
		blob_top_vec_.push_back(blob_top_loss_);
	}

	virtual ~HardLossLayerTest() {
		delete blob_bottom_data_;
		delete blob_bottom_label_;
		delete blob_top_loss_;
	}

	Blob<Dtype>* const blob_bottom_data_;
	Blob<Dtype>* const blob_bottom_label_;
	Blob<Dtype>* const blob_top_loss_;
	vector<Blob<Dtype>*> blob_bottom_vec_;
	vector<Blob<Dtype>*> blob_top_vec_;
};

TYPED_TEST_CASE(HardLossLayerTest, TestDtypesAndDevices);

TYPED_TEST(HardLossLayerTest, TestForward) {
	typedef typename TypeParam::Dtype Dtype;
	LayerParameter layer_param;
	HardParameter* hard_param = layer_param.mutable_hard_param();
	Dtype neg_num = 0; Dtype pair_size = 2; Dtype hard_ratio = 0.5; Dtype rand_ratio = 0.5; Dtype margin = 5.0;
	hard_param->set_neg_num(neg_num);
	hard_param->set_pair_size(pair_size);
	hard_param->set_hard_ratio(hard_ratio);
	hard_param->set_rand_ratio(rand_ratio);
	hard_param->set_margin(margin);
	HardLossLayer<Dtype> layer(layer_param);
	// layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
	// layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);

	const int dim = this->blob_bottom_data_->count() / this->blob_bottom_data_->num();
	const int num = this->blob_bottom_data_->num();

	//for (int i = 0; i < num; i+=pair_size) {
	//	// same pair
	//	Dtype dist = 0;
	//	for (int k = 0; k < dim; k++) {
	//		dist += (this->blob_bottom_data_->cpu_data()[i*dim+k]-this->blob_bottom_data_->cpu_data()[(i+1)*dim+k])*(this->blob_bottom_data_->cpu_data()[i*dim+k]-this->blob_bottom_data_->cpu_data()[(i+1)*dim+k]);
	//	}
	//	std::cout << "same label loss: " << dist << std::endl;
	//}
	GradientChecker<Dtype> checker(1e-2, 1e-2, 1701);
  	// check the gradient for the first bottom layer
  	checker.CheckGradientExhaustive(&layer, this->blob_bottom_vec_, this->blob_top_vec_, 0);
}

} // namespace caffe 






