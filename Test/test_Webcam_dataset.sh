#!/bin/bash


#for target in Chamonix Courbevoie Frankfurt Mexico Panorama StLouis
#do 
#	python ./test.py --action evaluation \
#					 --dataset Webcam \
#				 	 --metric match_score \
#				 	 --case $target \
#				 	 --output_result Webcam_${target}_result.mat
#done


python ./test.py --action evaluation \
				 --dataset Webcam \
				 --metric match_score \
				 --output_result Webcam_new_all_result.jpg
