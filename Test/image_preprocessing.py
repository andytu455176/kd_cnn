import sys, os, time
from multiprocessing import Process, Lock, Value
import numpy as np 
import h5py, cv2, cPickle, argparse
import matplotlib.pyplot as plt
import scipy.io
from lib.config import cfg

def parse_args():
    parser = argparse.ArgumentParser(description='Image preprocessing arguments')
    parser.add_argument('--action', dest='action', help='action to be done', default=None, type=str)
    parser.add_argument('--dir', dest='dir_path', help='directory used in the action', default=None, type=str)
    parser.add_argument('--output_dir', dest='output_dir', help='output directory', default=None, type=str)
    parser.add_argument('--filename', dest='filename', help='file name used in the action', default=None, type=str)
    parser.add_argument('--image_list', dest='image_list', help='image name list used in the action', default=None, type=str)
    parser.add_argument('--trans_num', dest='trans_num', help='num of transformation applied on an image', default=1, type=int)
    parser.add_argument('--proc_num', dest='proc_num', help='num of process used in transfomation', default=1, type=int)
    parser.add_argument('--prototxt', dest='prototxt', help='network prototxt file', default=None, type=str)
    parser.add_argument('--caffemodel', dest='caffemodel', help='network weights', default=None, type=str)
    parser.add_argument('--kp_file', dest='kp_file', help='keypoint file of the original image', default=None, type=str)
    parser.add_argument('--kp_rs_file', dest='kp_rs_file', help='keypoint, rot, scl file of the original image', default=None, type=str)
    parser.add_argument('--num_kp', dest='num_kp', help='num of keypoint require for OpenCV SIFT', default=20, type=int)
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def calculate_mean(dir_path, output_filename):
    image_list = os.listdir(dir_path)
    total_mean = np.zeros((1, 1, 3), dtype=np.float32)
    for i in range(len(image_list)):
        sys.stdout.flush()
        sys.stdout.write("cur progress: %d\r" % i)
        image_path = os.path.join(dir_path, image_list[i])
        image = cv2.imread(image_path)
        total_mean += np.mean(image, axis=(0, 1))

    total_mean = total_mean / float(len(image_list))
    temp = {}
    temp['mean'] = total_mean
    scipy.io.savemat(output_filename, temp)
    return 

def calculate_color_pca(dir_path, output_filename):
    image_list = os.listdir(dir_path)
    pixel_data = np.zeros((0, 3), dtype=np.float32)
    eigenval, eigenvec = np.zeros((3, 3), dtype=np.float32), np.zeros((3, 3), dtype=np.float32)
    for i in range(len(image_list)):
        sys.stdout.flush()
        sys.stdout.write("cur progress: %d\r" % i)
        image_path = os.path.join(dir_path, image_list[i])
        image = cv2.imread(image_path)
        pixel_data = np.vstack([pixel_data, np.mean(image, axis=(0, 1))])
    
    pixel_data -= pixel_data.mean(axis=0)
    pixel_cov = np.cov(pixel_data, rowvar=False)
    eigenval, eigenvec = np.linalg.eigh(pixel_cov)
    order = np.argsort(eigenval)[::-1]
    eigenval = eigenval[order]
    eigenvec = eigenvec[:, order]
    temp = {}
    temp['eigenval'] = eigenval
    temp['eigenvec'] = eigenvec
    temp['inv_eigenvec'] = np.linalg.inv(eigenvec)
    scipy.io.savemat(output_filename, temp)
    return 

def process_transform(num, code, code_format, output_dir, image_name, image, keypoint, eigenvec, inv_eigenvec, delta, mode, add_random_trans=True):
    if mode == 0: # kp_file
        kp_buffer = np.zeros((num, keypoint.shape[0], 2), dtype=np.float32)
    elif mode == 1: # kp_rs_file
        kp_buffer = np.zeros((num, keypoint.shape[0], 4), dtype=np.float32)

    for index in range(num):
        img, kp = image[...], keypoint[:, :2]
        img = process_bgr(img, eigenvec, inv_eigenvec, code['contrast1'][index,...]) #TODO
        img = process_hsv(img, [code['color'][index,...], code['contrast2'][index,...]]) #TODO
        img = process_blur(img, code['blur'][index,...])
        #img = process_gamma(img, code['gamma'][index,...])
        if index < float(num) * 0.8:
        #if index < float(num) * 1.0:
            ##### spatial handle scale to avoid too big image
            angle = np.deg2rad(code['rot'][index, 0])
            rotated_max_shape = np.max([np.abs(img.shape[0]*np.sin(angle))+np.abs(img.shape[1]*np.cos(angle)), np.abs(img.shape[0]*np.cos(angle))+np.abs(img.shape[1]*np.sin(angle))])
            if float(rotated_max_shape) * code['scale'][index, 0] > 600:
                new_scale = 599.0 / float(rotated_max_shape)
            else:
                new_scale = code['scale'][index, 0]
            #img, kp = process_rot_and_scale(img, kp, code['rot'][index,...], code['scale'][index,...])
            img, kp, _ = process_rot_and_scale(img, kp, code['rot'][index,...], np.array([new_scale], dtype=np.float32))
            ## modify code format for rc_file to store 
            code_format[index, 1] = new_scale
            code['scale'][index, 0] = new_scale
            #####
        else:
            img, kp, _ = process_homography_transform(img, kp, delta)
        if add_random_trans:
            kp = process_translation(kp, 0.10, 64)
        kp_buffer[index, :, :2] = kp
        if mode == 1:
            kp_buffer[index, :, 2:4] = keypoint[:, 2:4]
        output_path = os.path.join(output_dir, image_name[:-4]+"_"+str(index)+".jpg")
        cv2.imwrite(output_path, img)
    return kp_buffer

def process_translation(kp, perturbate_ratio, patch_size):
    kp_shape = kp.shape
    perturbate_matrix = (np.random.rand(*kp_shape)-0.5)*2 * perturbate_ratio * float(patch_size)
    return np.round(kp + perturbate_matrix).astype(np.int32)

def process_hsv(img, hsv_param):
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV).astype(np.float32)
    hsv_img[...,0] /= 179.0
    hsv_img[...,1:3] /= 255.0
    hsv_img[...,0] += hsv_param[0]
    hsv_img[...,1] = (hsv_img[...,1]**hsv_param[1][0]) * hsv_param[1][1] + hsv_param[1][2]
    hsv_img[...,2] = (hsv_img[...,2]**hsv_param[1][3]) * hsv_param[1][4] + hsv_param[1][5]
    hsv_img[hsv_img <= 0.0] = 0.0
    hsv_img[hsv_img >= 1.0] = 1.0
    hsv_img[...,0] *= 179.0
    hsv_img[...,1:3] *= 255.0
    hsv_img = hsv_img.astype(np.uint8)
    result_img = cv2.cvtColor(hsv_img, cv2.COLOR_HSV2BGR)
    return result_img

def process_bgr(img, eigenvec, inv_eigenvec, bgr_param):
    pixel_data = img.reshape(img.shape[0]*img.shape[1], img.shape[2]).astype(np.float32)
    coeff = bgr_param * np.dot(pixel_data, eigenvec)
    new_pixel_data = np.dot(coeff, inv_eigenvec)
    result_img = new_pixel_data.reshape(*(img.shape))
    result_img[result_img <= 0.0] = 0.0
    result_img[result_img >= 255.0] = 255.0
    result_img = result_img.astype(np.uint8)
    return result_img

def process_gamma(img, gamma):
    if gamma == 1.0:
        result_img = img[...]
    else:
        inv_gamma = 1.0 / gamma
        table = np.array([((i/255.0)**inv_gamma)*255 for i in xrange(0, 256)], dtype=np.uint8)
        result_img = cv2.LUT(img, table)
    return result_img 

def process_blur(img, blur):
    if blur == 0:
        result_img = img[...]
    else:
        result_img = cv2.GaussianBlur(img, (5, 5), blur)
    return result_img

def process_rot_and_scale(img, kp, rot, scale):
    if rot == 0.0 and scale == 0.0: 
        result_img, result_kp = img[...], kp[...]
    else:
        r = np.deg2rad(rot)
        center = np.array(img.shape[:2], dtype=np.float32) / 2.0
        M = cv2.getRotationMatrix2D(center=(center[1], center[0]), angle=rot, scale=scale)
        n_shape = (img.shape[0] * scale, img.shape[1] * scale)
        n_shape = (abs(np.sin(r)*n_shape[1])+abs(np.cos(r)*n_shape[0]), abs(np.sin(r)*n_shape[0])+abs(np.cos(r)*n_shape[1]))
        (tx,ty) = ((n_shape[1]-img.shape[1])/2, (n_shape[0]-img.shape[0])/2)
        M[0,2] += tx
        M[1,2] += ty
        result_img = cv2.warpAffine(img, M, dsize=(int(n_shape[1]), int(n_shape[0])), borderMode=cv2.BORDER_CONSTANT, borderValue=(int(cfg.MEAN_VAL_GBR[0, 0, 0]), int(cfg.MEAN_VAL_GBR[0, 0, 1]), int(cfg.MEAN_VAL_GBR[0, 0, 2])))

        n_center = np.array(result_img.shape[:2], dtype=np.float32) / (2.0 * scale)
        kp_p = kp - center

        # transform to polar coordinate
        theta = np.arctan2(kp_p[:, 1], kp_p[:, 0])
        n_theta = theta + r
        radius = np.sqrt(np.sum(kp_p**2, axis=1))
        n_kp_p = np.transpose(np.array([radius * np.cos(n_theta), radius * np.sin(n_theta)], dtype=np.float32))
        n_kp = n_kp_p + n_center 
        result_kp = (np.round(n_kp * scale)).astype(np.int32)
    return result_img, result_kp, M

def gen_random_transform_code(num):
    """ rot, scale, blur, color, contrast1, contrast2
        rot: [0, -60~60], scale: [1, 0.5~1.5](0.8~1.2 before), blur: [0, 1, 2, 3] ([0, 1, 2] before), gamma: [0.3~3.0]
        color: [0, -0.1~0.1], contrast1: [1, 0.5~2], contrast2: [s[1, 0.25~4], s[1, 0.7~1.4], s[0, -0.1~0.1], v[1, 0.25~4], v[1, 0.7~1.4], v[0, -0.1~0.1]]
    """
    code, code_format = {}, np.zeros((num, 0), dtype=np.float32)
    code['rot'] = np.random.rand(num, 1) * 120.0 - 60.0
    code['scale'] = np.random.rand(num, 1) * 1.0 + 0.5
    code['blur'] = np.random.choice([0, 1, 2, 3], (num, 1))
    code['gamma'] = np.random.rand(num, 1) * 2.7 + 0.3 # add gamma parameter for light transformation
    code['color'] = np.random.rand(num, 1) * 0.2 - 0.1
    code['contrast1'] = np.random.rand(num, 1) * 1.5 + 0.5
    code['contrast2'] = np.random.rand(num, 6)
    code['contrast2'][:, 0] = code['contrast2'][:, 0] * 3.75 + 0.25
    code['contrast2'][:, 1] = code['contrast2'][:, 1] * 0.7 + 0.7 
    code['contrast2'][:, 2] = code['contrast2'][:, 2] * 0.2 - 0.1
    code['contrast2'][:, 0] = code['contrast2'][:, 3] * 3.75 + 0.25
    code['contrast2'][:, 1] = code['contrast2'][:, 4] * 0.7 + 0.7 
    code['contrast2'][:, 2] = code['contrast2'][:, 5] * 0.2 - 0.1
    for trans_name in ['rot', 'scale', 'blur', 'gamma', 'color', 'contrast1', 'contrast2']:
        code_format = np.hstack([code_format, code[trans_name]])
    return code, code_format

def parallel_transform_worker(dir_path, image_list, output_dir, kp_file, output_kp_file, output_rc_file, lock, parallel_trans_progress, trans_num, mode):
    delta = 40 # for homography augmentation 
    f_kp_in = h5py.File(kp_file, 'r')
    color_pca = scipy.io.loadmat('color_pca.mat')
    eigenvec, inv_eigenvec = color_pca['eigenvec'], color_pca['inv_eigenvec']
    for i in range(len(image_list)):
        image_path = os.path.join(dir_path, image_list[i])
        img = cv2.imread(image_path)
        code, code_format = gen_random_transform_code(trans_num)
        keypoint = f_kp_in[image_list[i]][...]
        ##### remove keypoint that can be out of the range after homography transform 
        keypoint_in_range = (keypoint[:,0]>delta)*(keypoint[:,1]>delta)*(keypoint[:,0]<img.shape[0]-delta)*(keypoint[:,1]<img.shape[1]-delta)
        keypoint = keypoint[keypoint_in_range]
        #####
        kp_buffer = process_transform(trans_num, code, code_format, output_dir, image_list[i], img, keypoint, eigenvec, inv_eigenvec, delta, mode)
        have_lock = lock.acquire(True)
        try:
            if have_lock:
                f_kp_out = h5py.File(output_kp_file, 'r+')
                f_rc_out = h5py.File(output_rc_file, 'r+')
                f_kp_out.create_dataset(image_list[i], kp_buffer.shape, dtype=np.float32)
                f_kp_out[image_list[i]][...] = kp_buffer
                f_kp_out.close()
                f_rc_out.create_dataset(image_list[i], code_format.shape, dtype=np.float32)
                f_rc_out[image_list[i]][...] = code_format
                f_rc_out.close()
                parallel_trans_progress.value += 1
        finally:
            if have_lock:
                lock.release()
    f_kp_in.close()
    return 

def parallel_transform(dir_path, image_list_name, output_dir, kp_file, output_kp_file, output_rc_file, proc_num, trans_num, mode):
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    with open(image_list_name, 'r') as f:
        image_list = [x.strip() for x in f.readlines()]
    parallel_trans_progress = Value('i', 0)
    processes, lock = [], Lock()
    work_load = int(np.ceil(float(len(image_list))/float(proc_num)))
    f = h5py.File(output_kp_file, 'w')
    f.close()
    f = h5py.File(output_rc_file, 'w')
    f.close()

    for i in xrange(proc_num):
        time.sleep(1)
        name_list = image_list[i*work_load:(i+1)*work_load]
        p = Process(target=parallel_transform_worker, args=(dir_path, name_list, output_dir, kp_file, output_kp_file, output_rc_file, lock, parallel_trans_progress, trans_num, mode))
        processes.append(p)
        p.start()

    wait_list = np.array([False]*proc_num)
    while np.sum(wait_list) < proc_num:
        sys.stdout.flush()
        sys.stdout.write("cur progress: %d/%d\r" % (parallel_trans_progress.value, len(image_list)))
        for i in xrange(proc_num):
            processes[i].join(0.01)
            wait_list[i] = not processes[i].is_alive()

    print "transformation finishes!"
    # final join all processes 
    for i in xrange(proc_num):
        processes[i].join()
    print "all processes join!"
    return 

def set_caffe(prototxt, model, phase):
    caffe_root = '../new-py-faster-rcnn/caffe-fast-rcnn/'
    sys.path.insert(0, caffe_root + 'python')
    import caffe
    caffe.set_mode_gpu()
    if phase == "TRAIN":
        net = caffe.Net(prototxt, caffe.TRAIN)
    elif phase == "TEST":
        net = caffe.Net(prototxt, model, caffe.TEST)
    else:
        raise Exception("Type Error: %s" % phase)
    return net

def generate_SIFT_CNN_keypoint(dir_path, image_list_name, prototxt, caffemodel, kp_file):
    net = set_caffe(prototxt, caffemodel, "TEST")
    f_kp_out = h5py.File(kp_file, "w")
    with open(image_list_name, 'r') as f:
        image_list = [x.strip() for x in f.readlines()]
    for i in range(len(image_list)):
        sys.stdout.flush()
        sys.stdout.write("cur progress: %d/%d\r" % (i+1, len(image_list)))
        image_path = os.path.join(dir_path, image_list[i])
        img = cv2.imread(image_path, cv2.CV_LOAD_IMAGE_GRAYSCALE)
        net.blobs['data'].reshape(1, 1, img.shape[0], img.shape[1])
        net.blobs['data'].data[0, ...] = np.expand_dims(img, axis=0) - cfg.MEAN_VAL_GRAY
        net.forward()
        #keypoint = net.blobs['keypoint_proposal'].data[0, ...]
        keypoint = net.blobs['keypoint_proposal'].data[:, 1:3]
        f_kp_out.create_dataset(image_list[i], keypoint.shape, dtype=np.float32)
        f_kp_out[image_list[i]][...] = keypoint
    f_kp_out.close()
    return 

def process_homography_transform(img, kp, delta):
    ori_box = np.array([[0, 0], [img.shape[1], 0], [img.shape[1], img.shape[0]], [0, img.shape[0]]], dtype=np.float32)
    H_4pt = (np.random.rand(4, 2) * 2 * delta - delta).astype(np.float32)
    trans_box = ori_box + H_4pt
    M = cv2.getPerspectiveTransform(trans_box, ori_box)
    result_img = cv2.warpPerspective(img, M, dsize=(img.shape[1], img.shape[0]))
    kp_coor = kp[:, ::-1]
    kp_coor = np.concatenate([kp_coor, np.ones((kp_coor.shape[0],1), dtype=np.float32)], axis=1)
    result_kp_coor = np.dot(kp_coor, M.T)
    result_kp_coor /= result_kp_coor[:, [2]]
    result_kp = result_kp_coor[:, (1, 0)]
    return result_img, result_kp, M

def generate_SIFT_OpenCV_keypoint(dir_path, image_list_name, output_file, mode, num_kp):
    f_out = h5py.File(output_file, 'w')
    with open(image_list_name, 'r') as f:
        image_list = [x.strip() for x in f.readlines()]
    sift = cv2.SIFT(nfeatures=500)
    for i in range(len(image_list)):
        sys.stdout.flush()
        sys.stdout.write("cur progress: %d/%d\r" % (i+1, len(image_list)))
        image_path = os.path.join(dir_path, image_list[i])
        img = cv2.imread(image_path)
        kp = sift.detect(img, None)
        index = np.random.choice(range(len(kp)), np.min([len(kp), num_kp]), replace=False)
        sift_data = np.array([kp[x].pt for x in index], dtype=np.float32)[..., ::-1]
        if mode == 1: # output kp_fs_file 
            #rs_data = np.array([[np.deg2rad(kp[x].angle), kp[x].size/2.0*0.6] for x in index], dtype=np.float32)
            rs_data = np.array([[0.0, 1.0] for x in index], dtype=np.float32)
            sift_data = np.hstack([sift_data, rs_data])
        f_out.create_dataset(image_list[i], sift_data.shape, dtype=np.float32)
        f_out[image_list[i]][...] = sift_data
    f_out.close()
    return 

if __name__ == "__main__":
    args = parse_args()
    if args.action == 'calculate_mean':
        dir_path = args.dir_path # target dir contains images to be computed 
        output_filename = args.filename # output filename stored mean value 
        calculate_mean(dir_path, output_filename)
    elif args.action == 'calculate_color_pca':
        dir_path = args.dir_path # target dir contains images to be computed  
        output_filename = args.filename # output filename stored color pca
        calculate_color_pca(dir_path, output_filename)
    elif args.action == 'generate_SIFT_CNN_keypoint':
        dir_path = args.dir_path # target dir contains images to be computed 
        image_list = args.image_list # target image names to be processed (contained in dir_path)
        prototxt = args.prototxt # network prototoxt
        caffemodel = args.caffemodel # network weights
        kp_file = args.kp_file # output keypoint file name 
        generate_SIFT_CNN_keypoint(dir_path, image_list, prototxt, caffemodel, kp_file)
    elif args.action == 'generate_random_transform':
        dir_path = args.dir_path # target dir contains images to be computed 
        image_list = args.image_list # target image names to be processed (contained in dir_path)
        output_dir = args.output_dir # target dir to be output the transformation

        mode = 1
        if args.kp_file != None:
            mode, input_file = 0, args.kp_file # input (and output) keypoint filename 
            output_file = os.path.join(output_dir, 'kp_file.hdf5') # transformed kp file
        elif args.kp_rs_file != None:
            mode, input_file = 1, args.kp_rs_file # input (and output) keypoint, rot, scl filename
            output_file = os.path.join(output_dir, 'kp_rs_file.hdf5') # transformed kp file
        else: 
            print "no input file specified!"
            exit(0)

        output_rc_file = os.path.join(output_dir, 'rc_file.hdf5') # random code file 
        trans_num = args.trans_num # number of transformation 
        proc_num = args.proc_num # number of process 
        parallel_transform(dir_path, image_list, output_dir, input_file, output_file, output_rc_file, proc_num, trans_num, mode)
    elif args.action == 'generate_SIFT_OpenCV_keypoint':
        dir_path = args.dir_path # target dir contains images to be computed 
        image_list = args.image_list # target image names to be processed (contained in dir_path)
        mode = 1 # 0 for kp_file, 1 for kp_rs_file
        if args.kp_file != None:
            mode, output_file = 0, args.kp_file # output keypoint file name
        elif args.kp_rs_file != None:
            mode, output_file = 1, args.kp_rs_file # output keypoint, rot, scl filename
        else: 
            print "no output file specified!"
            exit(0)
        generate_SIFT_OpenCV_keypoint(dir_path, image_list, output_file, mode, args.num_kp)



        












