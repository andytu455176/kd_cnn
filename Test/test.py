import numpy as np 
import sys, os, h5py, cv2, re, argparse, time
from itertools import cycle
import scipy.io
from image_preprocessing import process_rot_and_scale
from multiprocessing import Process, Queue
from lib.config import cfg
import matplotlib.pyplot as plt
from sklearn.metrics import average_precision_score, precision_recall_curve
from lib.KD_Eval import KD_Eval, get_overlap
import cPickle

def parse_arg():
    parser = argparse.ArgumentParser(description='Image preprocessing arguments')
    parser.add_argument('--action', dest='action', help='show_feature_map or evaluation', default=None, type=str)
    parser.add_argument('--dataset', dest='dataset', help='target dataset to test', default=None, type=str)
    parser.add_argument('--case', dest='case', help='test target case for the dataset', default=None, type=str)
    parser.add_argument('--test_image_list', dest='test_image_list', help='test image list for synthesized test', default=None, type=str)
    parser.add_argument('--test_image_num', dest='test_image_num', help='the num of test images count from the head of test image list', type=int)
    parser.add_argument('--output_result', dest='output_result', help='paint the result onto the file', default=None, type=str)
    parser.add_argument('--metric', dest='metric', help='metric to evaluation', default=None, type=str)
    parser.add_argument('--image_dir', dest='image_dir', help='sythesized image dir', default=None, type=str)
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def distance_matrix(X, Y):
    XX = np.tile(np.sum([X**2], axis=2).transpose(), [1, Y.shape[0]]).astype(np.float64)
    YY = np.tile(np.sum([Y**2], axis=2), [X.shape[0], 1]).astype(np.float64)
    D = XX + YY - 2 * np.dot(X, Y.transpose())
    return D

def inner_matrix(X, Y):
    nX = X / np.linalg.norm(X, axis=1).reshape((X.shape[0], 1))
    nY = Y / np.linalg.norm(Y, axis=1).reshape((Y.shape[0], 1))
    return np.dot(nX, nY.transpose())

def get_ground_truth(n_kp, t_kp, image_shape, method, feat1, feat2, Hom, img1_shape, img2_shape):
    if method == "dist_thres":
        ###### simple distance threshold method #####
        #kp_pair_D = distance_matrix(t_kp, n_kp) / np.sqrt(image_shape[0]**2 + image_shape[1]**2)
        #pair_true_coor = np.array(np.where(kp_pair_D < 0.005), dtype=np.int32)
        ###### release match criteria #####
        D = distance_matrix(t_kp, n_kp)
        D[D < 0] = 0
        kp_pair_D = np.sqrt(D) / np.sqrt(image_shape[0]**2 + image_shape[1]**2)
        pair_true_coor = np.array(np.where(kp_pair_D < 0.005), dtype=np.int32)
        pair_true = np.zeros((kp_pair_D.shape[0]*kp_pair_D.shape[1],), dtype=np.float32)
        pair_true[pair_true_coor[0] + pair_true_coor[1]*kp_pair_D.shape[0]] = 1.0
        print "ground truth: ", pair_true_coor.shape[1]
        #############################################
    elif method == "nn":
        ##### finding one2one correspondences #####
        wout, twout, dout, tdout = get_overlap(feat1, feat2, img1_shape, img2_shape, Hom) # return non common_part
        pair_true = ((twout < 50).T).reshape(wout.shape[0]*wout.shape[1]) * 1.0
        print "ground truth: ", np.sum(pair_true)
        ###########################################
    return pair_true

def compute_corres_performance(desc, n_desc, pair_true, name):
    D = distance_matrix(desc, n_desc)
    #D = inner_matrix(desc, n_desc)
    index1 = np.arange(desc.shape[0])
    index2 = np.arange(n_desc.shape[0])
    idx_pairs = np.transpose([np.tile(index1, index2.shape[0]), np.repeat(index2, index1.shape[0])])
    pair_score = -1.0 * D[idx_pairs[:, 0], idx_pairs[:, 1]]
    s = average_precision_score(pair_true, pair_score)
    print (name + ": "), s
    ###### decide final matching threshold #####
    #precision, recall, threshold = precision_recall_curve(pair_true, pair_score)
    #f1_score = 2 * np.multiply(precision, recall) / (precision + recall)
    #thres = threshold[np.argmax(f1_score)]
    ############################################
    return s

def synthesized_image_test_rot_mAP(image_dir, test_image, rot_range, method, desc=[{"case":"SIFT", "name":"SIFT"}], output_image="", nb_kp=500):
    #image_dir = "../Flicker8k_Dataset/"
    total = np.zeros((len(desc), len(rot_range)), dtype=np.float32)

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(image_dir)
    
    for name in test_image:
        image_bgr = cv2.imread(image_dir + name)
        ##### resize image to fit in gpu memory after transformation #####
        print image_dir + name, image_bgr
        max_shape = np.max(image_bgr.shape)
        if (image_bgr.shape[0]*image_bgr.shape[1]) > (500.0*500.0):
            scale = 450.0 / float(max_shape)
            image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
        ##################################################################
        descriptor = []
        #keypoint, kp = sift_detect(image_bgr, nb_kp=nb_kp)
        feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_ori", image_bgr, nb_kp=nb_kp)
        for i, d in enumerate(desc):
            descriptor.append(get_descriptor(desc_history, desc_target_dir, name[:-4]+"_ori"+d['name'], d, image_bgr, keypoint))

        for j, rot in enumerate(rot_range):
            n_image_bgr, t_kp, M = process_rot_and_scale(image_bgr, kp, rot, 1.0)

            #n_keypoint, n_kp = sift_detect(n_image_bgr, nb_kp=nb_kp)
            feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_rot_"+str(rot), n_image_bgr, nb_kp=nb_kp)

            Hom = np.concatenate([M.copy(), np.array([[0, 0, 1]], dtype=np.float32)], axis=0)
            pair_true = get_ground_truth(n_kp, t_kp, n_image_bgr.shape[:2], method, feat1, feat2, Hom, image_bgr.shape, n_image_bgr.shape)

            for i, d in enumerate(desc):
                n_descriptor = get_descriptor(desc_history, desc_target_dir, name[:-4]+"_rot_"+str(rot)+d['name'], d, n_image_bgr, n_keypoint)

                ap = compute_corres_performance(descriptor[i], n_descriptor, pair_true, d['name'])
                total[i, j] += ap

    total = total / float(len(test_image))
    history_file_save(image_dir, kp_history, desc_history)
    return total

def synthesized_image_test_scl_mAP(image_dir, test_image, scl_range, method, desc=[{"case":"SIFT", "name":"SIFT"}], output_image="", nb_kp=500):
    #image_dir = "../Flicker8k_Dataset/"
    total = np.zeros((len(desc), len(scl_range)), dtype=np.float32)

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(image_dir)
    
    for name in test_image:
        image_bgr = cv2.imread(image_dir + name)
        ##### resize image to fit in gpu memory after transformation #####
        max_shape = np.max(image_bgr.shape)
        if (image_bgr.shape[0]*image_bgr.shape[1]) > (500.0*500.0):
            scale = 450.0 / float(max_shape)
            image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
        ##################################################################
        descriptor = []
        #keypoint, kp = sift_detect(image_bgr, nb_kp=nb_kp)
        feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_ori", image_bgr, nb_kp=nb_kp)
        for i, d in enumerate(desc):
            descriptor.append(get_descriptor(desc_history, desc_target_dir, name[:-4]+"_ori"+d['name'], d, image_bgr, keypoint))

        for j, scl in enumerate(scl_range):
            n_image_bgr, t_kp, M = process_rot_and_scale(image_bgr, kp, 0.0, scl)
            #n_keypoint, n_kp = sift_detect(n_image_bgr, nb_kp=nb_kp)
            feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_scl_"+str(scl), n_image_bgr, nb_kp=nb_kp)

            Hom = np.concatenate([M.copy(), np.array([[0, 0, 1]], dtype=np.float32)], axis=0)
            pair_true = get_ground_truth(n_kp, t_kp, n_image_bgr.shape[:2], method, feat1, feat2, Hom, image_bgr.shape, n_image_bgr.shape)

            for i, d in enumerate(desc):
                n_descriptor = get_descriptor(desc_history, desc_target_dir, name[:-4]+"_scl_"+str(scl)+d['name'], d, n_image_bgr, n_keypoint)
                ap = compute_corres_performance(descriptor[i], n_descriptor, pair_true, d['name'])
                total[i, j] += ap

    total = total / float(len(test_image))
    history_file_save(image_dir, kp_history, desc_history)
    return total

def Mikolajczyk_dataset_test_mAP(target_dir, desc, method, suffix="ppm", nb_kp=500):
    base_dir = "../data/Mikolajczyk_dataset"
    print target_dir
    image_dir = os.path.join(base_dir, target_dir)
    matrix_list = []

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(base_dir)
    
    for i in xrange(5):
        matrix_list.append(read_matrix(os.path.join(image_dir, "H1to%dp" % (i+2))))
    image_index = [x+2 for x in xrange(5)]
    result = np.zeros((len(desc), 5), dtype=np.float32)

    image_bgr = cv2.imread(os.path.join(image_dir, "img1.%s" % suffix))
    ###### resize the image to fit in the gpu memory ######
    max_shape = np.max(image_bgr.shape)
    if max_shape > 700.0:
        scale = 700.0 / float(max_shape)
        image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
    else:
        scale = 1.0
    scale_matrix = np.array([[scale, 0, 0], [0, scale, 0], [0, 0, 1]], dtype=np.float32)
    #######################################################
    descriptor = []
    #keypoint, kp = sift_detect(image_bgr, nb_kp=nb_kp)
    feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, "img1_"+target_dir, image_bgr, nb_kp=nb_kp)
    for i, d in enumerate(desc):
        descriptor.append(get_descriptor(desc_history, desc_target_dir, "img1_"+target_dir+"_"+d['name'], d, image_bgr, keypoint))

    for j, index in enumerate(image_index):
        n_image_bgr = cv2.imread(os.path.join(image_dir, "img%d.%s" % (index, suffix)))
        ###### resize the image to fit in the gpu memory ######
        n_image_bgr = cv2.resize(n_image_bgr, None, None, fx=scale, fy=scale)
        #######################################################
        #n_keypoint, n_kp = sift_detect(n_image_bgr, nb_kp=nb_kp)
        feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, ("img%d_" % index)+target_dir, n_image_bgr, nb_kp=nb_kp)
        
        ###### temporary test without remove out of bound points myself
        #t_kp_coor = calculate_trans_points(kp[...,::-1], matrix_list[j])
        #kp_is_valid = (t_kp_coor[:, 0]>=0)*(t_kp_coor[:, 1]>=0)*(t_kp_coor[:, 0]<n_image_bgr.shape[1])*(t_kp_coor[:, 1]<n_image_bgr.shape[0])
        #t_kp = t_kp_coor[kp_is_valid][..., ::-1]
        t_kp = n_kp
        kp_is_valid = np.ones((kp.shape[0],), dtype=np.bool)
        ######

        Hom = matrix_list[j]
        ###### refine the homography matrix due to the resize of image ######
        Hom = np.dot(np.dot(scale_matrix, Hom), np.linalg.inv(scale_matrix))
        #####################################################################
        pair_true = get_ground_truth(n_kp, t_kp, n_image_bgr.shape[:2], method, feat1, feat2, Hom, image_bgr.shape, n_image_bgr.shape)

        for i, d in enumerate(desc):
            n_descriptor = get_descriptor(desc_history, desc_target_dir, ("img%d_" % index)+target_dir+"_"+d['name'], d, n_image_bgr, n_keypoint)
            ap = compute_corres_performance(descriptor[i][kp_is_valid], n_descriptor, pair_true, d['name'])
            result[i, j] = ap

    history_file_save(base_dir, kp_history, desc_history)
    return result

def synthesized_image_test_rot_matching_score(image_dir, test_image, rot_range, desc=[{"case":"SIFT", "name":"SIFT"}], output_image="", nb_kp=500):
    #image_dir = "../Flicker8k_Dataset/"
    total = np.zeros((len(desc), len(rot_range)), dtype=np.float32) # for match_score currently

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(image_dir)
    
    for name in test_image:
        image_bgr = cv2.imread(image_dir + name)
        ##### resize image to fit in gpu memory after transformation #####
        max_shape = np.max(image_bgr.shape)
        if (image_bgr.shape[0]*image_bgr.shape[1]) > (500.0*500.0):
            scale = 450.0 / float(max_shape)
            image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
        ##################################################################
        descriptor = []
        #keypoint, kp = sift_detect(image_bgr, nb_kp=nb_kp)
        feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_ori", image_bgr, nb_kp=nb_kp)
        for i, d in enumerate(desc):
            descriptor.append(get_descriptor(desc_history, desc_target_dir, name[:-4]+"_ori"+d['name'], d, image_bgr, keypoint))

        for j, rot in enumerate(rot_range):
            n_image_bgr, t_kp, M = process_rot_and_scale(image_bgr, kp, rot, 1.0)
            #n_keypoint, n_kp = sift_detect(n_image_bgr, nb_kp=nb_kp)
            feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_rot_"+str(rot), n_image_bgr, nb_kp=nb_kp)
            #pair_true = get_ground_truth(n_kp, t_kp, n_image_bgr.shape[:2])
            Hom = np.concatenate([M.copy(), np.array([[0, 0, 1]], dtype=np.float32)], axis=0)

            for i, d in enumerate(desc):
                n_descriptor = get_descriptor(desc_history, desc_target_dir, name[:-4]+"_rot_"+str(rot)+d['name'], d, n_image_bgr, n_keypoint)
                #ap = compute_corres_performance(descriptor[i], n_descriptor, pair_true, d['name'])
                #total[i, j] += ap
                f1 = np.concatenate([feat1, descriptor[i]], axis=1)
                f2 = np.concatenate([feat2, n_descriptor], axis=1)
                repeat, match_score = KD_Eval(f1, f2, image_bgr.shape, n_image_bgr.shape, Hom, descriptor[i].shape[1], 0, -1, -1)
                print "repeatability: ", repeat[3], "matching score: ", match_score
                total[i, j] += match_score

    total = total / float(len(test_image))
    history_file_save(image_dir, kp_history, desc_history)
    return total

def synthesized_image_test_scl_matching_score(image_dir, test_image, scl_range, desc=[{"case":"SIFT", "name":"SIFT"}], output_image="", nb_kp=500):
    #image_dir = "../Flicker8k_Dataset/"
    total = np.zeros((len(desc), len(scl_range)), dtype=np.float32) # for match_score currently

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(image_dir)
    
    for name in test_image:
        image_bgr = cv2.imread(image_dir + name)
        ##### resize image to fit in gpu memory after transformation #####
        max_shape = np.max(image_bgr.shape)
        if (image_bgr.shape[0]*image_bgr.shape[1]) > (500.0*500.0):
            scale = 450.0 / float(max_shape)
            image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
        ##################################################################
        descriptor = []
        #keypoint, kp = sift_detect(image_bgr, nb_kp=nb_kp)
        feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_ori", image_bgr, nb_kp=nb_kp)
        for i, d in enumerate(desc):
            descriptor.append(get_descriptor(desc_history, desc_target_dir, name[:-4]+"_ori"+d['name'], d, image_bgr, keypoint))

        for j, scl in enumerate(scl_range):
            n_image_bgr, t_kp, M = process_rot_and_scale(image_bgr, kp, 0.0, scl)
            #n_keypoint, n_kp = sift_detect(n_image_bgr, nb_kp=nb_kp)
            feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, name[:-4]+"_scl_"+str(scl), n_image_bgr, nb_kp=nb_kp)
            #pair_true = get_ground_truth(n_kp, t_kp, n_image_bgr.shape[:2])
            Hom = np.concatenate([M.copy(), np.array([[0, 0, 1]], dtype=np.float32)], axis=0)

            for i, d in enumerate(desc):
                n_descriptor = get_descriptor(desc_history, desc_target_dir, name[:-4]+"_scl_"+str(scl)+d['name'], d, n_image_bgr, n_keypoint)
                #ap = compute_corres_performance(descriptor[i], n_descriptor, pair_true, d['name'])
                #total[i, j] += ap
                f1 = np.concatenate([feat1, descriptor[i]], axis=1)
                f2 = np.concatenate([feat2, n_descriptor], axis=1)
                repeat, match_score = KD_Eval(f1, f2, image_bgr.shape, n_image_bgr.shape, Hom, descriptor[i].shape[1], 0, -1, -1)
                print "repeatability: ", repeat[3], "matching score: ", match_score
                total[i, j] += match_score

    total = total / float(len(test_image))
    history_file_save(image_dir, kp_history, desc_history)
    return total

def Mikolajczyk_dataset_test_matching_score(target_dir, desc, suffix="ppm", nb_kp=500):
    base_dir = "../data/Mikolajczyk_dataset/"
    image_dir = os.path.join(base_dir, target_dir)
    matrix_list = []

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(base_dir)
    
    for i in xrange(5):
        matrix_list.append(read_matrix(os.path.join(image_dir, "H1to%dp" % (i+2))))
    image_index = [x+2 for x in xrange(5)]
    result = np.zeros((len(desc), 5), dtype=np.float32)

    image_bgr = cv2.imread(os.path.join(image_dir, "img1.%s" % suffix))
    ###### resize the image to fit in the gpu memory ######
    max_shape = np.max(image_bgr.shape)
    if max_shape > 700.0:
        scale = 700.0 / float(max_shape)
        image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
    else:
        scale = 1.0
    scale_matrix = np.array([[scale, 0, 0], [0, scale, 0], [0, 0, 1]], dtype=np.float32)
    #######################################################
    descriptor = []
    #keypoint, kp = sift_detect(image_bgr, nb_kp=nb_kp)
    feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, "img1_"+target_dir, image_bgr, nb_kp=nb_kp)
    for i, d in enumerate(desc):
        descriptor.append(get_descriptor(desc_history, desc_target_dir, "img1_"+target_dir+"_"+d['name'], d, image_bgr, keypoint))

    for j, index in enumerate(image_index):
        n_image_bgr = cv2.imread(os.path.join(image_dir, "img%d.%s" % (index, suffix)))
        ###### resize the image to fit in the gpu memory #####
        n_image_bgr = cv2.resize(n_image_bgr, None, None, fx=scale, fy=scale)
        ######################################################
        #n_keypoint, n_kp = sift_detect(n_image_bgr, nb_kp=nb_kp)
        feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, ("img%d_" % index)+target_dir, n_image_bgr, nb_kp=nb_kp)
        
        #t_kp_coor = calculate_trans_points(kp[...,::-1], matrix_list[j])
        #kp_is_valid = (t_kp_coor[:, 0]>=0)*(t_kp_coor[:, 1]>=0)*(t_kp_coor[:, 0]<n_image_bgr.shape[1])*(t_kp_coor[:, 1]<n_image_bgr.shape[0])
        #t_kp = t_kp_coor[kp_is_valid][..., ::-1]
        #pair_true = get_ground_truth(n_kp, t_kp, n_image_bgr.shape[:2])

        Hom = matrix_list[j]
        ###### refine the homography matrix due to the resize of image ######
        Hom = np.dot(np.dot(scale_matrix, Hom), np.linalg.inv(scale_matrix))
        #####################################################################

        for i, d in enumerate(desc):
            n_descriptor = get_descriptor(desc_history, desc_target_dir, ("img%d_" % index)+target_dir+"_"+d['name'], d, n_image_bgr, n_keypoint)
            #ap = compute_corres_performance(descriptor[i][kp_is_valid], n_descriptor, pair_true, d['name'])
            #result[i, j] = ap
            f1 = np.concatenate([feat1, descriptor[i]], axis=1)
            f2 = np.concatenate([feat2, n_descriptor], axis=1)
            repeat, match_score = KD_Eval(f1, f2, image_bgr.shape, n_image_bgr.shape, Hom, descriptor[i].shape[1], 1, -1, -1)
            print "repeatability: ", repeat[3], "matching score: ", match_score
            result[i, j] += match_score

    history_file_save(base_dir, kp_history, desc_history)
    return result

def Webcam_dataset_test_mAP(target_dir, desc, method, nb_kp=500):
    #base_dir = "~/Downloads/Webcam_Dataset/"
    base_dir = "/home/iis/Downloads/Webcam_Dataset/"
    image_dir = os.path.join(base_dir+target_dir, "test/image_gray/")
    with open(os.path.join(base_dir+target_dir, "test/test_imgs.txt"), "r") as f:
        image_list = [os.path.split(x.strip())[-1] for x in f.readlines()]

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(base_dir)
    
    index = range(20)
    #np.random.shuffle(index)
    total = np.zeros((len(desc), 1), dtype=np.float32) # for match_score currently
    for i in range(10):
        image_bgr = cv2.imread(image_dir+image_list[index[i]])
        ###### resize the image to fit in the gpu memory ######
        max_shape = np.max(image_bgr.shape)
        if max_shape > 800.0:
            scale = 800.0 / float(max_shape)
            image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
        else:
            scale = 1.0
        scale_matrix = np.array([[scale, 0, 0], [0, scale, 0], [0, 0, 1]], dtype=np.float32)
        #######################################################
        feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, ("%s_" % image_list[index[i]][:-4])+target_dir, image_bgr, nb_kp=nb_kp)
        n_image_bgr = cv2.imread(image_dir+image_list[index[i+10]])
        ###### resize the image to fit in the gpu memory ######
        n_image_bgr = cv2.resize(n_image_bgr, None, None, fx=scale, fy=scale)
        #######################################################
        feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, ("%s_" % image_list[index[i+10]][:-4])+target_dir, n_image_bgr, nb_kp=nb_kp)

        Hom = np.eye(3)
        ###### refine the homography matrix due to the resize of image ######
        Hom = np.dot(np.dot(scale_matrix, Hom), np.linalg.inv(scale_matrix))
        #####################################################################
        pair_true = get_ground_truth(n_kp, kp, n_image_bgr.shape[:2], method, feat1, feat2, Hom, image_bgr.shape, n_image_bgr.shape)
        for j, d in enumerate(desc):
            descriptor = get_descriptor(desc_history, desc_target_dir, ("%s_" % image_list[index[i]][:-4])+target_dir+"_"+d['name'], d, image_bgr, keypoint)
            n_descriptor = get_descriptor(desc_history, desc_target_dir, ("%s_" % image_list[index[i+10]][:-4])+target_dir+"_"+d['name'], d, n_image_bgr, n_keypoint)
            ap = compute_corres_performance(descriptor, n_descriptor, pair_true, d['name'])
            total[j, 0] += ap

    total = total / float(10)
    history_file_save(base_dir, kp_history, desc_history)
    return total

def Webcam_dataset_test_matching_score(target_dir, desc, nb_kp=500):
    #base_dir = "~/Downloads/Webcam_Dataset/"
    base_dir = "/home/iis/Downloads/Webcam_Dataset/"
    image_dir = os.path.join(base_dir+target_dir, "test/image_gray/")
    with open(os.path.join(base_dir+target_dir, "test/test_imgs.txt"), "r") as f:
        image_list = [os.path.split(x.strip())[-1] for x in f.readlines()]

    kp_target_dir, kp_history, desc_target_dir, desc_history = history_file_load(base_dir)

    index = range(20)
    #np.random.shuffle(index)
    total = np.zeros((len(desc), 1), dtype=np.float32) # for match_score currently
    repeat_mean = np.zeros((len(desc), 1), dtype=np.float32)
    for i in range(10):
        image_bgr = cv2.imread(image_dir+image_list[index[i]])
        ###### resize the image to fit in the gpu memory ######
        max_shape = np.max(image_bgr.shape)
        if max_shape > 800.0:
            scale = 800.0 / float(max_shape)
            image_bgr = cv2.resize(image_bgr, None, None, fx=scale, fy=scale)
        else:
            scale = 1.0
        scale_matrix = np.array([[scale, 0, 0], [0, scale, 0], [0, 0, 1]], dtype=np.float32)
        #######################################################
        feat1, keypoint, kp = sift_detect_feat(kp_history, kp_target_dir, ("%s_" % image_list[index[i]][:-4])+target_dir, image_bgr, nb_kp=nb_kp)
        n_image_bgr = cv2.imread(image_dir+image_list[index[i+10]])
        ###### resize the image to fit in the gpu memory ######
        n_image_bgr = cv2.resize(n_image_bgr, None, None, fx=scale, fy=scale)
        #######################################################
        feat2, n_keypoint, n_kp = sift_detect_feat(kp_history, kp_target_dir, ("%s_" % image_list[index[i+10]][:-4])+target_dir, n_image_bgr, nb_kp=nb_kp)

        Hom = np.eye(3)
        ###### refine the homography matrix due to the resize of image ######
        Hom = np.dot(np.dot(scale_matrix, Hom), np.linalg.inv(scale_matrix))
        #####################################################################

        for j, d in enumerate(desc):
            descriptor = get_descriptor(desc_history, desc_target_dir, ("%s_" % image_list[index[i]][:-4])+target_dir+"_"+d['name'], d, image_bgr, keypoint)
            n_descriptor = get_descriptor(desc_history, desc_target_dir, ("%s_" % image_list[index[i+10]][:-4])+target_dir+"_"+d['name'], d, n_image_bgr, n_keypoint)

            f1 = np.concatenate([feat1, descriptor], axis=1)
            f2 = np.concatenate([feat2, n_descriptor], axis=1)
            repeat, match_score = KD_Eval(f1, f2, image_bgr.shape, n_image_bgr.shape, Hom, descriptor.shape[1], 0, -1, -1)
            print repeat
            print "repeatability: ", repeat[3], "matching score: ", match_score
            total[j, 0] += match_score
            repeat_mean[j, 0] += repeat[3]

    total = total / float(10)
    repeat_mean = repeat_mean / float(10)
    history_file_save(base_dir, kp_history, desc_history)
    
    print "mean matching score:\t", total[j, 0]
    print "mean repeatability:\t", repeat_mean[j, 0]
    return total

def DTU_dataset_test_mAP():
    return 

def DTU_dataset_test_matching_score():
    return 

def calculate_trans_points(kp_coor, matrix):
    kp_coor_ones = np.hstack([kp_coor, np.ones((kp_coor.shape[0], 1), dtype=np.float32)])
    n_kp_coor = np.dot(matrix, kp_coor_ones.T)
    return n_kp_coor[:2, :].T

def read_matrix(filename):
    matrix = np.zeros((3, 3), dtype=np.float32)
    with open(filename, 'r') as f:
        for i in xrange(3):
            matrix[i, :] = np.array(filter(None, re.split(' |\t', f.readline().strip())), dtype=np.float32)
    return matrix

def sift_detect(image_bgr, nb_kp=500):
    sift = cv2.SIFT()
    image_gray = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2GRAY)
    keypoint = sift.detect(image_gray, None)
    response_array = np.array([x.response for x in keypoint], dtype=np.float32)
    sorted_idx = np.argsort(response_array)[::-1]
    keypoint = [keypoint[i] for i in sorted_idx][:nb_kp]
    kp = np.array([x.pt for x in keypoint], dtype=np.float32)[..., ::-1]
    return keypoint, kp

def sift_detect_feat(history, target_dir, image_id, image_bgr, nb_kp=500, is_history=False):
    """
    return (nb_kp, 5) numpy array according to repeatability.m format
    """
    # load from history file if exist
    if image_id in history and is_history:
        temp = scipy.io.loadmat(target_dir+image_id+".mat")
        feat = temp['feat']
        kp = temp['kp']
        keypoint = load_keypoint(target_dir+image_id+".pkl")
        return feat, keypoint, kp

    ### original version ###
    #sift = cv2.SIFT()
    ### just pick strong 500 by SIFT ###
    sift = cv2.SIFT(nfeatures=500)
    image_gray = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2GRAY)
    keypoint  = sift.detect(image_gray, None)
    ### temp not sorted ###
    #response_array = np.array([x.response for x in keypoint], dtype=np.float32)
    #sorted_idx = np.argsort(response_array)[::-1]
    #keypoint = [keypoint[i] for i in sorted_idx][:nb_kp]
    #######################
    feat = np.zeros((len(keypoint), 5), dtype=np.float32)
    feat[:, 0:2] = np.array([x.pt for x in keypoint], dtype=np.float32)
    feat[:, 2:5] = np.array([[1/(0.5*x.size)**2, 0, 1/(0.5*x.size)**2] for x in keypoint], dtype=np.float32)
    #feat[:, 2:5] = np.array([[1.0, 0.0, 1.0] for x in keypoint], dtype=np.float32)
    kp = np.array([x.pt for x in keypoint], dtype=np.float32)[..., ::-1]
    
    # store to history file
    temp = {}
    temp['feat'] = feat
    temp['kp'] = kp
    scipy.io.savemat(target_dir+image_id+".mat", temp)
    save_keypoint(target_dir+image_id+".pkl", keypoint)
    history.add(image_id)
    return feat, keypoint, kp

def save_keypoint(filename, keypoint):
    index = []
    for point in keypoint:
        temp = (point.pt, point.size, point.angle, point.response, point.octave, point.class_id)
        index.append(temp)
    with open(filename, "w") as f:
        cPickle.dump(index, f)
    return 

def load_keypoint(filename):
    with open(filename, "r") as f:
        index = cPickle.load(f)
    keypoint = []
    for point in index:
        temp = cv2.KeyPoint(x=point[0][0], y=point[0][1], _size=point[1], _angle=point[2], _response=point[3], _octave=point[4], _class_id=point[5])
        keypoint.append(temp)
    return keypoint

def get_descriptor(history, target_dir, image_id, desc, image_bgr, keypoint, patch_size=32, is_history=False):
    """
    filler: { "kp": kp, "angle": angle, "scale": scale, "half": patch_size//2 }
    """
    if image_id in history and is_history:
        descriptor = scipy.io.loadmat(target_dir+image_id+".mat")['desc']
        return descriptor

    if desc['case'] == "SIFT":
        sift = cv2.SIFT()
        image_gray = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2GRAY)
        _, descriptor = sift.compute(image_gray, keypoint)
    elif desc['case'] == "CNN":
        net = desc['net']
        image = np.transpose(image_bgr - cfg.MEAN_VAL_GBR, (2, 0, 1))[np.newaxis, ...]
        net.blobs['data'].reshape(1, 3, image.shape[2], image.shape[3])
        net.blobs['data'].data[...] = image
        kp = np.array([x.pt for x in keypoint], dtype=np.float32)[..., ::-1]
        angle = np.array([x.angle for x in keypoint], dtype=np.float32)
        scale = np.array([x.size*0.6/2 for x in keypoint], dtype=np.float32)
        filler = { "kp": kp, "angle": angle, "scale": scale, "half": patch_size//2 }
        # network preprocessing for various type of network 
        network_preprocessing(net, filler, desc['preproc'])
        net.forward()
        descriptor = net.blobs[desc['layer']].data[...].copy()

        #descriptor = np.max(net.blobs['pool2'].data[...].copy(), axis=(2, 3))
        ###### normalize descriptor to unit vector 
        #descriptor = descriptor / np.linalg.norm([descriptor], axis=2).T

    temp = {}
    temp['desc'] = descriptor
    scipy.io.savemat(target_dir+image_id+".mat", temp)
    history.add(image_id)
    return descriptor

def network_preprocessing(net, filler, mode):
    if mode == "roi_pooling":
        rois = np.zeros((filler['kp'].shape[0], 5), dtype=np.float32)
        rois[:, 0] = 0.0
        rois[:, 1:] = np.array([filler['kp'][:, 1]-filler['half'], filler['kp'][:, 0]-filler['half'], filler['kp'][:, 1]+filler['half'], filler['kp'][:, 0]+filler['half']], dtype=np.float32).T
        net.blobs['rois'].reshape(*(rois.shape))
        net.blobs['rois'].data[...] = rois
    elif mode == "roi_trans":
        rois = np.zeros((filler['kp'].shape[0], 5), dtype=np.float32)
        rois[:, 0] = 0.0
        rois[:, 1:3] = np.array([filler['kp'][:, 0], filler['kp'][:, 1]], dtype=np.float32).T
        rois[:, 3] = np.deg2rad(filler['angle'])
        rois[:, 4] = filler['scale']
        net.blobs['rois'].reshape(*(rois.shape))
        net.blobs['rois'].data[...] = rois
    elif mode == "keypoint_proposal":
        kp_proposal = np.zeros((filler['kp'].shape[0], 3), dtype=np.float32) ## change 5 to 3
        kp_proposal[:, 0] = 0.0
        kp_proposal[:, 1:3] = np.array([filler['kp'][:, 0], filler['kp'][:, 1]], dtype=np.float32).T
        net.blobs['keypoint_proposal'].reshape(*(kp_proposal.shape))
        net.blobs['keypoint_proposal'].data[...] = kp_proposal
    elif mode == "keypoint_proposal_scale":
        kp_proposal = np.zeros((filler['kp'].shape[0], 3), dtype=np.float32)
        kp_proposal[:, 0] = 0.0
        kp_proposal[:, 1:3] = np.array([filler['kp'][:, 0], filler['kp'][:, 1]], dtype=np.float32).T
        net.blobs['keypoint_proposal'].reshape(*(kp_proposal.shape))
        net.blobs['keypoint_proposal'].data[...] = kp_proposal
        net.blobs['scale'].reshape(filler['scale'].shape[0], 1, 1, 1)
        net.blobs['scale'].data[:, 0, 0, 0] = filler['scale'][...]

    return 

def show_feature_map(prototxt, caffemodel, nb_kp=100):
    caffe_root = '/home/iis/Downloads/py-faster-rcnn/caffe-fast-rcnn/'
    sys.path.insert(0, caffe_root + 'python')
    import caffe
    caffe.set_mode_gpu()

    net = caffe.Net(prototxt, caffemodel, caffe.TEST)
    image_bgr = cv2.imread('../Flicker8k_Dataset/619169586_0a13ee7c21.jpg')
    sift = cv2.SIFT()
    image_gray = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2GRAY)
    #keypoint, kp = sift_detect(image_bgr, nb_kp=nb_kp)
    image = np.transpose(image_bgr - cfg.MEAN_VAL_GBR, (2, 0, 1))[np.newaxis, ...]
    net.blobs['data'].reshape(1, 3, image.shape[2], image.shape[3])
    net.blobs['data'].data[...] = image
    
    # network preprocessing for various type of network 
    #network_preprocessing(net, filler, desc['preproc'])
    net.forward()

    print net.params['Grad'][0].data[...]

    plt.subplot(3, 4, 1)
    plt.imshow(net.blobs['o1_dog'].data[0, 0, ...], cmap='Greys_r')
    plt.subplot(3, 4, 2)
    plt.imshow(net.blobs['o1_dog'].data[0, 1, ...], cmap='Greys_r')
    plt.subplot(3, 4, 3)
    plt.imshow(net.blobs['o1_dog'].data[0, 2, ...], cmap='Greys_r')
    plt.subplot(3, 4, 4)
    plt.imshow(net.blobs['o1_dog'].data[0, 3, ...], cmap='Greys_r')
    plt.subplot(3, 4, 5)
    plt.imshow(net.blobs['o2_dog'].data[0, 0, ...], cmap='Greys_r')
    plt.subplot(3, 4, 6)
    plt.imshow(net.blobs['o2_dog'].data[0, 1, ...], cmap='Greys_r')
    plt.subplot(3, 4, 7)
    plt.imshow(net.blobs['o2_dog'].data[0, 2, ...], cmap='Greys_r')
    plt.subplot(3, 4, 8)
    plt.imshow(net.blobs['o2_dog'].data[0, 3, ...], cmap='Greys_r')
    plt.subplot(3, 4, 9)
    plt.imshow(net.blobs['grad'].data[0, 0, ...], cmap='Greys_r')
    plt.subplot(3, 4, 10)
    plt.imshow(net.blobs['grad'].data[0, 1, ...], cmap='Greys_r')
    plt.subplot(3, 4, 11)
    plt.imshow(net.blobs['max_bin'].data[0, 0, ...], cmap='Greys_r')
    plt.show()
    return 

def generate_network(prototxt, model_dir, caffemodel):
    net = caffe.Net(prototxt, os.path.join(model_dir, caffemodel), caffe.TEST)
    return net 

def run_test(args, test_image, desc_in, queue):
    #caffe_root = '/home/iis/Downloads/py-faster-rcnn/caffe-fast-rcnn/'

    if desc_in['case'] == "CNN":
        caffe_root = desc_in['caffe_root']
        sys.path.insert(0, caffe_root + 'python')
        import caffe
        caffe.set_mode_gpu()
        desc_in['net'] = caffe.Net(desc_in['prototxt'], desc_in['caffemodel'], caffe.TEST)

    param = get_test_param(args.dataset, args.case)
    if args.dataset == "Mikolajczyk":
        suffix = "ppm" if args.case != "boat" else "pgm"
        if args.metric == "match_score":
            total = Mikolajczyk_dataset_test_matching_score(args.case, [desc_in], suffix=suffix)
        elif args.metric[:3] == "mAP": # mAP_dist_thres, mAP_nn
            total = Mikolajczyk_dataset_test_mAP(args.case, [desc_in], args.metric[4:], suffix=suffix)
    elif args.dataset == "Synthesized":
        if args.case == "rot":
            if args.metric == "match_score":
                total= synthesized_image_test_rot_matching_score(args.image_dir, test_image, param, [desc_in])
            elif args.metric[:3] == "mAP": # mAP_dist_thres, mAP_nn
                total= synthesized_image_test_rot_mAP(args.image_dir, test_image, param, args.metric[4:], [desc_in])
        elif args.case == "scl":
            if args.metric == "match_score":
                total = synthesized_image_test_scl_matching_score(args.image_dir, test_image, param, [desc_in])
            elif args.metric[:3] == "mAP": # mAP_dist_thres, mAP_nn
                total = synthesized_image_test_scl_mAP(args.image_dir, test_image, param, args.metric[4:], [desc_in])
    elif args.dataset == "Webcam":
        total = np.zeros((len([desc_in]), 0), dtype=np.float32)
        for target_dir in param:
            if args.metric == "match_score":
                cur_total = Webcam_dataset_test_matching_score(target_dir, [desc_in])
            elif args.metric[:3] == "mAP": # mAP_dist_thres, mAP_nn
                cur_total = Webcam_dataset_test_mAP(target_dir, [desc_in], args.metric[4:])
            total = np.hstack([total, cur_total])
    queue.put(total)

    desc_in['net'] = None
    return 

def show_train_data(target_dir, prototxt, caffemodel):
    os.chdir(target_dir)
    caffe_root = '/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/'
    sys.path.insert(0, caffe_root + 'python')
    import caffe
    caffe.set_mode_gpu()

    net = caffe.Net(prototxt, caffemodel, caffe.TEST)
    image_bgr = cv2.imread('../Flicker8k_Dataset/619169586_0a13ee7c21.jpg')
    net.forward()

    target_index = 0
    image = (np.transpose(net.blobs['data'].data[target_index, ...].copy(), (1, 2, 0))+cfg.MEAN_VAL_GBR)[..., ::-1]
    plt.imshow(image.astype(np.uint8))
    keypoint = net.blobs['keypoint_proposal'].data[...].copy()
    print keypoint.shape
    index = np.where(keypoint[:, 0] == target_index)[0]
    plt.plot(keypoint[index, 1], keypoint[index, 0], 'o', color='r', ms=5)
    plt.show()

    return 

def get_test_param(dataset, case):
    if dataset == "Mikolajczyk":
        param = range(1, 6)
    elif dataset == "Synthesized":
        if case == "rot": 
            param = range(-30, 35, 5)
        elif case == "scl":
            param = [0.6, 0.8, 1.0, 1.1, 1.2]
    elif dataset == "Webcam":
        param = ['Chamonix', 'Courbevoie', 'Frankfurt', 'Mexico', 'Panorama', 'StLouis']
    return param 

def history_file_load(base_dir):
    # handle kp_history
    kp_target_dir = os.path.join(base_dir, "feat_storage/keypoint/")
    if not os.path.isfile(kp_target_dir+"history.pkl"):
        kp_history = set()
    else:
        with open(kp_target_dir+"history.pkl", "r") as f:
            kp_history = cPickle.load(f)

    # handle desc_history
    desc_target_dir = os.path.join(base_dir, "feat_storage/descriptor/")
    if not os.path.isfile(desc_target_dir+"history.pkl"):
        desc_history = set()
    else:
        with open(desc_target_dir+"history.pkl", "r") as f:
            desc_history = cPickle.load(f)
    return kp_target_dir, kp_history, desc_target_dir, desc_history

def history_file_save(base_dir, kp_history, desc_history):
    with open(os.path.join(base_dir, "feat_storage/keypoint/")+"history.pkl", "w") as f:
        cPickle.dump(kp_history, f)
    with open(os.path.join(base_dir, "feat_storage/descriptor/")+"history.pkl", "w") as f:
        cPickle.dump(desc_history, f)
    return 

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height, '%0.1f' % height, ha='center', va='bottom', fontsize=7)

if __name__ == "__main__":
    """
        desc: { case: ["SIFT", "CNN"], 
                net: [None], 
                prototxt: ['prototxt'], caffemodel: ['caffemodel']
                preproc: ["roi_pooling", "roi_trans", "keypoint_proposal"] 
                layer: ['fc1', 'score'], 
                name: ['SIFT', 'CNN'] }
    """

    args = parse_arg()

    if args.action == "show_feature_map":
        show_feature_map("roit_train/KD_CNN_test_roit_pyr.prototxt", "roit_train/full_models_pyr/kd_cnn_iter_17000.caffemodel")
        #show_feature_map("roit_train/KD_CNN_test_roit_ori.prototxt", "roit_train/full_models_ori/kd_cnn_iter_9000.caffemodel")
        #show_feature_map("roit_train/KD_CNN_test_roit_pyr.prototxt", "roit_train/desc_feat_models_pyr/kd_cnn_iter_13000.caffemodel")
        #show_feature_map("roit_train/KD_CNN_test_roit_pyr.prototxt", "roit_train/desc_models_pyr/kd_cnn_iter_16000.caffemodel")
    elif args.action == "show_train_data":
        show_train_data("roit_train", "KD_CNN_train_roit_pyr.prototxt", "full_models_pyr/kd_cnn_iter_17000.caffemodel")
    elif args.action == "evaluation":
        if args.dataset == "Synthesized":
            with open(args.test_image_list, 'r') as f:
                test_image = [x.strip() for x in f.readlines()[:args.test_image_num]]
        elif args.dataset == "Mikolajczyk" or args.dataset == "Webcam":
            test_image = []

        # model_select 
        d_pyr_full_select_32 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_pyr_full_%d" % x), "prototxt": "roit_train/KD_CNN_test_roit_pyr.prototxt", "caffemodel": ("roit_train/full_models_pyr/kd_cnn_iter_%d.caffemodel" % x) } for x in range(1000, 18000, 4000)]

        d_ori_full_select_32 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_ori_full_%d" % x), "prototxt": "roit_train/KD_CNN_test_roit_ori.prototxt", "caffemodel": ("roit_train/full_models_ori/kd_cnn_iter_%d.caffemodel" % x) } for x in range(1000, 9001, 2000)]


        d_pyr_desc_select_32 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_pyr_desc_%d" % x), "prototxt": "roit_train/KD_CNN_test_roit_pyr.prototxt", "caffemodel": ("roit_train/desc_models_pyr/kd_cnn_iter_%d.caffemodel" % x) } for x in range(1000, 16001, 3000)]

        d_pyr_desc_feat_select_32 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_pyr_desc_feat_%d" % (x+16200)), "prototxt": "roit_train/KD_CNN_test_roit_pyr.prototxt", "caffemodel": ("roit_train/desc_feat_models_pyr/kd_cnn_iter_%d.caffemodel" % x) } for x in range(1000, 13001, 3000)]

        d_ori_desc_select_32 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_ori_desc_%d" % x), "prototxt": "roit_train/KD_CNN_test_roit_ori.prototxt", "caffemodel": ("roit_train/desc_models_ori/kd_cnn_iter_%d.caffemodel" % x) } for x in range(1000, 16000, 6000)]

        d_ori_desc_feat_select_32 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_ori_desc_feat_%d" % (x+16000)), "prototxt": "roit_train/KD_CNN_test_roit_ori.prototxt", "caffemodel": ("roit_train/desc_feat_models_ori/kd_cnn_iter_%d.caffemodel" % x) } for x in range(1000, 9000, 3000)]

        d_conv_multi_select_64 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_conv_multi_64_%d" % x), "prototxt": "roit_roip_train/KD_CNN_test_roit_ori_conv_multiscale.prototxt", "caffemodel": ("roit_roip_train/desc_models_conv_multi_64/kd_cnn_iter_%d.caffemodel" % x), "caffe_root": "/home/iis/Downloads/py-faster-rcnn/caffe-fast-rcnn/" } for x in range(1000, 12001, 3000)+[12000]]


        d_conv_scale_map_select_64 = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": ("CNN_conv_scale_map_64_%d" % x), "prototxt": "roit_roip_train/KD_CNN_test_roit_ori_conv_scale_map.prototxt", "caffemodel": ("roit_roip_train/desc_models_conv_scale_map_64_new/kd_cnn_iter_%d.caffemodel" % x), "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" } for x in range(1000, 40001, 9000)]


        # comparison_test
        d_sift = { "case":"SIFT", "name":"SIFT" }

        d_pyr_full_32 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_pyr_full_32", "prototxt": "roit_train/KD_CNN_test_roit_pyr.prototxt", "caffemodel": "roit_train/full_models_pyr/kd_cnn_iter_17000.caffemodel" }

        d_pyr_desc_feat_32 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_pyr_desc_feat_32", "prototxt": "roit_train/KD_CNN_test_roit_pyr.prototxt", "caffemodel": "roit_train/desc_feat_models_pyr/kd_cnn_iter_13000.caffemodel" }
        d_ori_full_32 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_ori_full_32", "prototxt": "roit_train/KD_CNN_test_roit_ori.prototxt", "caffemodel": "roit_train/full_models_ori/kd_cnn_iter_9000.caffemodel" }

        d_ori_desc_32 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_ori_desc_32", "prototxt": "roit_train/KD_CNN_test_roit_ori.prototxt", "caffemodel": "roit_train/desc_models_ori/kd_cnn_iter_18000.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" } 



        d_pyr_desc_32 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_pyr_desc_32", "prototxt": "roit_train/KD_CNN_test_roit_pyr.prototxt", "caffemodel": "roit_train/desc_models_pyr/kd_cnn_iter_16000.caffemodel" } 



        d_rois_trans_approx_32 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_roi_trans_approx_32", "prototxt": "KD_CNN_test_roit_input_manual.prototxt", "caffemodel": "patch_size_32_models/models_8000labels/kd_cnn_iter_7000_full_inner.caffemodel", "caffe_root": "/home/iis/Downloads/old-py-faster-rcnn/caffe-fast-rcnn/" }

        d_rois_trans_approx_32_new = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_roi_trans_approx_32_new", "prototxt": "KD_CNN_test_roit_input_manual_scale.prototxt", "caffemodel": "KD_CNN_test_roit_input_manual_scale.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_rois_trans_sift_32 = { "case": "CNN", "net": None, "preproc": "roi_trans", "layer": "fc1", "name": "CNN_roi_trans_sift_32", "prototxt": "KD_CNN_test_roit_old.prototxt", "caffemodel": "patch_size_32_models/models_8000labels/kd_cnn_iter_8800.caffemodel", "caffe_root": "/home/iis/Downloads/old-py-faster-rcnn/caffe-fast-rcnn/" }



        d_ori_desc_64 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_ori_desc_64", "prototxt": "roit_roip_train/KD_CNN_test_roit_ori.prototxt", "caffemodel": "roit_roip_train/desc_models_ori_64/kd_cnn_iter_81000.caffemodel", "caffe_root": "/home/iis/Downloads/py-faster-rcnn/caffe-fast-rcnn/" }

        d_pyr_desc_64 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_pyr_desc_64", "prototxt": "roit_roip_train/KD_CNN_test_roit_pyr.prototxt", "caffemodel": "roit_roip_train/desc_models_pyr_64/kd_cnn_iter_39000.caffemodel", "caffe_root": "/home/iis/Downloads/py-faster-rcnn/caffe-fast-rcnn/" }

        d_conv_multi_64 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_conv_multi_64", "prototxt": "roit_roip_train/KD_CNN_test_roit_ori_conv_multiscale.prototxt", "caffemodel": "roit_roip_train/desc_models_conv_multi_64/kd_cnn_iter_10000.caffemodel", "caffe_root": "/home/iis/Downloads/py-faster-rcnn/caffe-fast-rcnn/" }

        d_conv_scale_map_64 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_conv_scale_map_64", "prototxt": "roit_roip_train/KD_CNN_test_roit_ori_conv_scale_map.prototxt", "caffemodel": "roit_roip_train/desc_models_conv_scale_map_64_new/kd_cnn_iter_40000.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_conv_scale_map_scnonlin_64 = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_conv_scale_map_scnonlin_64", "prototxt": "roit_roip_train/KD_CNN_test_roit_ori_conv_scale_map_nonlin.prototxt", "caffemodel": "roit_roip_train/models/kd_cnn_iter_19000.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }

        #d_full_train_sc = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "score", "name": "CNN_full_train_sc", "prototxt": "roit_train/KD_CNN_test_roit.prototxt", "caffemodel": "roit_train/all_feat_update_models/kd_cnn_iter_18400.caffemodel" }

        #d_test_roip_roit = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "score", "name": "CNN_test", "prototxt": "roit_roip_train/KD_CNN_test_roit_ori.prototxt", "caffemodel": "roit_train/desc_models_ori/kd_cnn_iter_18000.caffemodel" }


        ###### siamese result start from here #####

        d_siamse_desc_models_first = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_desc_models_first", "prototxt": "../Siamese/history_models_1/desc_models_first/KD_CNN_test.prototxt", "caffemodel": "../Siamese/history_models_1/desc_models_first/all_models/kd_cnn_iter_epoch14.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_siamse_desc_models_second = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_desc_models_second", "prototxt": "../Siamese/history_models_1/desc_models_second/KD_CNN_test.prototxt", "caffemodel": "../Siamese/history_models_1/desc_models_second/all_models/kd_cnn_iter_epoch9.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_siamse_scl_no_gt_model_first = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_scl_no_gt_model_first", "prototxt": "../Siamese/history_models_1/scl_no_gt_model_first/KD_CNN_test_scl_nonlin_no_gt.prototxt", "caffemodel": "../Siamese/history_models_1/scl_no_gt_model_first/all_models/kd_cnn_iter_epoch3.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_siamse_scl_no_gt_model_second = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_scl_no_gt_model_second", "prototxt": "../Siamese/history_models_1/scl_no_gt_model_second/KD_CNN_test_scl_nonlin_no_gt.prototxt", "caffemodel": "../Siamese/history_models_1/scl_no_gt_model_second/all_models/kd_cnn_iter_epoch6.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }


        d_siamse_desc_bn_models = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_desc_bn_models", "prototxt": "../Siamese/history_models_2/tested_models/desc_bn_models/KD_CNN_test_bn.prototxt", "caffemodel": "../Siamese/history_models_2/tested_models/desc_bn_models/all_models/kd_cnn_iter_epoch9.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" } ## can hit

        d_siamse_desc_complicated_12_models = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_desc_complicated_12_models", "prototxt": "../Siamese/history_models_2/desc_complicated_12_models/KD_CNN_test.prototxt", "caffemodel": "../Siamese/history_models_2/desc_complicated_12_models/all_models/kd_cnn_iter_epoch8.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" } 

        d_siamse_scl_gt_bn_models = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_scl_gt_bn_models", "prototxt": "../Siamese/history_models_2/tested_models/scl_gt_bn_models/KD_CNN_test_bn_scl_nonlin_gt.prototxt", "caffemodel": "../Siamese/history_models_2/tested_models/scl_gt_bn_models/all_models/kd_cnn_iter_epoch4.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" } 

        d_siamse_desc_bn_rhnm_models = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_desc_bn_rhnm_models", "prototxt": "../Siamese/history_models_1/desc_bn_models_rhnm/KD_CNN_test_bn_rhnm.prototxt", "caffemodel": "../Siamese/history_models_1/desc_bn_models_rhnm/all_models/kd_cnn_iter_epoch14.caffemodel", "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" }

        ##########

        d_siamse_desc_bn_models_select = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_desc_bn_%d" % x, "prototxt": "../Siamese/history_models_2/desc_bn_models/KD_CNN_test_bn.prototxt", "caffemodel": "../Siamese/history_models_2/desc_bn_models/all_models/kd_cnn_iter_epoch%d.caffemodel" % x, "caffe_root": "/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/" } for x in [0, 3, 6, 9]] ## can hit


        ########

        d_siamese_desc_bn_naive_models = { "case": "CNN", "net": None, "preproc": "roi_trans", "layer": "fc1", "name": "CNN_Siamese_desc_bn_naive", "prototxt": "naive_60_desc_models/KD_CNN_test_bn_naive.prototxt", "caffemodel": "naive_60_desc_models/all_models/kd_cnn_iter_epoch6.caffemodel", "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_siamese_desc_bn_naive_more_models = { "case": "CNN", "net": None, "preproc": "roi_trans", "layer": "fc1", "name": "CNN_Siamese_desc_bn_more_naive", "prototxt": "naive_60_desc_models_more/KD_CNN_test_bn_naive.prototxt", "caffemodel": "naive_60_desc_models_more/all_models/kd_cnn_iter_epoch15.caffemodel", "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_siamese_desc_60_color_models = { "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_desc_60_color", "prototxt": "desc_60_color/KD_CNN_test_bn.prototxt", "caffemodel": "desc_60_color/all_models/kd_cnn_iter_90000.caffemodel", "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_siamese_desc_60_color_sift_scale_models = { "case": "CNN", "net": None, "preproc": "keypoint_proposal_scale", "layer": "fc1", "name": "CNN_Siamese_desc_60_sift_scale_color", "prototxt": "desc_60_color/KD_CNN_test_bn_sift_scale.prototxt", "caffemodel": "desc_60_color/all_models/kd_cnn_iter_90000.caffemodel", "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/" }

        d_siamese_desc_bn_M_sift_scale_models = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal_scale", "layer": "fc1", "name": "CNN_Siamese_M_sift_scale_%d" % x, "prototxt": "desc_bn_Mikolajczyk_models/KD_CNN_test_bn_sift_scl.prototxt", "caffemodel": "desc_bn_Mikolajczyk_models/all_models/kd_cnn_iter_epochM%d.caffemodel" % x, "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/", "metric": "L2" } for x in range(9)]

        d_siamese_desc_bn_M_models = [{ "case": "CNN", "net": None, "preproc": "keypoint_proposal", "layer": "fc1", "name": "CNN_Siamese_M_%d" % x, "prototxt": "desc_bn_Mikolajczyk_models/KD_CNN_test_bn.prototxt", "caffemodel": "desc_bn_Mikolajczyk_models/all_models/kd_cnn_iter_epochM%d.caffemodel" % x, "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/", "metric": "L2" } for x in range(9)]


        ########


        d_siamese_desc_kprs_tr_models = { "case":"CNN", "net":None, "preproc":"roi_trans", "layer":"fc1", "name":"CNN_Siamese_kprs_tr", "prototxt":"desc_kprs_tr_models/KD_CNN_test_bn_kprs_hnm.prototxt", "caffemodel":"desc_kprs_tr_models/all_models/kd_cnn_iter_epoch19.caffemodel", "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/", "metric": "L2"  }

        d_siamese_desc_kprs_01_models = { "case":"CNN", "net":None, "preproc":"roi_trans", "layer":"fc1", "name":"CNN_Siamese_kprs_01", "prototxt":"desc_kprs_01_models/KD_CNN_test_bn_kprs_hnm.prototxt", "caffemodel":"desc_kprs_01_models/all_models/kd_cnn_iter_epoch19.caffemodel", "caffe_root": "../new-py-faster-rcnn/caffe-fast-rcnn/", "metric": "L2"  }




        #desc = [d_sift, d_rois_trans_sift, d_rois_trans_approx] # rois_trans
        #desc = [d_sift, d_pyr_full, d_pyr_desc_feat] # pyr
        #desc = [d_sift, d_ori_full, d_ori_desc] # ori
        #desc = [d_sift, d_pyr_desc, d_ori_desc] # pyr_ori

        #desc = [d for d in d_pyr_full_select] # pyr_full_select 
        #desc = [d for d in d_ori_full_select] # ori_full_select 
        #desc = [d for d in (d_pyr_desc_select + d_pyr_desc_feat_select)] # pyr_desc_feat_select
        #desc = [d for d in (d_ori_desc_select + d_ori_desc_feat_select)] # ori_desc_feat_select
        #desc = [d for d in d_conv_multi_select_64] # conv_multi_select_64
        #desc = [x for x in d_conv_scale_map_select_64] # conv_scale_map_select_64

        #desc = [d_test_roip_roit]

        #desc = [d_sift, d_ori_desc_64, d_ori_desc_32]
        #desc = [d_sift, d_ori_desc_64, d_pyr_desc_64]

        #desc = [d_sift, d_ori_desc_64, d_conv_multi_64]
        ##desc = [d_sift, d_pyr_desc_64, d_ori_desc_64, d_conv_multi_64]
        #desc = [d_sift, d_ori_desc_64, d_pyr_desc_64, d_conv_multi_64, d_conv_scale_map_64]
        #desc = [d_sift, d_ori_desc_64, d_pyr_desc_64, d_conv_multi_64, d_conv_scale_map_64]
        #desc  = [d_conv_scale_map_scnonlin_64]
        
        #desc  = [d_sift, d_siamse_desc_bn_models, d_siamse_scl_gt_bn_models]

        #desc = [d_sift] + d_siamse_desc_bn_models_select

        #desc = [d_sift, d_siamse_desc_bn_models, d_siamse_scl_gt_bn_models, d_siamse_desc_bn_rhnm_models]

        desc = [d_sift, d_siamese_desc_kprs_tr_models, d_siamese_desc_kprs_01_models]


        queue = Queue()
        param = get_test_param(args.dataset, args.case)
        all_total = np.zeros((0, len(param)), dtype=np.float32) # all_total: 0: num_desc, 1: num_param
        for i in range(len(desc)):
            p = Process(target=run_test, args=(args, test_image, desc[i], queue))
            p.start()
            cur_total = queue.get()
            print cur_total.shape
            all_total = np.vstack([all_total, cur_total])
            p.join()

        colors = cycle("bgrcmykw")
        fig = plt.figure()

        if args.metric[:3] == "mAP":
            plt.ylim([0.0, 1.0])
            plt.ylabel('average precision')
        elif args.metric == "match_score":
            plt.ylim([0.0, 100.0])
            plt.ylabel('matching score')

        if args.dataset == "Webcam":
            ax = fig.add_subplot(111)
            ind = np.arange(len(param)+1)
            width = 0.2
            plt.xlim([-width, len(ind)+width])
            desc_result = []
            for i, d in enumerate(desc):
                desc_result.append(ax.bar(ind+i*width, np.hstack([all_total[i, :], np.mean([all_total[i, :]], axis=1)]), width, color=colors.next()))
                autolabel(desc_result[-1])
            ax.set_xticks(ind+width)
            xtickNames = ax.set_xticklabels(param+['Average'])
            plt.setp(xtickNames, rotation=45, fontsize=10)
            ax.legend((x[0] for x in desc_result), (d['name'] for d in desc))
        else:
            for i, d in enumerate(desc):
                plt.plot(param, all_total[i, :].tolist(), marker='o', linestyle='-', color=colors.next(), label=d['name'])
            plt.xlim([param[0], param[-1]])
            plt.xlabel('%s (%s)' % (args.dataset, args.case))
            plt.legend(bbox_to_anchor=(1, -0.05), prop={'size': 8}, loc='upper center', ncol=1)
            plt.tight_layout(pad=5)

        if args.output_result == "":
            plt.show()
        else:
            plt.savefig(args.output_result)

    #args = parse_arg()
    #if args.dataset == "Mikolajczyk":
    #    for target_dir in ['bark', 'bikes', 'boat', 'graf', 'leuven', 'trees', 'ubc', 'wall']:
    #        if target_dir == "boat":
    #            Mikolajczyk_dataset_test(target_dir, suffix="pgm")
    #        else:
    #            Mikolajczyk_dataset_test(target_dir)
    #elif args.dataset == "Synthesized":
    #    with open('patch_size_32/models_8000labels/train_image_list.txt', 'r') as f:
    #        train_image_list = [x.strip() for x in f.readlines()]
    #    all_image_list = os.listdir("../Flicker8k_Dataset")
    #    test_image_list = list(set(all_image_list) - set(train_image_list))
    #    sampled_image = [test_image_list[x] for x in np.random.choice(range(len(test_image_list)), 5)]

   






