#!/bin/bash

#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --case rot \
#				 --metric mAP_dist_thres \
#				 --test_image_list validation_image_list.txt \
#				 --test_image_num 10 \
#				 --output_result conv_multi_64_rot_val_result.jpg

##### three total #####
#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --image_dir ../data/Flicker8k_Dataset/ \
#				 --case rot \
#				 --metric mAP_dist_thres \
#				 --test_image_list Flicker_test_image_list.txt \
#				 --test_image_num 100 \
#				 --output_result mAP_dist_all_rot_result_Flicker.jpg
#
#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --image_dir ../data/Flicker8k_Dataset/ \
#				 --case scl \
#				 --metric mAP_dist_thres \
#				 --test_image_list Flicker_test_image_list.txt \
#				 --test_image_num 100 \
#				 --output_result mAP_dist_all_scl_result_Flicker.jpg

python ./test.py --action evaluation \
				 --dataset Synthesized \
				 --image_dir ../data/MS_COCO/ \
				 --case rot \
				 --metric mAP_dist_thres \
				 --test_image_list MS_COCO_image_list.txt \
				 --test_image_num 100 \
				 --output_result mAP_dist_all_rot_result_COCO.jpg

python ./test.py --action evaluation \
				 --dataset Synthesized \
				 --image_dir ../data/MS_COCO/ \
				 --case scl \
				 --metric mAP_dist_thres \
				 --test_image_list MS_COCO_image_list.txt \
				 --test_image_num 100 \
				 --output_result mAP_dist_all_scl_result_COCO.jpg

######
#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --case rot \
#				 --metric mAP_nn \
#				 --test_image_list test_image_list.txt \
#				 --test_image_num 20 \
#				 --output_result mAP_nn_all_rot_result.jpg
#
#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --case scl \
#				 --metric mAP_nn \
#				 --test_image_list test_image_list.txt \
#				 --test_image_num 20 \
#				 --output_result mAP_nn_all_scl_result.jpg
######
#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --case rot \
#				 --metric match_score \
#				 --test_image_list test_image_list.txt \
#				 --test_image_num 20 \
#				 --output_result match_score_all_rot_result.jpg
#
#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --case scl \
#				 --metric match_score \
#				 --test_image_list test_image_list.txt \
#				 --test_image_num 20 \
#				 --output_result match_score_all_scl_result.jpg
######################

#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --case rot \
#				 --metric match_score \
#				 --test_image_list test_image_list.txt \
#				 --test_image_num 200 \
#				 --output_result match_score_ori_desc_64_comp_rot_result.jpg

##########

#python ./test.py --action evaluation \
#				 --dataset Synthesized \
#				 --image_dir ../data/Flicker8k_Dataset/ \
#				 --case rot \
#				 --metric mAP_dist_thres \
#				 --test_image_list Flicker_test_image_list.txt \
#				 --test_image_num 1 \
#				 --output_result temp.jpg




