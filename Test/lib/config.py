import scipy.io
import numpy as np 
from easydict import EasyDict as edict

__C = edict()

cfg = __C

__C.TRAIN = edict()

# root of the base directory
__C.kd_cnn_root = '/home/iis/Cheng-Hao/Bitbucket/kd_cnn/'

# pixel mean value (for grayscale)
__C.MEAN_VAL_GRAY = np.array([[113.43834774]], dtype=np.float32)

# pixel mean value (for gbr)
__C.MEAN_VAL_GBR = np.array([[[ 102.99888611,  113.75482178,  116.79717255]]], dtype=np.float32)

# target size for the img_min_size
__C.TRAIN.TARGET_SIZE = 800

# maximum size for the image sizes
__C.TRAIN.MAX_SIZE = 600



