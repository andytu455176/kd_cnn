import sys, cv2, cPickle, h5py, os
import numpy as np 
import matplotlib.pyplot as plt 
from lib.config import cfg 
from multiprocessing import Process 

def detector(image_name, output_file="", n_kp=500):
    sift = cv2.SIFT(nfeatures=n_kp)
    image = cv2.imread(image_name, cv2.CV_LOAD_IMAGE_GRAYSCALE)
    kp = sift.detect(image, None)
    feat = np.zeros((len(kp), 5), dtype=np.float32)
    feat[:, 0:2] = np.array([x.pt for x in kp], dtype=np.float32)
    feat[:, 2:5] = np.array([[1.0/(0.5*x.size)**2, 0.0, 1.0/(0.5*x.size)**2] for x in kp], dtype=np.float32)
    if output_file != "":
        with open(output_file, 'w') as f:
            f.write("%d\n%d\n" % (0, len(kp)))
            for i in range(len(kp)):
                f.write(" ".join([str(x) for x in feat[i, ...]]) + "\n")
        with open(output_file+"_keypoint", 'w') as f:
            f.write("%d\n%d\n" % (0, len(kp)))
            for x in kp:
                f.write("%f %f %f %f %f %f\n" % (x.pt[0], x.pt[1], x.size, x.angle, x.response, x.octave))
    return feat, kp

def descriptor(image_name, feat, d, output_file):
    if d['case'] == "SIFT":
        sift = cv2.SIFT(nfeatures=500)
        image = cv2.imread(image_name, cv2.CV_LOAD_IMAGE_GRAYSCALE)
        _, desc = sift.compute(image, d['kp'])
    elif d['case'] == "CNN":
        desc = cnn_descriptor(image_name, d)

    if output_file != "":
        with open(output_file, 'w') as f:
            f.write("%d\n%d\n" % (desc.shape[1], desc.shape[0]))
            for i in range(desc.shape[0]):
                f.write(" ".join([str(x) for x in feat[i, ...]]) + " ")
                f.write(" ".join([str(x) for x in desc[i, ...]]) + "\n")
    return desc 


def cnn_descriptor(image_name, d, batch_size=300):
    caffe_root = cfg.kd_cnn_root + 'new-py-faster-rcnn/caffe-fast-rcnn/'
    sys.path.insert(0, caffe_root + 'python')
    import caffe 
    caffe.set_mode_gpu()

    net = caffe.Net(d['prototxt'], d['caffemodel'], caffe.TEST)
    num_kp = len(d['kp'])
    descdim = net.blobs[d['layer']].data[...].shape[1]
    mu = cfg.MEAN_VAL_GBR
    desc = np.zeros((num_kp, descdim), dtype=np.float32)
    image = cv2.imread(image_name)
    image = np.transpose(image-mu, (2, 0, 1))[np.newaxis, ...]

    kp = np.array([x.pt for x in d['kp']], dtype=np.float32)[..., ::-1]
    angle = np.array([np.deg2rad(x.angle) for x in d['kp']], dtype=np.float32)
    scale = np.array([x.size*0.6/2 for x in d['kp']], dtype=np.float32)
    filler = { "kp": kp, "angle": angle, "scale": scale }

    cur_index = 0
    while cur_index < num_kp:
        cur_batch = np.min([batch_size, num_kp-cur_index])
        net.blobs['data'].reshape(*(image.shape))
        net.blobs['data'].data[...] = image
        network_preprocessing(net, filler, d['preproc'], cur_index, cur_index+cur_batch)
        net.forward()
        desc[cur_index:cur_index+cur_batch, ...] = net.blobs[d['layer']].data[...].copy()
        cur_index += cur_batch
    return desc

def network_preprocessing(net, filler, mode, ids, ide):
    if mode == "roi_trans":
        rois = np.zeros((ide-ids, 5), dtype=np.float32)
        rois[:, 0] = 0.0
        rois[:, 1:3] = np.array([filler['kp'][ids:ide, 0], filler['kp'][ids:ide, 1]], dtype=np.float32).T
        rois[:, 3] = filler['angle'][ids:ide]
        rois[:, 4] = filler['scale'][ids:ide]
        net.blobs['rois'].reshape(*(rois.shape))
        net.blobs['rois'].data[...] = rois
    elif mode == "keypoint_proposal":
        kp_proposal = np.zeros((ide-ids, 3), dtype=np.float32) 
        kp_proposal[:, 0] = 0.0
        kp_proposal[:, 1:3] = np.array([filler['kp'][ids:ide, 0], filler['kp'][ids:ide, 1]], dtype=np.float32).T
        net.blobs['keypoint_proposal'].reshape(*(kp_proposal.shape))
        net.blobs['keypoint_proposal'].data[...] = kp_proposal
    elif mode == "keypoint_proposal_scale":
        kp_proposal = np.zeros((ide-ids, 3), dtype=np.float32)
        kp_proposal[:, 0] = 0.0
        kp_proposal[:, 1:3] = np.array([filler['kp'][ids:ide, 0], filler['kp'][ids:ide, 1]], dtype=np.float32).T
        net.blobs['keypoint_proposal'].reshape(*(kp_proposal.shape))
        net.blobs['keypoint_proposal'].data[...] = kp_proposal
        net.blobs['scale'].reshape(ide-ids, 1, 1, 1)
        net.blobs['scale'].data[:, 0, 0, 0] = filler['scale'][ids:ide]
    return 

def get_desc_dict(kp):

    d_sift = [{ "case": "SIFT", "name": "SIFT", "kp": kp, "short_name": "sift" }]

    d_siamese_desc_kprs_01_models = [{ "case":"CNN", "preproc":"roi_trans", "layer":"fc1", "name":"CNN_Siamese_kprs_01", "prototxt":"desc_kprs_01_models/KD_CNN_test_bn_kprs_hnm.prototxt", "caffemodel":"desc_kprs_01_models/all_models/kd_cnn_iter_epoch19.caffemodel", "kp":kp, "short_name":"kprs01" }]

    d_siamese_desc_kprs_tr_models_select = [{ "case":"CNN", "preproc":"roi_trans", "layer":"fc1", "name":"CNN_Siamese_kprs_tr_%d" % x, "prototxt":"desc_kprs_tr_models/KD_CNN_test_bn_kprs_hnm.prototxt", "caffemodel":"desc_kprs_tr_models/all_models/kd_cnn_iter_epoch%d.caffemodel" % x, "kp":kp, "short_name":"kprstr_%d" % x } for x in [0, 4, 9, 14, 19]]

    d_siamese_desc_kprs_01_models_select = [{ "case":"CNN", "preproc":"roi_trans", "layer":"fc1", "name":"CNN_Siamese_kprs_01_%d" % x, "prototxt":"desc_kprs_01_models/KD_CNN_test_bn_kprs_hnm.prototxt", "caffemodel":"desc_kprs_01_models/all_models/kd_cnn_iter_epoch%d.caffemodel" % x, "kp":kp, "short_name":"kprs01_%d" % x } for x in [0, 4, 9, 14, 19]]

    #d_siamese_desc_hnm_all_models_select = [{ "case":"CNN", "preproc":"keypoint_proposal", "layer":"fc1", "name":"CNN_Siamese_hnm_all_%d" % x, "prototxt":"desc_hnm_all_models/KD_CNN_test_bn_hnm.prototxt", "caffemodel":"desc_hnm_all_models/all_models/kd_cnn_iter_epoch%d.caffemodel" % x, "kp":kp, "short_name":"hnmall_%d" % x } for x in [0, 4, 8]]

    return d_sift

if __name__ == "__main__":
    """ 
        desc: { case: ["SIFT", "CNN"], 
                net: [None], 
                prototxt: ['prototxt'], caffemodel: ['caffemodel']
                preproc: ["roi_pooling", "roi_trans", "keypoint_proposal"] 
                layer: ['fc1', 'score'], 
                name: ['SIFT', 'CNN'], 
                kp: [...], 
                short_name: ['sift', ...] }
    """

    Keypoint_dir = cfg.kd_cnn_root + "Test/Keypoint/"
    Descriptor_dir = cfg.kd_cnn_root + "Test/Descriptor/"

    base_dir = "../data"
    dataset = "Mikolajczyk"

    for scene in ["bark", "bikes", "boat", "graf", "ubc", "leuven", "wall", "trees"]:
        for i in range(1, 7):
            print i
            img = "img%d" % i
            if scene == "boat":
                image_name = os.path.join(base_dir, dataset+"_dataset", scene, img+".pgm")
            else:
                image_name = os.path.join(base_dir, dataset+"_dataset", scene, img+".ppm")
            store_name = "_".join([dataset, scene, img])

            feat, kp = detector(image_name, output_file=Keypoint_dir+store_name+".sift")

            d_desc = get_desc_dict(kp)

            for d in d_desc:
                p = Process(target=descriptor, args=(image_name, feat, d, Descriptor_dir+store_name+".sift."+d['short_name']))
                p.start()
                p.join()
                #desc = descriptor(image_name, feat, d, Descriptor_dir+store_name+".sift."+d['short_name'])
















