name: "KD_CNN"

input: "data"
input_shape: { dim: 1 dim: 1 dim: 100 dim: 100 }

layer {
	name: "Preprocess"
	type: "Python"
	bottom: "data"
	top: "p_data"
	python_param {
		module: "lib.data_layer"
		layer: "DataLayer"
		param_str: "'mode': 'gray' \n'input_from_list': 'false'"
	}
}

layer {
	name: "Pre_blur"
	type: "Convolution"
	bottom: "p_data"
	top: "pb_data"
	convolution_param {
		num_output: 1
		kernel_size: 5 pad: 2 stride: 1
		bias_term: false
	}
}

layer {
	name: "Downsample"
	type: "Pooling"
	bottom: "pb_data"
	top: "ds_data"
	pooling_param {
		pool: AVE
		kernel_size: 2
		stride: 2
	}	
}

########## Octave 1 start ##########

layer {
	name: "O1_DOG"
	type: "Convolution"
	bottom: "pb_data"
	top: "o1_dog"
	convolution_param {
		num_output: 4
		#kernel_size: 39 pad: 19 stride: 1
		kernel_size: 11 pad: 5 stride: 1
		#kernel_size: 7 pad: 3 stride: 1
	}
}

layer {
	name: "O1_diff"
	type: "Convolution"
	bottom: "o1_dog"
	top: "o1_diff"
	convolution_param {
		num_output: 52
		kernel_size: 3 pad: 1 stride: 1
		bias_term: false
	}
}

layer {
	name: "O1_diff_gate"
	type: "Sigmoid"
	bottom: "o1_diff"
	top: "o1_diff"
}

#layer {
#	name: "O1_diff_gate"
#	type: "Threshold"
#	bottom: "o1_diff"
#	top: "o1_diff"
#	threshold_param {
#		threshold: 0.0
#	}
#}

layer {
	name: "O1_sum"
	type: "Convolution"
	bottom: "o1_diff"
	top: "o1_local_opt"
	convolution_param {
		num_output: 2
		kernel_size: 3 pad: 1 stride: 1
		bias_filler { type: 'constant' value: 0.0 }
	}
}

layer {
	name: "O1_scale"
	type: "Power"
	bottom: "o1_local_opt"
	top: "o1_local_opt"
	power_param {
		power: 2.0
		scale: 0.07692
		shift: -1.0
	}
}

#layer {
#	name: "O1_scale_gate"
#	type: "Threshold"
#	bottom: "o1_local_opt"
#	top: "o1_local_opt"
#	threshold_param { threshold: 0.95 }
#}

layer {
	name: "O1_shift"
	type: "Power"
	bottom: "o1_local_opt"
	top: "o1_local_opt"
	power_param { shift: -0.95 }
}

layer {
	name: "O1_shift_gate"
	type: "ReLU"
	bottom: "o1_local_opt"
	top: "o1_local_opt"
}

layer {
	name: "O1_keypoint"
	type: "Convolution"
	bottom: "o1_local_opt"
	top: "o1_keypoint"
	convolution_param {
		num_output: 1
		kernel_size: 3 pad: 1 
		bias_filler { type: 'constant' value: 0.0 }
	}
}

########## Octave 1 end    ##########

########## Octave 2 start  ##########
layer {
	name: "O2_DOG"
	type: "Convolution"
	bottom: "ds_data"
	top: "o2_dog"
	convolution_param {
		num_output: 4
		#kernel_size: 39 pad: 19 stride: 1
		kernel_size: 11 pad: 5 stride: 1
		#kernel_size: 7 pad: 3 stride: 1
	}
}

layer {
	name: "O2_diff"
	type: "Convolution"
	bottom: "o2_dog"
	top: "o2_diff"
	convolution_param {
		num_output: 52
		kernel_size: 3 pad: 1 
		bias_term: false
	}
}

layer {
	name: "O2_diff_gate"
	type: "Sigmoid"
	bottom: "o2_diff"
	top: "o2_diff"
}

#layer {
#	name: "O2_diff_gate"
#	type: "Threshold"
#	bottom: "o2_diff"
#	top: "o2_diff"
#	threshold_param {
#		threshold: 0.0
#	}
#}

layer {
	name: "O2_sum"
	type: "Convolution"
	bottom: "o2_diff"
	top: "o2_local_opt"
	convolution_param {
		num_output: 2
		kernel_size: 3 pad: 1 stride: 1
		bias_filler { type: 'constant' value: 0.0 }
	}
}

layer {
	name: "O2_scale"
	type: "Power"
	bottom: "o2_local_opt"
	top: "o2_local_opt"
	power_param {
		power: 2.0
		scale: 0.07692
		shift: -1.0
	}
}

#layer {
#	name: "O2_scale_gate"
#	type: "Threshold"
#	bottom: "o2_local_opt"
#	top: "o2_local_opt"
#	threshold_param { threshold: 0.95 }
#}

layer {
	name: "O2_shift"
	type: "Power"
	bottom: "o2_local_opt"
	top: "o2_local_opt"
	power_param { shift: -0.95 }
}

layer {
	name: "O2_shift_gate"
	type: "ReLU"
	bottom: "o2_local_opt"
	top: "o2_local_opt"
}

layer {
	name: "O2_keypoint"
	type: "Convolution"
	bottom: "o2_local_opt"
	top: "o2_keypoint"
	convolution_param {
		num_output: 1
		kernel_size: 3 pad: 1 
		bias_filler { type: 'constant' value: 0.0 }
	}
}

layer {
	name: "O2_keypoint_up"
	type: "Deconvolution"
	bottom: "o2_keypoint"
	top: "o2_keypoint_up"
	convolution_param {
		num_output: 1
		kernel_size: 4 pad: 1 stride: 2
		bias_term: false
	}
}

########## Octave 2 end  ##########

layer {
	name: "Keypoint"
	type: "Eltwise"
	bottom: "o1_keypoint"
	bottom: "o2_keypoint_up"
	top: "pure_keypoint"
	eltwise_param { operation: MAX coeff: 1.0 coeff: 1.0 }
}

layer {
	name: "Grad"
	type: "Convolution"
	bottom: "p_data"
	top: "grad"
	convolution_param {
		num_output: 2
		kernel_size: 3 pad: 1
		bias_term: false
	}
}
layer {
	name: "Grad_power"
	type: "Power"
	bottom: "grad"
	top: "grad"
	power_param { power: 2.0 }
}
layer {
	name: "Grad_sum"
	type: "Convolution"
	bottom: "grad"
	top: "grad_mag"
	convolution_param {
		num_output: 1
		kernel_size: 1
		bias_term: false
	}
}
layer {
	name: "Grad_sqrt"
	type: "Power"
	bottom: "grad_mag"
	top: "grad_mag"
	power_param { power: 0.5 }
}
layer {
	name: "Patch_Grad"
	type: "Convolution" 
	bottom: "grad_mag"
	top: "grad_patch"
	convolution_param {
		num_output: 1
		#kernel_size: 25 pad: 12
		kernel_size: 33 pad: 16
		bias_term: false
	}
}
layer {
	name: "Patch_Grad_shift"
	type: "Power"
	bottom: "grad_patch"
	top: "grad_patch"
	power_param { shift: -100.0 }
}
layer {
	name: "Patch_Grad_thres"
	type: "ReLU"
	bottom: "grad_patch"
	top: "grad_patch"
}

layer {
	name: "Final_Keypoint"
	type: "Eltwise"
	bottom: "pure_keypoint"
	bottom: "grad_patch"
	top: "keypoint"
	eltwise_param { operation: PROD }
}

layer {
	# keypoint prediction layer 
	name: "Keypoint_gate"
	type: "Sigmoid"
	bottom: "keypoint"
	top: "keypoint"
}

layer {
	name: "Keypoint_proposal"
	type: "Python"
	bottom: "keypoint"
	top: "keypoint_proposal"
	python_param {
		module: "lib.keypoint_proposal_layer"
		layer: "KeypointProposalLayer"
		param_str: "'nms_thres': '0.15' \n'patch_size': '64' \n'mode': 'kp_nms'"
	}
}










