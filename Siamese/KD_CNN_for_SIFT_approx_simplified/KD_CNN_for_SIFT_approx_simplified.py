import sys, cv2, time
import numpy as np
from lib.config import cfg
import matplotlib.pyplot as plt
import scipy.io

caffe_root = '/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/'
sys.path.insert(0, caffe_root + 'python')
import caffe 
caffe.set_mode_gpu()

image_dir = '../Flicker8k_Dataset/'
net = caffe.Net('KD_CNN_for_SIFT_approx_simplified.prototxt', 'KD_CNN_for_SIFT_approx_simplified.caffemodel', caffe.TEST)
#net = caffe.Net('KD_CNN_for_SIFT_approx_simplified.prototxt', caffe.TRAIN)

#image = cv2.imread(image_dir + '2473689180_e9d8fd656a.jpg', cv2.CV_LOAD_IMAGE_GRAYSCALE)
image = cv2.imread(image_dir + '997722733_0cb5439472.jpg', cv2.CV_LOAD_IMAGE_GRAYSCALE)
net.blobs['data'].reshape(1, 1, image.shape[0], image.shape[1])
net.blobs['data'].data[0, ...] = np.expand_dims(image, axis=0) - cfg.MEAN_VAL_GRAY

##### for Parameters filling 
## fill antialias filter
#antialias_filter = scipy.io.loadmat('antialias_filter.mat')
#net.params['Pre_blur'][0].data[...] = antialias_filter['filter']
#
## fill dog kernel
#octaves = 2
#dog_kernel = scipy.io.loadmat('dog_kernel.mat')
#size = net.params['O1_DOG'][0].data.shape[2]
#conv_param = np.zeros((5, 1, size, size), dtype=np.float32)
#for i in xrange(conv_param.shape[0]):
#    kernel = dog_kernel['k%d' % (i+1)]
#    if kernel.shape[0] <= size:
#        conv_param[i, 0, ...] = np.lib.pad(kernel, (size-kernel.shape[0])/2, 'constant', constant_values=0)
#    else:
#        half = kernel.shape[0]/2
#        conv_param[i, 0, ...] = kernel[(half-size/2):(half+1+size/2), (half-size/2):(half+1+size/2)]
#
#print conv_param
#
#for octave in range(octaves):
#    dog_conv = net.params['O%d_DOG' % (octave+1)][0].data
#    dog_conv[0, 0, ...] = conv_param[1, 0, ...] - conv_param[0, 0, ...]
#    dog_conv[1, 0, ...] = conv_param[2, 0, ...] - conv_param[1, 0, ...]
#    dog_conv[2, 0, ...] = conv_param[3, 0, ...] - conv_param[2, 0, ...]
#    dog_conv[3, 0, ...] = conv_param[4, 0, ...] - conv_param[3, 0, ...]
#
## fill 26 diff filters
#diff_kernel = scipy.io.loadmat("diff_kernel.mat")
#for octave in xrange(octaves):
#    for i in xrange(26):
#        net.params['O%d_diff' % (octave+1)][0].data[i, :, :, :] = np.zeros((4, 3, 3), dtype=np.float32)
#        net.params['O%d_diff' % (octave+1)][0].data[i, :3, :, :] = diff_kernel['k'][0][i] * 50.0
#        net.params['O%d_diff' % (octave+1)][0].data[i+26, :, :, :] = np.zeros((4, 3, 3), dtype=np.float32)
#        net.params['O%d_diff' % (octave+1)][0].data[i+26, 1:4, :, :] = diff_kernel['k'][0][i] * 50.0
#
## fill sum_all filter
#for octave in xrange(octaves):
#    net.params['O%d_sum' % (octave+1)][0].data[0, ...] = np.zeros((52, 3, 3), dtype=np.float32)
#    net.params['O%d_sum' % (octave+1)][0].data[0, :26, 1, 1] = np.ones((26,), dtype=np.float32)
#    net.params['O%d_sum' % (octave+1)][0].data[1, ...] = np.zeros((52, 3, 3), dtype=np.float32)
#    net.params['O%d_sum' % (octave+1)][0].data[1, 26:, 1, 1] = np.ones((26,), dtype=np.float32)
#
## fill keypoint sum filter 
#for octave in xrange(octaves):
#    net.params['O%d_keypoint' % (octave+1)][0].data[0, ...] = np.zeros((2, 3, 3), dtype=np.float32)
#    net.params['O%d_keypoint' % (octave+1)][0].data[0, :, 1, 1] = np.ones((2,), dtype=np.float32)
#
## fill o2 upsample filter 
#net.params['O2_keypoint_up'][0].data[...] = np.zeros((1, 1, 4, 4), dtype=np.float32)
#net.params['O2_keypoint_up'][0].data[0, 0, 1, 1] = 1.0
#
########### for aggressive keypoint 
## fill gradient filter
#net.params['Grad'][0].data[0, 0, ...] = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype=np.float32)
#net.params['Grad'][0].data[1, 0, ...] = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]], dtype=np.float32)
#net.params['Grad_sum'][0].data[0, ...] = np.ones((2, 1, 1), dtype=np.float32)
##net.params['Patch_Grad'][0].data[0, 0, ...] = np.ones((25, 25), dtype=np.float32) / 625.0
#net.params['Patch_Grad'][0].data[0, 0, ...] = np.ones((33, 33), dtype=np.float32) / (33.0*33.0)
#####


start = time.clock()
net.forward()
print time.clock() - start

#time.sleep(10)

keypoint = net.blobs['keypoint_proposal'].data[...]
keypoint = keypoint[keypoint[:, 0] == 0]
print keypoint

#net.save('KD_CNN_for_SIFT_approx_simplified.caffemodel')

show_image = net.blobs['p_data'].data[...].copy()
show_image = show_image[0, 0, ...]
plt.imshow(show_image, cmap='Greys_r')
plt.plot(keypoint[:, 2], keypoint[:, 1], 'o', ms=5)
plt.show()

