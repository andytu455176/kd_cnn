import sys, cv2
import numpy as np 
from lib.config import cfg
import matplotlib.pyplot as plt
import scipy.io

caffe_root = '/home/iis/Downloads/new-py-faster-rcnn/caffe-fast-rcnn/'
sys.path.insert(0, caffe_root + 'python')
import caffe 
caffe.set_mode_gpu()

net = caffe.Net("KD_CNN_initial.prototxt", caffe.TRAIN)
#net = caffe.Net("KD_CNN_test.prototxt", "KD_CNN_dog_grad_orient_scale_init.caffemodel", caffe.TEST)

print "blobs info:"
for name in net.blobs:
    print name, net.blobs[name].data.shape

print "params info:"
for name in net.params:
    if len(net.params[name]) == 2:
        print name, net.params[name][0].data.shape, net.params[name][1].data.shape
    else:
        print name, net.params[name][0].data.shape


net.params['BGR_to_gray'][0].data[0, :, 0, 0] = np.array([0.114, 0.587, 0.299], dtype=np.float32)

# fill antialias filter
antialias_filter = scipy.io.loadmat('KD_CNN_for_SIFT_approx_simplified/antialias_filter.mat')
net.params['Pre_blur'][0].data[...] = antialias_filter['filter']

# fill DOG kernel 
octaves = 2
dog_kernel = scipy.io.loadmat('KD_CNN_for_SIFT_approx_simplified/dog_kernel.mat')
size = net.params['O1_DOG'][0].data.shape[2]
conv_param = np.zeros((5, 1, size, size), dtype=np.float32)
for i in xrange(conv_param.shape[0]):
    kernel = dog_kernel['k%d' % (i+1)]
    if kernel.shape[0] <= size:
        conv_param[i, 0, ...] = np.lib.pad(kernel, (size-kernel.shape[0])/2, 'constant', constant_values=0)
    else:
        half = kernel.shape[0]/2
        conv_param[i, 0, ...] = kernel[(half-size/2):(half+1+size/2), (half-size/2):(half+1+size/2)]

for octave in range(octaves):
    dog_conv = net.params['O%d_DOG' % (octave+1)][0].data
    dog_conv[0, 0, ...] = conv_param[1, 0, ...] - conv_param[0, 0, ...]
    dog_conv[1, 0, ...] = conv_param[2, 0, ...] - conv_param[1, 0, ...]
    dog_conv[2, 0, ...] = conv_param[3, 0, ...] - conv_param[2, 0, ...]
    dog_conv[3, 0, ...] = conv_param[4, 0, ...] - conv_param[3, 0, ...]

# fill gradient filter
net.params['Grad'][0].data[0, 0, ...] = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]], dtype=np.float32)
net.params['Grad'][0].data[1, 0, ...] = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype=np.float32)
net.params['Grad'][1].data[...] = 0.0 # Grad_bias_term
net.params['Grad_sum'][0].data[0, ...] = np.ones((2, 1, 1), dtype=np.float32)

net.params['Grad_inv_stack'][0].data[...] = np.ones((2, 1, 1, 1), dtype=np.float32)
net.params['Grad_inv_stack'][1].data[...] = np.zeros((2,), dtype=np.float32)

for i, angle in enumerate(range(-175, 180, 10)):
    x = np.cos(float(angle)*np.pi / 180.0)
    y = np.sin(float(angle)*np.pi / 180.0)
    net.params['Grad_orient'][0].data[i, ...] = np.array([[[y]], [[x]]], dtype=np.float32)
    net.params['Grad_orient'][1].data[i] = 0.0

net.params['Grad_mag_stack'][0].data[...] = np.ones((36, 1, 1, 1), dtype=np.float32)
net.params['Grad_mag_stack'][1].data[...] = np.zeros((36,), dtype=np.float32)

bin_k = scipy.io.loadmat('./bin_k.mat')['bin_k']
net.params['Orient_vote'][0].data[...] = np.zeros((36, 1, 9, 9), dtype=np.float32)
for i in range(36):
    net.params['Orient_vote'][0].data[i, 0, ...] = bin_k[4:13, 4:13]
    net.params['Orient_vote'][1].data[0, ...] = np.zeros((1,), dtype=np.float32)


net.params['Scale_map'][0].data[0, :, 0, 0] = np.array([1.6*2**(float(x)/4.) for x in range(8)], dtype=np.float32) * 0.3


## fill o2 upsample filter 
##net.params['O2_keypoint_up'][0].data[...] = np.zeros((1, 1, 4, 4), dtype=np.float32)
#net.params['O2_keypoint_up'][0].data[0, 0, 1, 1] = 1.0



image_dir = '../Flicker8k_Dataset/'
image_bgr = cv2.imread(image_dir + '997722733_0cb5439472.jpg')
#image_bgr = cv2.imread(image_dir + '2471974379_a4a4d2b389.jpg')
image = np.transpose(image_bgr - cfg.MEAN_VAL_GBR, (2, 0, 1))[np.newaxis, ...]
net.blobs['data'].reshape(1, 3, image.shape[2], image.shape[3])
net.blobs['data'].data[...] = image
net.blobs['keypoint_proposal'].data[...] = np.array([[0, 78, 60]], dtype=np.float32)

net.forward()

net.save('KD_CNN_dog_grad_orient_scale_init.caffemodel')

#plt.imshow((np.transpose(net.blobs['data'].data[0, ...].copy(), (1, 2, 0)) + cfg.MEAN_VAL_GBR)[..., ::-1].astype(np.uint8))
#plt.imshow(net.blobs['p_data'].data[0, 0, ...].copy(), cmap='Greys_r')
#for i in range(4):
#    plt.subplot(1, 4, i+1)
#    plt.imshow(net.blobs['o1_dog'].data[0, i, ...].copy().astype(np.uint8), cmap='Greys_r')
#plt.imshow(net.blobs['gr_data'].data[0, 0, ...].copy().astype(np.uint8), cmap='Greys_r')
#for i in range(2):
#    plt.subplot(1, 2, i+1)
#    plt.imshow(net.blobs['grad'].data[0, i, ...].copy())
#plt.imshow(net.blobs['max_bin'].data[0, 0, ...].copy())
#for i in range(4):
#    plt.subplot(1, 4, i+1)
#    plt.imshow(net.blobs['o2_dog_up'].data[0, i, ...].copy().astype(np.uint8), cmap='Greys_r')
#plt.imshow(net.blobs['scale_map'].data[0, 0, ...].copy().astype(np.uint8), cmap='Greys_r')
#plt.show()

#print net.blobs['roi_trans_proposal'].data[...]
#target_index = 0
#plt.subplot(1, 2, 1)
#plt.imshow((np.transpose(net.blobs['data'].data[0, ...].copy(), (1, 2, 0)) + cfg.MEAN_VAL_GBR)[..., ::-1].astype(np.uint8))
#plt.plot(net.blobs['keypoint_proposal'].data[:, 2], net.blobs['keypoint_proposal'].data[:, 1], 'o', ms=5)
#plt.subplot(1, 2, 2)
#plt.imshow((np.transpose(net.blobs['patch_feat'].data[0, :3, ...].copy(), (1, 2, 0)) + cfg.MEAN_VAL_GBR)[..., ::-1].astype(np.uint8))
#plt.show()










