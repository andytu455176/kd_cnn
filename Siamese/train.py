import numpy as np 
import sys, os
import scipy.io
import argparse
import matplotlib.pyplot as plt

from lib.config import cfg 

sys.path.insert(0, cfg.kd_cnn_root + 'new-py-faster-rcnn/lib')
from utils.timer import Timer

caffe_root = cfg.kd_cnn_root + 'new-py-faster-rcnn/caffe-fast-rcnn/'
sys.path.insert(0, caffe_root + 'python')
import caffe
from caffe.proto import caffe_pb2

import google.protobuf as pb2

def parse_args():
    parser = argparse.ArgumentParser(description='Descriptor Network training arguments')
    parser.add_argument('--solver', dest='solver', help='solver prototxt file', default=None, type=str)
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    return args

def train_descriptor_net():
    return 

def solver(solver_prototxt, pre_weight=""):
    solver_param = caffe_pb2.SolverParameter()
    with open(solver_prototxt, 'rt') as f:
        pb2.text_format.Merge(f.read(), solver_param)
        print solver_param.train_net
        print solver_param.base_lr
    solver = caffe.get_solver(solver_prototxt)
    if pre_weight:
        solver.net.copy_from(pre_weight)
    return solver

#def run_solver(niter, solver):
#    loss_record = np.zeros((niter%10+1,), dtype=np.float32)
#    timer = Timer()
#    for i in range(niter):
#        sys.stdout.flush()
#        sys.stdout.write("Epoch<%d> ... progress: (img) %d/%d, (roi) %d/%d, speed: %.3fs/iter, loss: %f\r" % (solver.net.layers[0].epoch, solver.net.layers[0].cur_img, len(solver.net.layers[0].img_name_list), solver.net.layers[0].cur_roi, solver.net.layers[0].total_roi * solver.net.layers[0].NUM_TRANS, timer.average_time, solver.net.blobs['loss'].data))
#        timer.tic()
#        solver.step(1)
#        timer.toc()
#        if i % 10 == 0:
#            loss_record[i/10] = solver.net.blobs['loss'].data[...]
#            temp = {}
#            temp['loss_record'] = loss_record
#            scipy.io.savemat("models/loss_record.mat", temp)
#    return 

def run_solver(max_iter, max_epoch, solver):
    timer = Timer()
    cur_loss, cur_epoch, last_iter = 0.0, 0, 0
    #cur_scl_loss = 0.0
    loss_record = np.zeros((max_epoch, 2), dtype=np.float32)
    for i in range(max_iter):
        timer.tic()
        solver.step(1)
        timer.toc()
        cur_loss = (cur_loss * float(i - last_iter) + solver.net.blobs['loss'].data.copy()) / float(i+1 - last_iter)
        #cur_scl_loss = (cur_scl_loss * float(i - last_iter) + solver.net.blobs['scl_loss'].data.copy()) / float(i+1 - last_iter)

        sys.stdout.flush()
        #sys.stdout.write("Epoch<%d> ... progress: (img) %d/%d, (roi_trans) %d/%d, speed: %.3fs/iter, loss: %f, scl_loss: %f\r" % (solver.net.layers[0].epoch, solver.net.layers[0].cur_img, len(solver.net.layers[0].img_name_list), solver.net.layers[0].cur_trans, solver.net.layers[0].NUM_TRANS, timer.average_time, cur_loss, cur_scl_loss))
        sys.stdout.write("Epoch<%d> ... progress: (img) %d/%d, (roi_trans) %d/%d, speed: %.3fs/iter, loss: %f\r" % (solver.net.layers[0].epoch, solver.net.layers[0].cur_img, len(solver.net.layers[0].img_name_list), solver.net.layers[0].cur_trans, solver.net.layers[0].NUM_TRANS, timer.average_time, cur_loss))

        if (cur_epoch < solver.net.layers[0].epoch):
            loss_record[cur_epoch, 0] = cur_loss
            #loss_record[cur_epoch, 1] = cur_scl_loss
            temp = {}
            temp['loss_record'] = loss_record
            scipy.io.savemat("desc_bn_Mikolajczyk_models/loss_record.mat", temp)
            # save net for epoch 
            solver.snapshot()

            if cur_epoch == (max_epoch - 1):
                print "\n\n"
                print "====== training finish ======"
                #print "Epoch: %d, Scl_Loss: %f, Loss: %f, Iteration: %d, Total_images: %d, Total_rois: %d" % (cur_epoch, cur_scl_loss, cur_loss, i, len(solver.net.layers[0].img_name_list), solver.net.layers[0].NUM_TRANS)
                print "Epoch: %d, Loss: %f, Iteration: %d, Total_images: %d, Total_rois: %d" % (cur_epoch, cur_loss, i, len(solver.net.layers[0].img_name_list), solver.net.layers[0].NUM_TRANS)
                print ""
                break
            else:
                cur_epoch += 1
                cur_loss = 0.0
                #cur_scl_loss = 0.0
                last_iter = i+1

    return 

if __name__ == "__main__":
    #args = parse_args()
    caffe.set_mode_gpu()
    #s = solver("solver.prototxt", pre_weight="./KD_CNN_dog_grad_orient_scale_init.caffemodel")
    #s = solver("solver.prototxt", pre_weight="./models/all_models/kd_cnn_iter_epoch3.caffemodel")
    s = solver("solver.prototxt")
    s.restore("models/all_models/kd_cnn_iter_epoch14.solverstate")
    run_solver(150000, 5, s)
    #s = solver("solver.prototxt", pre_weight="KDCNN_K_0.caffemodel")
    #run_solver(11, s)
    







