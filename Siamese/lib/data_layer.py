from config import cfg
import sys, yaml, cv2, os
import numpy as np 

#sys.path.insert(0, '/home/iis/Downloads/py-faster-rcnn/lib')
#from fast_rcnn.nms_wrapper import nms
#
#caffe_root = '/home/iis/Downloads/py-faster-rcnn/caffe-fast-rcnn/'
#sys.path.insert(0, caffe_root + 'python')
#import caffe

caffe_root = '../new-py-faster-rcnn/caffe-fast-rcnn/'
sys.path.insert(0, caffe_root + 'python')
import caffe

class DataLayer(caffe.Layer):
    def setup(self, bottom, top):
        print "DataLayer setting up!"
        """  yaml param:
        mode (color, gray), image_list
        """
        layer_param = yaml.load(self.param_str_)
        self.input_from_list = True if layer_param['input_from_list'] == 'true' else False
        if self.input_from_list:
            with open(layer_param['image_list'], 'r') as f:
                self.image_list = [x.strip() for x in f.readlines()]
            self.image_dir = layer_param['image_dir']
        self.mode = layer_param['mode']
        if self.mode == 'color':
            top[0].reshape(1, 3, 100, 100)
        elif self.mode == 'gray':
            top[0].reshape(1, 1, 100, 100)
        self.cur = 0
        self.IMG_BATCH_SIZE = 2

    def forward(self, bottom, top):
        if self.input_from_list:
            if self.cur > len(self.image_list):
                self.cur = 0
            image_names = self.image_list[self.cur:self.cur+self.IMG_BATCH_SIZE]
            self.cur += self.IMG_BATCH_SIZE
            if self.mode == 'color':
                img_blobs = self.get_next_image_batch_color(image_names)
            elif self.mode == 'gray':
                img_blobs = self.get_next_image_batch_gray(image_names)

            top[0].reshape(*(img_blobs.shape))
            top[0].data[...] = img_blobs
        else: 
            assert bottom[0].data.shape[0] == 1, "Only single item batches are supported"
            if self.mode == 'color':
                img = np.transpose(bottom[0].data[0, ...], (1, 2, 0))
                img_shape = img.shape

                if img.shape[0]%2 != 0:
                    img = np.concatenate([img, img[-1][np.newaxis,:, :]], axis=0)
                if img.shape[1]%2 != 0:
                    img = np.concatenate([img, img[:, -1][:, np.newaxis, :]], axis=1)

                top[0].reshape(bottom[0].data.shape[0], 3, img.shape[0], img.shape[1])
                top[0].data[...] = np.transpose(img, (2, 0, 1))
            elif self.mode == 'gray':
                img = bottom[0].data[0, 0, ...]
                img_shape = img.shape
                if img_shape[0]%2 != 0:
                    img = np.vstack([img, img[-1]])
                if img_shape[1]%2 != 0:
                    img = np.hstack([img, img[:,[-1]]])

                top[0].reshape(bottom[0].data.shape[0], 1, img.shape[0], img.shape[1])
                top[0].data[...] = img

    def reshape(self, bottom, top):
        pass

    def backward(self, top, propagate_down, bottom):
        pass

    def get_next_image_batch_color(self, img_names):
        """ convert images into blobs
        """
        num_img, processed_img = len(img_names), []
        for i, name in enumerate(img_names):
            img = cv2.imread(os.path.join(self.image_dir, name))
            img = img - cfg.MEAN_VAL_GBR
            img_min_size, img_max_size = min(img.shape[:2]), max(img.shape[:2])

            ##### may not need scale, because of the scaling transform is applied
            scale = 1.0 ##########
            #scale = float(cfg.TRAIN.TARGET_SIZE) / float(img_min_size)
            # prevent the images too big to fit in the memory
            if float(img_max_size) * scale > cfg.TRAIN.MAX_SIZE:
                scale = float(cfg.TRAIN.MAX_SIZE) / float(img_max_size)
            if scale != 1.0:
                img = cv2.resize(img, None, None, fx=scale, fy=scale, interpolation=cv2.INTER_LINEAR)

            ##### make each side even #####
            if img.shape[0]%2 != 0:
                img = np.concatenate([img, img[-1][np.newaxis,:, :]], axis=0)
                #img = np.vstack([img, img[-1]])
            if img.shape[1]%2 != 0:
                img = np.concatenate([img, img[:, -1][:, np.newaxis, :]], axis=1)
                #img = np.hstack([img, img[:,[-1]]])
            ###############################

            processed_img.append(img)
    
        max_shape = np.array([img.shape for img in processed_img]).max(axis=0)
        print max_shape
        img_blobs = np.zeros((num_img, max_shape[0], max_shape[1], 3), dtype=np.float32)
        for i, img in enumerate(processed_img):
            img_blobs[i, :img.shape[0], :img.shape[1], :] = img
        img_blobs = img_blobs.transpose((0, 3, 1, 2))

        return img_blobs
 

    def get_next_image_batch_gray(self, img_names):
        """ convert images into blobs
        """
        num_img, processed_img = len(img_names), []
        for i, name in enumerate(img_names):
            img = cv2.imread(os.path.join(self.image_dir, name), cv2.CV_LOAD_IMAGE_GRAYSCALE)
            img = img - cfg.MEAN_VAL_GRAY
            img_min_size, img_max_size = min(img.shape[:2]), max(img.shape[:2])

            ##### may not need scale, because of the scaling transform is applied
            scale = 1.0 ##########
            #scale = float(cfg.TRAIN.TARGET_SIZE) / float(img_min_size)
            # prevent the images too big to fit in the memory
            if float(img_max_size) * scale > cfg.TRAIN.MAX_SIZE:
                scale = float(cfg.TRAIN.MAX_SIZE) / float(img_max_size)
            if scale != 1.0:
                img = cv2.resize(img, None, None, fx=scale, fy=scale, interpolation=cv2.INTER_LINEAR)

            ##### make each side even #####
            if img.shape[0]%2 != 0:
                img = np.vstack([img, img[-1]])
            if img.shape[1]%2 != 0:
                img = np.hstack([img, img[:,[-1]]])
            ###############################

            processed_img.append(img)
    
        max_shape = np.array([img.shape for img in processed_img]).max(axis=0)
        img_blobs = np.zeros((num_img, max_shape[0], max_shape[1]), dtype=np.float32)
        for i, img in enumerate(processed_img):
            img_blobs[i, :img.shape[0], :img.shape[1]] = img
        img_blobs = img_blobs[:, np.newaxis, ...]

        return img_blobs
 
















