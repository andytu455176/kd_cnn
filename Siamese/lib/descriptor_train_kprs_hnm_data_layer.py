import h5py, yaml, cv2, cPickle
import sys, os
import numpy as np 
import matplotlib.pyplot as plt 
from collections import defaultdict
from itertools import combinations

from config import cfg 

caffe_root = cfg.kd_cnn_root + 'new-py-faster-rcnn/caffe-fast-rcnn/'
sys.path.insert(0, caffe_root + 'python')
import caffe

class DescriptorTrainKPRSHNMDataLayer(caffe.Layer):
    def setup(self, bottom, top):
        print "DescriptorTrainDataLayer setting up!"
        """ yaml param: 
        image_list, image_dir, kp_rs_file, 
        data_info_name, data_info_mode, 
        num_trans, 
        img_batch_size, roi_per_img
        """
        layer_param = yaml.load(self.param_str_)
        with open(layer_param['image_list'], 'r') as f:
            image_list = [x.strip() for x in f.readlines()]
    
        self.IMG_BATCH_SIZE = int(layer_param['img_batch_size'])
        self.ROI_PER_IMG = int(layer_param['roi_per_img'])
        data_info_name = layer_param['data_info_name'] if 'data_info_name' in layer_param else None
        self.init_layer(layer_param['image_dir'], layer_param['kp_rs_file'], int(layer_param['num_trans']))

        if 'data_info_name' not in layer_param:
            np.random.shuffle(image_list)
            self.init_data(image_list)
        else:
            if layer_param['data_info_mode'] == 'w':
                np.random.shuffle(image_list)
                self.init_data(image_list, True, data_info_name)
            elif layer_param['data_info_mode'] == 'r':
                self.init_data_from_file(data_info_name)

        top[0].reshape(1, 3, 100, 100)
        top[1].reshape(1, 5)
        top[2].reshape(1,)

    def forward(self, bottom, top):
        """ top[0]: img_blobs, top[1]: rois_blobs, top[2]: labels_blobs
        """
        batch_data = self.get_next_minibatch()
        blob_name_to_top = { 'img_blobs': 0, 'rois_blobs': 1, 'labels_blobs': 2 }
        for name, blobs in batch_data.iteritems():
            index = blob_name_to_top[name]
            top[index].reshape(*(blobs.shape))
            top[index].data[...] = blobs.astype(np.float32, copy=False)
               
    def reshape(self, bottom, top):
        pass

    def backward(self, top, propagate_down, bottom):
        pass

    def init_layer(self, image_dir, kp_rs_file, num_trans):
        self.epoch, self.cur_img, self.cur_roi, self.total_roi, self.cur_trans = 0, 0, 0, 0, 0
        self.image_dir = image_dir
        self.kp_rs_file_name = kp_rs_file
        self.kp_rs_file = h5py.File(self.kp_rs_file_name, 'r')
        self.NUM_TRANS = num_trans

        if self.NUM_TRANS%2 != 0:
            print "num_trans must be an even number"
            exit()

        self.trans_index = range(self.NUM_TRANS)
        np.random.shuffle(self.trans_index)
        self.img_name_list = []
        self.img_to_patch = dict()
        return 

    def init_data(self, image_list, is_output=False, data_info_name=""):
        roi_per_img = self.ROI_PER_IMG
        for name in image_list:
            if self.kp_rs_file[name][0, ...].shape[0] <= 0: continue # no keypoint detected 
            candidate_kp_idx = range(self.kp_rs_file[name][0, ...].shape[0])
            num_roi = np.min([len(candidate_kp_idx), roi_per_img])
            chosen_kp_idx = np.random.choice(candidate_kp_idx, num_roi, replace=False)
            self.img_to_patch[name] = chosen_kp_idx
            self.img_name_list.append(name)
        if is_output: self.output_data(data_info_name)
        return 

    def init_data_from_file(self, data_info_name):
        with open(data_info_name, 'rb') as f:
            data_info = cPickle.load(f)
        self.image_dir = data_info['image_dir']
        self.img_name_list = data_info['img_name_list']
        self.img_to_patch = data_info['img_to_patch']
        np.random.shuffle(self.img_name_list)
        return 

    def output_data(self, data_info_name):
        data_info = dict()
        data_info['image_dir'] = self.image_dir
        data_info['img_name_list'] = self.img_name_list
        data_info['img_to_patch'] = self.img_to_patch
        with open(data_info_name, 'wb') as f:
            cPickle.dump(data_info, f)
        return 

    def close_layer(self):
        # init layer
        self.kp_rs_file.close()
        # init training data
        return

    def get_next_image_batch(self, img_names):
        """ convert images into blobs
        """
        processed_img = []
        for i, name in enumerate(img_names):
            for j in range(2):
                img_name = name[:-4]+"_"+str(self.trans_index[self.cur_trans+j])+".jpg"
                img = cv2.imread(os.path.join(self.image_dir, img_name))
                img = img - cfg.MEAN_VAL_GBR

                ##### make each side even #####
                if img.shape[0]%2 != 0:
                    img = np.concatenate([img, img[-1][np.newaxis,:, :]], axis=0)
                    #img = np.vstack([img, img[-1]])
                if img.shape[1]%2 != 0:
                    img = np.concatenate([img, img[:, -1][:, np.newaxis, :]], axis=1)
                    #img = np.hstack([img, img[:,[-1]]])
                ###############################

                processed_img.append(img)

        num_img = len(processed_img)
        max_shape = np.array([img.shape for img in processed_img]).max(axis=0)
        img_blobs = np.zeros((num_img, max_shape[0], max_shape[1], 3), dtype=np.float32)
        for i, img in enumerate(processed_img):
            img_blobs[i, :img.shape[0], :img.shape[1], :] = img
        img_blobs = img_blobs.transpose((0, 3, 1, 2))

        return img_blobs
    
    def get_next_roi_batch(self, img_names):
        """ convert keypoints into rois and surrogate labels
        """
        rois_blobs = np.zeros((0, 5), dtype=np.float32)
        labels_blobs = np.zeros((0,), dtype=np.float32)
        num_img = len(img_names)

        num_roi_list = [self.img_to_patch[name].shape[0] for name in img_names]

        for i, img_name in enumerate(img_names):
            index1, index2 = self.trans_index[self.cur_trans], self.trans_index[self.cur_trans+1]
            candidate_kp1 = self.kp_rs_file[img_name][index1, ...]
            candidate_kp2 = self.kp_rs_file[img_name][index2, ...]
            chosen_kp_idx = self.img_to_patch[img_name]

            num_roi = chosen_kp_idx.shape[0]
            roi_index = np.arange(num_roi)

            kp1 = candidate_kp1[chosen_kp_idx]
            kp2 = candidate_kp2[chosen_kp_idx]

            roi_param1 = np.array([np.ones((num_roi,))*(2*i), kp1[:, 0], kp1[:, 1], kp1[:, 2], kp1[:, 3]], dtype=np.float32).T
            roi_param2 = np.array([np.ones((num_roi,))*(2*i+1), kp2[:, 0], kp2[:, 1], kp2[:, 2], kp2[:, 3]], dtype=np.float32).T

            cur_rois_blobs = np.zeros((2*num_roi, 5), dtype=np.float32)
            cur_rois_blobs[2*roi_index] = roi_param1[...]
            cur_rois_blobs[2*roi_index+1] = roi_param2[...]
            rois_blobs = np.vstack([rois_blobs, cur_rois_blobs])

            cur_labels_blobs = np.ones((2*num_roi,), dtype=np.float32) * i
            labels_blobs = np.hstack([labels_blobs, cur_labels_blobs])

        return rois_blobs, labels_blobs

    def get_next_minibatch(self):
        if self.cur_img >= len(self.img_name_list):
            # all images are passed through 
            self.cur_trans += 2
            self.cur_img = 0
            if self.cur_trans >= len(self.trans_index):
                # all transformation are passed through, next epoch 
                self.cur_roi = 0
                self.cur_trans = 0
                self.epoch += 1
                np.random.shuffle(self.trans_index) #TODO
            np.random.shuffle(self.img_name_list) #TODO

        img_names = self.img_name_list[self.cur_img:self.cur_img+self.IMG_BATCH_SIZE]
        self.cur_img += np.min([len(self.img_name_list)-self.cur_img, self.IMG_BATCH_SIZE])

        img_blobs = self.get_next_image_batch(img_names)
        rois_blobs, labels_blobs = self.get_next_roi_batch(img_names)

        #self.cur_roi += rois_blobs1.shape[0]
        #print "Epoch<%d> ... progress: (img) %d/%d, (roi) %d/%d" % (self.epoch, self.cur_img, len(self.img_name_list), self.cur_roi, self.total_roi * self.NUM_TRANS)
        return { 'img_blobs': img_blobs, 'rois_blobs': rois_blobs, 'labels_blobs': labels_blobs }

















