import sys
import numpy as np 
import yaml
from config import cfg 

sys.path.insert(0, cfg.kd_cnn_root + 'new-py-faster-rcnn/lib')
from fast_rcnn.nms_wrapper import nms

caffe_root = cfg.kd_cnn_root + 'new-py-faster-rcnn/caffe-fast-rcnn/'
sys.path.insert(0, caffe_root + 'python')
import caffe

class KeypointProposalLayer(caffe.Layer):
    def setup(self, bottom, top):
        """ yaml param:
        nms_thres, patch_size, mode ('rois_all', 'rois_nms', 'kp_all', 'kp_nms')
        """
        layer_param = yaml.load(self.param_str_)
        self.nms_thres = float(layer_param['nms_thres'])
        self.patch_size = int(layer_param['patch_size'])
        self.half = self.patch_size // 2
        self.mode = layer_param['mode']
        if layer_param['mode'] == 'rois_nms' or layer_param['mode'] == 'rois_all':
            top[0].reshape(1, 5)
        elif layer_param['mode'] == 'kp_all' or layer_param['mode'] == 'kp_nms':
            top[0].reshape(1, 3)
        #self.nms_thres = 0.15
        #self.patch_size = 32
        #self.half = self.patch_size // 2
        print "KeypointProposalLayer setting up!!"
        
    def forward(self, bottom, top):
        """ bottom[0] is the keypoint map
        """
        #assert bottom[0].data.shape[0] == 1, "Only single item batches are supported"

        img_shape = bottom[0].data.shape[2:]
        kp, n_kp = np.zeros((0, 3), dtype=np.float32), np.zeros((0, 3), dtype=np.float32)
        for image_index in xrange(bottom[0].data.shape[0]):
            keypoint = np.vstack(np.where(bottom[0].data[image_index, 0, ...] > 0.5))
            c_kp = np.hstack([np.ones((keypoint.shape[1], 1), dtype=np.float32)*image_index, keypoint.T])
            boxes = np.array([keypoint[0]-self.half, keypoint[1]-self.half, keypoint[0]+self.half, keypoint[1]+self.half], dtype=np.float32).T
            keep = nms(np.hstack([boxes, np.ones((boxes.shape[0],1), dtype=np.float32)]), self.nms_thres)
            cn_kp = np.array([boxes[keep][:, 0]+self.half, boxes[keep][:, 1]+self.half]).T
            cn_kp = cn_kp[(cn_kp[:,0]>=self.half)&(cn_kp[:,0]<img_shape[0]-self.half)&(cn_kp[:,1]>=self.half)&(cn_kp[:,1]<img_shape[1]-self.half)]
            cn_kp = np.hstack([np.ones((cn_kp.shape[0], 1), dtype=np.float32)*image_index, cn_kp])
            kp = np.vstack([kp, c_kp])
            n_kp = np.vstack([n_kp, cn_kp])

        if n_kp.shape[0] > 0:
            if self.mode == 'kp_nms':
                top[0].reshape(n_kp.shape[0], 3)
                top[0].data[...] = n_kp
            elif self.mode == 'kp_all':
                top[0].reshape(kp.shape[0], 3)
                top[0].data[...] = kp
            elif self.mode == 'rois_nms':
                rois = np.array([n_kp[:,2]-self.half, n_kp[:,1]-self.half, n_kp[:,2]+self.half, n_kp[:,1]+self.half], dtype=np.float32).T
                top[0].reshape(n_kp.shape[0], 5)
                top[0].data[...] = np.hstack([n_kp[:, [0]], rois])
            elif self.mode == 'rois_all':
                rois = np.array([kp[:,2]-self.half, kp[:,1]-self.half, kp[:,2]+self.half, kp[:,1]+self.half], dtype=np.float32).T
                top[0].reshape(kp.shape[0], 5)
                top[0].data[...] = np.hstack([kp[:, [0]], rois])
        else:
            if self.mode == 'kp_nms' or self.mode == 'kp_all':
                top[0].reshape(1, 3)
                top[0].data[...] = np.zeros((1, 3), dtype=np.float32)
            elif self.mode == 'rois_nms' or self.mode == 'rois_all':
                top[0].reshape(1, 5)
                top[0].data[...] = np.zerots((1, 5), dtype=np.float32)

    def reshape(self, bottom, top):
        pass

    def backward(self, top, propagate_down, bottom):
        pass











