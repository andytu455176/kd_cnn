#!/bin/bash

							  #--kp_rs_file ${kd_cnn_root}/Siamese/hdf5_file/Mikolajczyk_opencv_ori_kp_rs_tr.hdf5 \
							  #--kp_rs_file ${kd_cnn_root}/Siamese/hdf5_file/Mikolajczyk_opencv_ori_kp_rs_01.hdf5 \
							  #--kp_file ${kd_cnn_root}/Siamese/hdf5_file/Mikolajczyk_cnn_ori_keypoint.hdf5 \

kd_cnn_root=/home/iis/Cheng-Hao/Bitbucket/kd_cnn

python ${kd_cnn_root}/Siamese/image_preprocessing.py --action generate_random_transform \
							  --dir ${kd_cnn_root}/data/Mikolajczyk_train_Dataset \
							  --output_dir ${kd_cnn_root}/Siamese/trans_images_Mikolajczyk \
							  --image_list ${kd_cnn_root}/Siamese/image_list/Mikolajczyk_image_list.txt \
							  --kp_file ${kd_cnn_root}/Siamese/hdf5_file/Mikolajczyk_opencv_ori_keypoint.hdf5 \
							  --trans_num 60 \
							  --proc_num 6

