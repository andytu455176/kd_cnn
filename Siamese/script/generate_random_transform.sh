#!/bin/bash

							  #--kp_rs_file ${kd_cnn_root}/Siamese/hdf5_file/opencv_ori_kp_rs_01.hdf5 \
							  #--kp_file ${kd_cnn_root}/Siamese/hdf5_file/opencv_ori_keypoint.hdf5 \
							  #--kp_file ${kd_cnn_root}/Siamese/hdf5_file/cnn_ori_keypoint.hdf5 \

kd_cnn_root=/home/iis/Cheng-Hao/Bitbucket/kd_cnn

python ${kd_cnn_root}/Siamese/image_preprocessing.py --action generate_random_transform \
							  --dir ${kd_cnn_root}/data/Flicker8k_Dataset \
							  --output_dir ${kd_cnn_root}/Siamese/trans_images \
							  --image_list ${kd_cnn_root}/Siamese/image_list/image_list.txt \
							  --kp_rs_file ${kd_cnn_root}/Siamese/hdf5_file/opencv_ori_kp_rs_tr.hdf5 \
							  --trans_num 60 \
							  --proc_num 6

