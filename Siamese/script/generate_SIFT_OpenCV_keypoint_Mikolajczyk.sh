#!/bin/bash

				#--kp_rs_file ${kd_cnn_root}/Siamese/hdf5_file/Mikolajczyk_opencv_ori_kp_rs_tr.hdf5 \
				#--kp_rs_file ${kd_cnn_root}/Siamese/hdf5_file/Mikolajczyk_opencv_ori_kp_rs_01.hdf5 \

kd_cnn_root=/home/iis/Cheng-Hao/Bitbucket/kd_cnn

python ${kd_cnn_root}/Siamese/image_preprocessing.py --action generate_SIFT_OpenCV_keypoint \
				--dir ${kd_cnn_root}/data/Mikolajczyk_train_Dataset \
				--image_list ${kd_cnn_root}/Siamese/image_list/Mikolajczyk_image_list.txt \
				--kp_file ${kd_cnn_root}/Siamese/hdf5_file/Mikolajczyk_opencv_ori_keypoint.hdf5 \
				--num_kp 25



