#!/bin/bash

kd_cnn_root=/home/iis/Cheng-Hao/Bitbucket/kd_cnn

python ${kd_cnn_root}/Siamese/image_preprocessing.py --action generate_SIFT_CNN_keypoint \
		--dir ${kd_cnn_root}/data/Flicker8k_Dataset \
		--image_list ${kd_cnn_root}/Siamese/image_list/image_list.txt \
		--prototxt ${kd_cnn_root}/Siamese/KD_CNN_for_SIFT_approx_simplified/KD_CNN_for_SIFT_approx_simplified.prototxt \
    	--caffemodel ${kd_cnn_root}/Siamese/KD_CNN_for_SIFT_approx_simplified/KD_CNN_for_SIFT_approx_simplified.caffemodel \
		--kp_file ${kd_cnn_root}/Siamese/hdf5_file/cnn_ori_keypoint.hdf5





